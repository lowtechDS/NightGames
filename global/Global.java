package global;

import gui.GUI;
import gui.Title;
import items.Component;
import items.Consumable;
import items.Flask;
import items.Item;
import items.Potion;
import items.Toy;
import items.Trophy;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import pet.Ptype;


import skills.*;
import trap.Alarm;
import trap.AphrodisiacTrap;
import trap.Decoy;
import trap.DissolvingTrap;
import trap.EnthrallingTrap;
import trap.IllusionTrap;
import trap.Snare;
import trap.Spiderweb;
import trap.SpringTrap;
import trap.StripMine;
import trap.TentacleTrap;
import trap.Trap;
import trap.Tripline;

import actions.Action;
import actions.Bathe;
import actions.Craft;
import actions.Drink;
import actions.Energize;
import actions.Hide;
import actions.JerkOff;
import actions.Locate;
import actions.Movement;
import actions.Recharge;
import actions.Resupply;
import actions.Scavenge;
import actions.SetTrap;
import actions.Use;
import actions.Wait;
import areas.Area;
import areas.MapDrawHint;
import characters.Angel;
import characters.Attribute;
import characters.Character;
import characters.Cassie;
import characters.Eve;
import characters.Jewel;
import characters.Kat;
import characters.Mara;
import characters.Maya;
import characters.NPC;
import characters.Personality;
import characters.Player;
import characters.Reyka;
import characters.Samantha;
import characters.Trait;
import characters.Yui;
import daytime.Daytime;
import daytime.Scene;


public class Global {
	private static Random rng;
	private static GUI gui;
	private static HashSet<Skill> skillPool;
	private static ArrayList<Action> actionPool;
	private static ArrayList<Trap> trapPool;
	private static HashSet<Trait> featPool;
	private static HashSet<Character> players;
	private static HashSet<Character> resting;
	private static HashSet<Flag> flags;
	private static HashMap<Flag,Float> counters;
	private static Player human;
	private static Match match;
	private static Daytime day;
	private static int date;
	public static Scene current;
	public static boolean debug = false;
	
	public Global(GUI gui){
		rng=new Random();
		flags = new HashSet<Flag>();
		players = new HashSet<Character>();
		resting = new HashSet<Character>();
		counters = new HashMap<Flag,Float>();
		current=null;
		this.gui = gui;
		this.gui.BuildGUI();
		buildActionPool();
		buildFeatPool();
	}
	public static void newGame(Player one, boolean tutorial){
		human = one;
		players.add(human);		
		gui.populatePlayer(human);
		buildSkillPool(human);
		Global.learnSkills(human);
		date=0;
		Cassie ai1 = new Cassie();
		Jewel ai2 = new Jewel();
		Mara ai3 = new Mara();
		Angel ai4 = new Angel();
		players.add(ai1.character);
		players.add(ai2.character);
		players.add(ai3.character);
		players.add(ai4.character);
		if(tutorial){
			new Tutorial(one).play("");
		}else{
			match = new Match(players,Modifier.normal);
			match.round();
		}
	}
	
	public static int random(int d){
		if(d<=0){
			return 0;
		}
		return rng.nextInt(d);
	}

	public static GUI gui(){
		return gui;
	}

	public static Player getPlayer(){
		return human;
	}

	public static void buildSkillPool(Player p){
		skillPool=new HashSet<Skill>();
		skillPool.add(new Kiss(p));
		skillPool.add(new StripTop(p));
		skillPool.add(new StripBottom(p));
		skillPool.add(new FondleBreasts(p));
		skillPool.add(new Fuck(p));
		skillPool.add(new Tickle(p));
		skillPool.add(new Shove(p));
		skillPool.add(new Slap(p));
		skillPool.add(new ArmBar(p));
		skillPool.add(new Blowjob(p));
		skillPool.add(new Cunnilingus(p));
		skillPool.add(new Escape(p));
		skillPool.add(new Flick(p));
		skillPool.add(new Knee(p));
		skillPool.add(new LegLock(p));
		skillPool.add(new LickNipples(p));
		skillPool.add(new Maneuver(p));
		skillPool.add(new Paizuri(p));
		skillPool.add(new PerfectTouch(p));
		skillPool.add(new Restrain(p));
		skillPool.add(new Reversal(p));
		skillPool.add(new Spank(p));
		skillPool.add(new Stomp(p));
		skillPool.add(new SuckNeck(p));
		skillPool.add(new Tackle(p));
		skillPool.add(new Taunt(p));
		skillPool.add(new Trip(p));
		skillPool.add(new Whisper(p));
		skillPool.add(new Kick(p));
		skillPool.add(new Footjob(p));
		skillPool.add(new Handjob(p));
		skillPool.add(new Squeeze(p));
		skillPool.add(new Nurple(p));
		skillPool.add(new Finger(p));
		skillPool.add(new Tie(p));
		skillPool.add(new Masturbate(p));
		skillPool.add(new Piston(p));
		skillPool.add(new Grind(p));
		skillPool.add(new Thrust(p));
		skillPool.add(new UseDildo(p));
		skillPool.add(new UseOnahole(p));
		skillPool.add(new UseCrop(p));
		skillPool.add(new Carry(p));
		skillPool.add(new Tighten(p));
		skillPool.add(new HipThrow(p));
		skillPool.add(new SpiralThrust(p));
		skillPool.add(new Bravado(p));
		skillPool.add(new Diversion(p));
		skillPool.add(new Undress(p));
		skillPool.add(new Strapon(p));
		skillPool.add(new AssFuck(p));
		skillPool.add(new Turnover(p));
		skillPool.add(new Tear(p));
		skillPool.add(new Binding(p));
		skillPool.add(new Bondage(p));
		skillPool.add(new WaterForm(p));
		skillPool.add(new DarkTendrils(p));
		skillPool.add(new Dominate(p));
		skillPool.add(new FlashStep(p));
		skillPool.add(new FlyCatcher(p));
		skillPool.add(new Illusions(p));
		skillPool.add(new LustAura(p));
		skillPool.add(new MagicMissile(p));
		skillPool.add(new Masochism(p));
		skillPool.add(new NakedBloom(p));
		skillPool.add(new ShrinkRay(p));
		skillPool.add(new SpawnFaerie(p,Ptype.fairyfem));
		skillPool.add(new SpawnImp(p,Ptype.impfem));
		skillPool.add(new SpawnFaerie(p,Ptype.fairymale));
		skillPool.add(new SpawnImp(p,Ptype.impmale));
		skillPool.add(new SpawnSlime(p));
		skillPool.add(new StunBlast(p));
		skillPool.add(new Fly(p));
		skillPool.add(new Command(p));
		skillPool.add(new Obey(p));
		skillPool.add(new Drain(p));
		skillPool.add(new StoneForm(p));
		skillPool.add(new FireForm(p));
		skillPool.add(new Defabricator(p));
		skillPool.add(new TentaclePorn(p));
		skillPool.add(new Sacrifice(p));
		skillPool.add(new Frottage(p));
		skillPool.add(new FaceFuck(p));
		skillPool.add(new VibroTease(p));
		skillPool.add(new TailPeg(p));
		skillPool.add(new CommandDismiss(p));
		skillPool.add(new CommandDown(p));
		skillPool.add(new CommandGive(p));
		skillPool.add(new CommandHurt(p));
		skillPool.add(new CommandInsult(p));
		skillPool.add(new CommandMasturbate(p));
		skillPool.add(new CommandOral(p));
		skillPool.add(new CommandStrip(p));
		skillPool.add(new CommandStripPlayer(p));
		skillPool.add(new CommandUse(p));
		skillPool.add(new ShortCircuit(p));
		skillPool.add(new IceForm(p));
		skillPool.add(new Barrier(p));
		skillPool.add(new CatsGrace(p));
		skillPool.add(new TailJob(p));
		skillPool.add(new FaceSit(p));
		skillPool.add(new Purr(p));
		skillPool.add(new Fabricator(p));
		skillPool.add(new BondageStraps(p));
		skillPool.add(new MageArmor(p));
		skillPool.add(new Dive(p));
		skillPool.add(new Cowardice(p));
		skillPool.add(new Stumble(p));
		skillPool.add(new Invite(p));
		skillPool.add(new Beg(p));
		skillPool.add(new ShamefulDisplay(p));
		skillPool.add(new TortoiseWrap(p));
		skillPool.add(new Scroll(p));
		skillPool.add(new Suggestion(p));
		skillPool.add(new HeightenSenses(p));
		skillPool.add(new Blindside(p));
		skillPool.add(new SpawnFGoblin(p));
		skillPool.add(new Needle(p));
		skillPool.add(new StealClothes(p));
		skillPool.add(new BunshinAssault(p));
		skillPool.add(new BunshinService(p));
		skillPool.add(new Substitute(p));
		skillPool.add(new GoodnightKiss(p));
		skillPool.add(new WindUp(p));
		skillPool.add(new Haste(p));
		skillPool.add(new CheapShot(p));
		skillPool.add(new AttireShift(p));
		skillPool.add(new EmergencyJump(p));
		skillPool.add(new Unstrip(p));
		skillPool.add(new Rewind(p));
		skillPool.add(new LustOverflow(p));
		skillPool.add(new ManaFortification(p));
		skillPool.add(new PleasureBomb(p));
		skillPool.add(new MatterConverter(p));
		skillPool.add(new KoushoukaBurst(p));
		skillPool.add(new Humiliate(p));
		skillPool.add(new EnflameLust(p));
		skillPool.add(new PheromoneOverdrive(p));
		skillPool.add(new Nymphomania(p));
		skillPool.add(new CrushPride(p));
		skillPool.add(new Buck(p));
		skillPool.add(new HoneyTrap(p));
		skillPool.add(new PleasureSlave(p));
		skillPool.add(new KissEX(p));
		skillPool.add(new KneeEX(p));
		skillPool.add(new HandjobEX(p));
		skillPool.add(new StripTopEX(p));
		skillPool.add(new StripBottomEX(p));
		skillPool.add(new FingerEX(p));
		skillPool.add(new ThrustEX(p));
	}

	public static void buildActionPool(){
		actionPool=new ArrayList<Action>();
		actionPool.add(new Resupply());
		actionPool.add(new Bathe());
		actionPool.add(new Scavenge());
		actionPool.add(new Craft());
		actionPool.add(new Use(Flask.Lubricant));
		actionPool.add(new Recharge());
		actionPool.add(new JerkOff());
		actionPool.add(new Energize());
		actionPool.add(new Hide());
		for(Potion p:Potion.values()){
			actionPool.add(new Drink(p));
		}
		buildTrapPool();
		for(Trap t:trapPool){
			actionPool.add(new SetTrap(t));
		}
		actionPool.add(new Wait());
	}
	public static void populateTargetedActions(HashSet<Character> combatants){
		for(Character active: combatants){
			actionPool.add(new Locate(active));
		}
	}

	public static void buildTrapPool(){
		trapPool=new ArrayList<Trap>();
		trapPool.add(new Alarm());
		trapPool.add(new Tripline());
		trapPool.add(new Snare());
		trapPool.add(new SpringTrap());
		trapPool.add(new AphrodisiacTrap());
		trapPool.add(new DissolvingTrap());
		trapPool.add(new Decoy());
		trapPool.add(new Spiderweb());
		trapPool.add(new EnthrallingTrap());
		trapPool.add(new IllusionTrap());
		trapPool.add(new StripMine());
		trapPool.add(new TentacleTrap());
	}
	public static void buildFeatPool(){
		featPool = new HashSet<Trait>();
		featPool.add(Trait.sprinter);
		featPool.add(Trait.QuickRecovery);
		featPool.add(Trait.Sneaky);
		featPool.add(Trait.Confident);
		featPool.add(Trait.SexualGroove);
		featPool.add(Trait.BoundlessEnergy);
		featPool.add(Trait.Unflappable);
		featPool.add(Trait.resourceful);
		featPool.add(Trait.treasureSeeker);
		featPool.add(Trait.sympathetic);
		featPool.add(Trait.leadership);
		featPool.add(Trait.tactician);
		featPool.add(Trait.PersonalInertia);
		featPool.add(Trait.fitnessNut);
		featPool.add(Trait.expertGoogler);
		featPool.add(Trait.mojoMaster);
		featPool.add(Trait.fastLearner);
		featPool.add(Trait.cautious);
		featPool.add(Trait.responsive);
		featPool.add(Trait.assmaster);
		featPool.add(Trait.Clingy);
		featPool.add(Trait.houdini);
		featPool.add(Trait.coordinatedStrikes);
		featPool.add(Trait.evasiveManuevers);
		featPool.add(Trait.handProficiency);
		featPool.add(Trait.handExpertise);
		featPool.add(Trait.handMastery);
		featPool.add(Trait.oralProficiency);
		featPool.add(Trait.oralExpertise);
		featPool.add(Trait.oralMastery);
		featPool.add(Trait.intercourseProficiency);
		featPool.add(Trait.intercourseExpertise);
		featPool.add(Trait.intercourseMastery);
		featPool.add(Trait.footloose);
		featPool.add(Trait.amateurMagician);
		featPool.add(Trait.bountyHunter);
		featPool.add(Trait.challengeSeeker);
		featPool.add(Trait.showoff);
	}

	public static ArrayList<Action> getActions(){
		return actionPool;
	}
	public static HashSet<Trait> getFeats(){
		return featPool;
		
	}
	public static Character lookup(String name){
		for(Character player:players){
			if(player.name().equalsIgnoreCase(name)){
				return player;
			}
		}
		return null;
	}

	public static Match getMatch(){
		return match;
	}

	public static Daytime getDay(){
		return day;
	}

	public static void dawn(){
		match=null;
		for(Character rested: resting){
			rested.gainXP(100+(10*(human.getLevel()-rested.getLevel())));
		}
		for(Character player: players){
			player.getStamina().fill();
			player.getArousal().empty();
			player.getMojo().empty();
			player.change(Modifier.normal);
		}
		date++;
		day=new Daytime(human);
		Global.gui().refresh();
	}

	public static void dusk(Modifier matchmod){
		HashSet<Character> lineup = new HashSet<Character>();
		Character lover=null;
		int maxaffection=0;
		day=null;
		for(Character player: players){
			player.getStamina().fill();
			player.getArousal().empty();
			player.getMojo().empty();
			if(player.getPure(Attribute.Science)>0){
				player.chargeBattery();
			}
			if(human.getAffection(player)>maxaffection){
				maxaffection=human.getAffection(player);
				lover=player;
			}			
		}
		if(matchmod==Modifier.maya){
			ArrayList<Character> randomizer = new ArrayList<Character>();
			if(lover!=null){
				lineup.add(lover);
			}
			lineup.add(human);
			randomizer.addAll(players);
			Collections.shuffle(randomizer);
			for(Character player: randomizer){
				if(!lineup.contains(player)&&!player.human()&&lineup.size()<4&&!player.has(Trait.event)){
					lineup.add(player);
				}
				else if(lineup.size()>=4||player.has(Trait.event)){
					resting.add(player);
				}
			}
			if(!Global.checkFlag(Flag.Maya)){
				Global.newChallenger(new Maya());
				Global.flag(Flag.Maya);
			}
			getNPC("Maya").scaleLevel(human.getLevel()+20);
			lineup.add(getNPC("Maya"));
			match=new Match(lineup,matchmod);
		}
		else if(players.size()>5){
			ArrayList<Character> randomizer = new ArrayList<Character>();
			if(lover!=null){
				lineup.add(lover);
			}
			lineup.add(human);
			randomizer.addAll(players);
			Collections.shuffle(randomizer);
			for(Character player: randomizer){
				if(!lineup.contains(player)&&!player.human()&&lineup.size()<5&&!player.has(Trait.event)){
					lineup.add(player);
				}
				else if(lineup.size()>=5||player.has(Trait.event)){
					resting.add(player);
				}
			}
			match=new Match(lineup,matchmod);
		}
		else{
			match=new Match(players,matchmod);
		}
		buildActionPool();
		populateTargetedActions(lineup);
		match.round();
	}

	public static void gainSkills(Character c){
		for(Skill skill:skillPool){
			if(skill.requirements(c)&&!c.knows(skill)){
				c.learn(skill.copy(c));
				if(c.human()){
					gui().message("You've learned "+skill.toString()+".");
				}
			}
		}
		if(c.getPure(Attribute.Dark)>=6&&!c.has(Trait.darkpromises)){
			c.add(Trait.darkpromises);
		}
		if(c.getPure(Attribute.Animism)>=2&&!c.has(Trait.pheromones)){
			c.add(Trait.pheromones);
		}
	}

	public static void learnSkills(Player p){
		for(Skill skill:skillPool){
			if(skill.requirements()&&!p.knows(skill)){
				p.learn(skill);
			}
		}
		if(p.getPure(Attribute.Dark)>=6&&!p.has(Trait.darkpromises)){
			p.add(Trait.darkpromises);
		}
		if(p.getPure(Attribute.Animism)>=2&&!p.has(Trait.pheromones)){
			p.add(Trait.pheromones);
		}
	}


	public static void flag(Flag f){
		flags.add(f);
	}

	public static void unflag(Flag f){
		flags.remove(f);
	}

	public static boolean checkFlag(Flag f){
		return flags.contains(f);
	}

	public static float getValue(Flag f){
		if(!counters.containsKey(f)){
			return 0;
		}
		else{
			return counters.get(f);
		}
	}
	public static void modCounter(Flag f, float inc){
		if(!counters.containsKey(f)){
			setCounter(f,inc);
		}
		else{
			counters.put(f, getValue(f)+inc);
		}
	}
	public static void setCounter(Flag f, float val){
		counters.put(f,val);
	}
	
	public static void save(boolean auto){
		try {
			FileWriter file;
			if(auto){
				file = new FileWriter("./auto.sav");
			}
			else{
				JFileChooser dialog = new JFileChooser("./");            
	            dialog.setMultiSelectionEnabled(false);
	            dialog.setFileFilter(new FileNameExtensionFilter("Save File (.sav)","sav"));
	            dialog.setSelectedFile(new File("Save.sav"));
	            int rv = dialog.showSaveDialog(gui);
	            
	            if (rv != JFileChooser.APPROVE_OPTION)
	            {
	            	return;
	            }
				file = new FileWriter(dialog.getSelectedFile());
			}
			PrintWriter saver = new PrintWriter(file);
			human.save(saver);
			for(Character c: players){
				if(!c.human()){
					c.save(saver);
				}
			}
			saver.write("Flags\n");
			for(Flag f: flags){
				saver.write(f.toString());
				saver.write("\n");
			}
			saver.write("#\n");
			if(Global.checkFlag(Flag.night)){
				saver.write("dawn "+date+"\n");
			}
			else{
				saver.write("dusk "+date+"\n");
			}
			for(Flag counter: counters.keySet()){
				saver.write(counter+" "+counters.get(counter)+"\n");
			}
			saver.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void load(){
		JFileChooser dialog = new JFileChooser("./");            
        dialog.setMultiSelectionEnabled(false);
        dialog.setFileFilter(new FileNameExtensionFilter("Save File (.sav)","sav"));
        int rv = dialog.showOpenDialog(gui);      
        if (rv != JFileChooser.APPROVE_OPTION)
        {
        	return;
        }
		FileInputStream file;
		String header;
		players.clear();
		flags.clear();
		gui.clearText();
		human=new Player("Dummy");
		gui.purgePlayer();
		buildSkillPool(human);
		NPC reyka = new Reyka().character;
		NPC kat = new Kat().character;
		NPC eve = new Eve().character;
		NPC sam = new Samantha().character;
		NPC maya = new Maya().character;
		NPC yui = new Yui().character;
		players.add(new Cassie().character);
		players.add(new Jewel().character);
		players.add(new Mara().character);
		players.add(new Angel().character);
		players.add(reyka);
		players.add(kat);
		players.add(eve);
		players.add(sam);
		players.add(maya);
		players.add(yui);
		boolean dawn = false;
		try {
			file = new FileInputStream(dialog.getSelectedFile());
			Scanner loader = new Scanner(file);
			while(loader.hasNext()){
				header=loader.next();				
				if(header.equalsIgnoreCase("P")){
					human.load(loader);
					players.add(human);
				}
				else if(header.equalsIgnoreCase("NPC")){
					String name = loader.next();
					lookup(name).load(loader);
				}
				else if(header.equalsIgnoreCase("Flags")){
					String next = loader.next();
					while(!next.equals("#")){
						flags.add(Flag.valueOf(next));
						next = loader.next();
					}
					if(loader.next().equalsIgnoreCase("Dawn")){
						dawn=true;
					}
					else{
						dawn=false;
					}
					if(loader.hasNext()){
						date = loader.nextInt();
					}
					else{
						date = 1;
					}
					while(loader.hasNext()){
						setCounter(Flag.valueOf(loader.next()),Float.parseFloat(loader.next()));
					}
				}
			}
			loader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!checkFlag(Flag.Reyka)){
			players.remove(reyka);
		}
		if(!checkFlag(Flag.Kat)){
			players.remove(kat);
		}
		if(!checkFlag(Flag.Eve)){
			players.remove(eve);
		}
		if(!checkFlag(Flag.Samantha)){
			players.remove(sam);
		}
		if(!checkFlag(Flag.Maya)){
			players.remove(maya);
		}
		if(!checkFlag(Flag.Yui)){
			players.remove(yui);
		}
		gui.populatePlayer(human);
		if(dawn){
			dawn();
		}
		else{
			if(Global.getDayString(Global.getDate()).startsWith("Sunday")){
				new NightOff(human);
			}else{
				new Prematch(human);
			}
		}
	}

	public static HashSet<Character> everyone(){
		return players;
	}

	public static boolean newChallenger(Personality challenger){
		if(getNPC(challenger.getCharacter().name())==null){
			while(challenger.getCharacter().getLevel()<=human.getLevel()){
				challenger.getCharacter().gainXP(95+(challenger.getCharacter().getLevel()*5));
				challenger.getCharacter().ding();
			}
			players.add(challenger.getCharacter());
			return true;
		}else{
			return false;
		}
	}

	public static NPC getNPC(String name){
		for( Character c : players){
			if(c.name().equalsIgnoreCase(name)){
				return (NPC)c;
			}
		}
		return null;
	}
	public static Item getItem(String name){
		for(Flask f: Flask.values()){
			if(f.toString().equalsIgnoreCase(name)){
				return f;
			}
		}
		for(Potion p: Potion.values()){
			if(p.toString().equalsIgnoreCase(name)){
				return p;
			}
		}
		for(Component c: Component.values()){
			if(c.toString().equalsIgnoreCase(name)){
				return c;
			}
		}
		for(Consumable c: Consumable.values()){
			if(c.toString().equalsIgnoreCase(name)){
				return c;
			}
		}
		for(Trophy t: Trophy.values()){
			if(t.toString().equalsIgnoreCase(name)){
				return t;
			}
		}
		for(Toy t: Toy.values()){
			if(t.toString().equalsIgnoreCase(name)){
				return t;
			}
		}
		return null;
	}

	public static void main(String[] args){
		new Logwriter();
		new GUI();		
	}

	public static int getDate(){
		return date;
	}
	public static String getDayString(int day){
		switch(day%7){
		case 1:
			return "Monday";
		case 2:
			return "Tuesday";
		case 3:
			return "Wednesday";
		case 4:
			return "Thursday";
		case 5:
			return "Friday";
		case 6:
			return "Saturday";
		default:
			return "Sunday";
		}
	}
	public static String getIntro() {
		return "Warning This game contains depictions of explicit sexual content. Do not proceed unless you are at least 18 and are comfortable with such content. "
				+ "All characters depicted are over 18 years of age.\n\n"
				+ "You don't really know why you're going to the Student Union in the middle of the night. You'd have to be insane to accept the invitation you received this afternoon. Seriously, someone " +
			"is offering you money to sexfight a bunch girls? You're more likely to get mugged (though you're not carrying any money) or murdered if you show up. Best case scenerio, it's probably a prank " +
			"for gullible freshmen. You have no good reason to believe the invitation is on the level, but here you are, walking into the empty Student Union.\n\n"
			+ "Not quite empty, it turns out. The same " +
			"woman who approached you this afternoon greets you and brings you to a room near the back of the building. Inside, you're surprised to find three quite attractive girls. After comparing notes, " +
			"you confirm they're all freshmen like you and received the same invitation today. You're surprised, both that these girls would agree to such an invitation, and that you're the only guy here. For " +
			"the first time, you start to believe that this might actually happen.\n\n"
			+ "After a few minutes of awkward small talk (though none of these girls seem self-conscious about being here), the woman walks " +
			"in again leading another girl. Embarrassingly you recognize the girl, named Cassie, who is a classmate of yours, and who you've become friends with over the past couple weeks. She blushes when she " +
			"sees you and the two of you consciously avoid eye contact while the woman explains the rules of the competition.\n\n"
			+ "There are a lot of specific points, but the rules basically boil down to this:\n" +
			"*Competitors move around the empty areas of the campus and engage each other in sexfights.\n"
			+ "*When one competitor orgasms, the other gets a point and can claim their clothes.\n"
			+ "*Additional orgasms between those two players are not worth any points until the loser gets a replacement set of clothes at either the Student Union or the first floor of the dorm building.\n "
			+ "*It seems to be customary, but not required, for the loser to get the winner off after a fight, when it doesn't count.\n"
			+ "*After three hours, the match ends and each player is paid for each opponent they defeat, each set of clothes taken, and a bonus for whoever scores the most points.\n\n"
			+ "After the explanation, she confirms with each participant whether they are still interested in participating.\n\n"
			+ "Everyone agrees. The first match starts at exactly 10:00.";
	}

	public static void reset(){
		players.clear();
		flags.clear();
		human=new Player("Dummy");
		gui.purgePlayer();
		gui.createCharacter();
	}
	public static String pickRandom(Object[] array) {		
		return array[Global.random(array.length)].toString();
	}
	public static String pickRandom(Object[] array,Integer[] weights){
		int total=0;
		int choice;
		int count = 0;
		for(Integer w : weights){
			total+=w;
		}
		choice = random(total);
		for(int i=0; i<array.length; i++){
			count += weights[i];
			if(choice <= count){
				return array[i].toString();
			}
		}
		return array[0].toString();
	}
}
