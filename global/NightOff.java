package global;

import java.util.HashMap;

import characters.Attribute;
import characters.Dummy;
import characters.Emotion;
import characters.NPC;
import characters.Player;
import daytime.Scene;

public class NightOff implements Scene {
	private Player player;
	private NPC cassie;
	private NPC mara;
	private Dummy cassieSprite;
	private Dummy maraSprite;
	private plans doing;
	public NightOff(Player player){
		this.player=player;
		this.cassie = Global.getNPC("Cassie");
		this.mara = Global.getNPC("Mara");
		this.doing = plans.NONE;
		Global.current=this;
		Global.gui().clearCommand();
		cassieSprite = new Dummy("Cassie");
		maraSprite = new Dummy("Mara");
		play("Choices");		
	}
	public void offerChoices(){
		Global.gui().message("You could take tonight to relax and recover. Sometimes it's not bad to have a quiet night to yourself.");
		Global.gui().choose("Stay in");
		if(cassie.getAffection(player)>=10){
			Global.gui().message("You don't think Cassie has any plans for tonight. If you want to be a good boyfriend, this would be a good opportunity "
					+ "for a proper date night.");
			Global.gui().choose("Invite Cassie on a date");
		}
		if(mara.getAffection(player)>=12 && player.getPure(Attribute.Science)>=1){
			Global.gui().message("Mara sends you a text that she'll be working on something in Jett's workshop, and could use a hand.");
			Global.gui().choose("Go help Mara");
		}
	}
	@Override
	public void respond(String response) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(this.doing == plans.STAYIN){
			if(response.contains("Power")){
				Global.gui().message("You watch over several of the fights that you weren't present for, focusing on direct combat techniques. "
						+ "There are no amateurish catfights here, "
						+ "these girls can be brutal with each other. You had assumed your testicles were being unfairly persecuted, but you "
						+ "see a surprising number of kicks and knees aimed at the female anatomy. Each painful hit is usually followed by a "
						+ "significant amount of licking and fingering. That probably helps prevent any hard feelings between them.<p>"
						+ "By careful study, you manage to pick up some new grappling techniques that should be useful in future fights. In order "
						+ "to stay focused, you end up jerking off several times, which keeps you from getting as much rest as you planned.<p>"
						+ "<b>Your Power has improved noticeably.</b>");
				player.mod(Attribute.Power, 2);
			}else if(response.contains("Seduction")){
				Global.gui().message("You skim through and watch some of the most intense intimate encounters of the last couple matches. You can "
						+ "tell that despite your experience in the games, these girls still know their way around the female body better than "
						+ "you do. You're pretty sure most of them are bisexual, but even the supposedly straight Cassie looks pretty enthusiastic "
						+ "about eating out her opponents. These videos are effectively the most convincing lesbian porn you've ever seen. The fact that "
						+ "it stars girls you intimately know is just a bonus.<p>"
						+ "Needless to say, you spend most of time watching and rewatching these videos with your cock in your hand, but you also "
						+ "learn a few new tricks for getting a girl off.<p>"
						+ "<b>Your Seduction has improved noticeably.</b>");
				player.mod(Attribute.Seduction, 2);
			}else if(response.contains("Cunning")){
				Global.gui().message("You watch the highlights of the most recent matches, looking for the most cunning tricks you can "
						+ "adopt. Unsurprisingly, Mara is the most proflic trickster, but just about everyone seems to have a few tricks up their "
						+ "sleeves. To some extent, each girl has subtly different tactics for stripping and outmaneuvering their opponents.<p>"
						+ "You're theoretically observing and analyzing strategies, but the abundance of nudity and sex makes it difficult to "
						+ "concentrate. You end up needing to take a couple breaks to masturbate, but you eventually have a short list of tricks "
						+ "you're eager to try out in your next match."
						+ "<b>Your Cunning has improved noticeably.</b>");
				player.mod(Attribute.Cunning, 2);
			}else{
				Global.gui().message("You spend a quiet night relaxing and watching videos on your computer. You end up falling asleep pretty early. "
						+ "It's not the most exciting night, but your body thanks you for the extra rest. When you wake up, you feel more refeshed and "
						+ "focused.<p>"
						+ "<b>Your maximum Stamina and Arousal have increased.</b>");
				player.getStamina().gain(5);
				player.getArousal().gain(5);
			}
			Global.gui().endMatch();
		}
		if(this.doing == plans.CASSIEMOVIE){
			if(response.startsWith("Head back")){
				cassieSprite.undress();
				cassieSprite.setBlush(3);
				cassieSprite.setMood(Emotion.horny);
				Global.gui().loadPortrait(player, cassieSprite);
				Global.gui().message(" You need no further invitation and stand up so quickly that you almost forget to put your dick back in your pants.<p>"
						+ "When you are back inside your room, Cassie almost immediately begins undressing. She is much more fired up than usual after the "
						+ "daring act she pulled in the theater. Her enthusiasm is infectious, and, before you know it, you're both naked on the bed "
						+ "kissing passionately.<p>"
						+ "In a matter of moments, Cassie is straddling you, rubbing her moist pussy against your hardening dick. Your focus is purely on "
						+ "pleasuring her, so you bring one hand up to her breast and move one to your now throbbing erection. You knead her breast while "
						+ "you line up your cock and slowly slide into her burning pussy.<p>"
						+ "Cassie stays still for a moment to adjust to the full warmth, then starts moving her hips, slowly at first. Soon though, she "
						+ "finds the right rhythm and begins speeding up. You bring your second hand up to play both of her cute nipples, spurring her "
						+ "on even further.<p>"
						+ "After several minutes, you both are nearing climax. Cassie picks up her bouncing to a feverish pace, and you begin thrusting "
						+ "to match, taking hold of her hips to time it. Each time your pelvises pound together, a shockwave of pleasure rolls over both "
						+ "of you. Cassie is quickly overwhelmed by these quakes, and cums hard on your dick. You keep pounding away at the beautiful "
						+ "woman on top of you, and quickly reach climax yourself. With one last mighty thrust, you blast your load inside her, giving her "
						+ "a second powerful orgasm.<p>"
						+ "As the two of you begin catching your breath, Cassie leans down to plant a gentle kiss on your lips. You wrap her in your arms "
						+ "and bring her close while basking in the afterglow of your love. The two of you stay together like that for some time. "
						+ "Eventually, she breaks the embrace. She dresses and leaves the room, but only long enough to clean up a little. After a minute, "
						+ "she returns and lies down next to you. You take the opportunity to ask her if she enjoyed the date, even though it didn't end up "
						+ "being very romantic.<p>"
						+ "She quietly replies, <i>\"Well, I hadn't really thought about it until recently, but I guess sex is pretty "
						+ "romantic itself. And the movie was boring anyway, so no loss.\"</i> After all that's happened, you can't help but agree. By now "
						+ "it's late enough that Cassie sees no reason to get up, so the two of you lie together until you both fall asleep.");
			}else{
				cassieSprite.undress();
				cassieSprite.setBlush(3);
				cassieSprite.setMood(Emotion.horny);
				Global.gui().loadPortrait(player, cassieSprite);
				Global.gui().message("Cassie's eyes widen in surprise for a moment before she slips down to the floor, between your knees. She quickly "
						+ "removes her shirt and her bra, giving you free access to her boobs as she starts to lick the tip of your cock.<p>"
						+ "Between the skillful ministrations of the beautiful woman in front of you and the idea of being noticed by the people sitting "
						+ "merely 20 feet away, you don't last very long. You quietly tell Cassie that you're about to cum, and she responds by taking "
						+ "the length of your shaft into her mouth until the head is poking at the back of her throat. The rush of warmth and pressure "
						+ "sends you over the edge, and you blow your load down Cassie's eager throat.<p>"
						+ "Before you have a chance to catch your breath, Cassie stands up and brings her face close to yours. She swallows deeply, making "
						+ "sure that you can hear the last bit of your cum make its way down her throat.<p>"
						+ "Cassie is clearly not satisfied yet. She quickly throws on her shirt and stuffs her bra into her purse as she leads you away from "
						+ "your seats. In the light of the lobby, you can see that Cassie is much more fired up than usual, so much so that she's turning "
						+ "red in the face. She keeps pulling you along, but not toward the exit. She looks around quickly to make sure no one is looking "
						+ "and drags you into the women's restroom. The restroom is empty and likely will be until the movie finishes, so she begins "
						+ "undressing on the spot.<p>"
						+ "While you remove your pants, Cassie hops onto the restroom counter and spreads her legs. She begins rubbing her moist pussy "
						+ "in anticipation as you move over to her. She has herself so worked up that she nearly cums as soon as you slide your cock into "
						+ "her. You don't give her a chance to breathe, though, as you find the rhythm of your thrusts and begin to pick up the pace. In "
						+ "a matter of moments, Cassie is teetering on the edge of orgasm, so you decide to push her over. You begin to rub her clit, and "
						+ "she reacts without thinking by frantically playing with her nipples. She cums hard, and the increased tightness of her folds "
						+ "sends you over the edge with her. You unload your cum in her hot pussy for what seems like an eternity.<p>"
						+ "After you finish, she wraps her legs around you to keep you inside her. By now the movie is probably almost finished, but you "
						+ "are both more worried about holding each other close than the people who might catch you. Now that she has calmed down again, "
						+ "Cassie gives you a gentle kiss on the lips.<p>"
						+ "After a few minutes of cuddling, you reluctantly break your embrace and pull out of her. The two of you quickly clean up and "
						+ "dress, and make your way out of the restroom before anyone else comes in. In the lobby, you see people leaving the movie, "
						+ "some of them making their way toward the restrooms. Cassie can't help but giggle at how close you came to being found out. "
						+ "Now that you have a chance, you ask Cassie if that date was really her idea of romantic.<p>"
						+ "She says, <i>\"Well, I hadn't really thought about it until recently, but I guess sex is pretty romantic itself. And the "
						+ "movie was boring anyway, so no loss.\"</i> After all that's happened, you can't help but agree. You plant another kiss on "
						+ "Cassie's lips, thinking it will be a goodbye kiss, but it lingers longer than you intended. Before you know it, you are inviting "
						+ "her back to your room to further \"discuss\" the intricacies of romance.");
			}
			cassie.gainAffection(player, 3);
			player.gainAffection(cassie, 3);
			Global.gui().endMatch();
		}
		if(this.doing == plans.MARATESTING){			
			if(response.contains("down")){
				maraSprite.setBlush(3);
				maraSprite.setMood(Emotion.confident);
				Global.gui().loadPortrait(player, maraSprite);
				Global.gui().message("You turn the vibration down to about 20%. Mara starts giving a more detailed explanation of the design of her "
						+ "multitool, directing Jett�fs attention back to it. She glances your direction and gives an almost imperceptible nod. You "
						+ "take that to mean this level is manageable for now.<p>"
						+ "She pulls a stool up next to Jett�fs workbench and sits down. You shouldn�ft have to worry about her ability to stay "
						+ "standing now. You wait until you�fre sure Jett is completely focused on testing the device, then turn up the vibration a "
						+ "little bit. Mara doesn�ft visibly react this time. She�fs getting better at this.<p>"
						+ "You gradually continue to increase the vibration intensity as Mara and Jett continue working on her multitool. You "
						+ "notice her squirming on the stool, but it�fs probably only because you�fre looking for it. Jett still doesn�ft seem to "
						+ "have caught on.<p>"
						+ "<i>\"Even at a consistent wattage, is this even safe in concept?\"</i> Jett gives Mara a stern look. <i>\"The last "
						+ "thing you want to do is sterilize your targets.\"</i> He has to be at least a little suspicious by now. You can tell "
						+ "how flushed Mara is from here, and he�fs sitting right next to her. You consider turning the vibrator down again, "
						+ "but if you don�ft let her cum, the test will never finish.<p>"
						+ "<i>\"I-I wasn�ft planning to use it on anyone until I finished researching the safety considerations.\"</i> Mara "
						+ "suddenly breaks into a minor coughing fit. You notice her hand under the workbench, trying to signal you outside "
						+ "of Jett�fs line of sight. You�fre not 100% sure what she�fs trying to indicate, but it looks urgent.<p>"
						+ "Did she finish already? Is she coughing to conceal her orgasm? You recall that Mara gets very sensitive after "
						+ "orgasming. If she just came, that would explain the urgency of her gestures. You shut off the remote to be safe.<p>"
						+ "Mara looks relieved and reassures Jett that she is ok. When he isn�ft paying attention, she throws a quick wink "
						+ "in your direction. Looks like you made the right call.<p>"
						+ "Jett removes the new attachment from Mara�fs multitool and hands it back to her. <i>\"Sorry Mara, but I think "
						+ "this �epleasure beam�f of yours isn�ft a viable design. I can tell the idea has you pretty worked up, but it doesn�ft "
						+ "seem safe or practical.\"</i><p>"
						+ "Mara cheerfully excuses herself and practically skips back to you. Jett looks a little confused at her attitude, "
						+ "given his rejection of her prototype, but he shrugs and resumes his work. Mara cozies up next to you and gives "
						+ "you a quick kiss on the cheek.<p>"
						+ "<i>\"Mission accomplished.\"</i> She whispers seductively in your ear. <i>\"That felt so good.\"</i> You were "
						+ "focused on your task, but it�fs actually pretty exciting to think you just secretly made Mara orgasm in front of "
						+ "someone. It would have been more satisfying to do it with a more �ehands on�f approach, but knowing you can pleasure "
						+ "her with a button press is pretty hot in its own way.<p>"
						+ "You congratulate Mara on a successful field test and try to pass her back the remote. However, she grins and shakes "
						+ "her head. She snuggles up closer to you and nibbles gently on your ear.<p>"
						+ "<i>\"You hold onto it.\"</i> Her whisper is hot with anticipation. <i>\"Surprise me.\"</i>");
			}else{
				maraSprite.setBlush(3);
				maraSprite.setMood(Emotion.desperate);
				Global.gui().loadPortrait(player, maraSprite);
				Global.gui().message("When you turn the remote up to maximum, you see a shudder run through Mara�fs body. She bites her lip to "
						+ "suppress a moan and her knees buckle. You quickly shut off the remote, but that was suspicious as hell. There�fs no "
						+ "way Jett missed that.<p>"
						+ "Jett lets out a patient sigh and pushes the multitool to the side. <i>\"Ok Mara, show me your <u>other</u> new toy.\"</i>"
						+ " He gestures sternly to an open space on the workbench. Well, shit. You�fre busted now.<p>"
						+ "Mara sheepishly reaches into her pants to retrieve the vibrator. Unfortunately you�fre holding half the toy, so you "
						+ "have to come forward too. You walk up to Jett�fs workbench and hand over the remote. He raises an eyebrow at you, "
						+ "but says nothing.<p>"
						+ "Mara elbows you in the ribs for messing up and places the egg on the workbench. It�fs still visibly wet with her "
						+ "juices, but Jett picks it up without hesitation and begins disassembling the casing. Mara turns bright red as she "
						+ "watches her mentor casually handle something that was just inside her.<p>"
						+ "Long awkward seconds pass as Jett examines the device. <i>\"This is a very expensive motor for such a basic sex toy. "
						+ "What�fs the point?\"</i> He pins Mara with his stare, while she fidgets embarrassedly.<p>"
						+ "<i>\"It�fs very quiet, so you can use it in public without getting caught.\"</i> Mara is much more quiet and subdued "
						+ "than usual.<p>Jett�fs stern expression breaks as he lets out a snort of amusement. <i>\"It�fs quiet, but you�fre not. "
						+ "As soon as you start moaning, you�fre going to get caught.\"</i> He passes the egg back to Mara and she pockets it "
						+ "with another blush. <i>\"I don�ft see any practical use for this in the Games, but you two are free to "
						+ "have your fun.\"</i><p>"
						+ "You and Mara quietly retreat after Jett dismisses you. Outside the workshop, Mara slumps against you, thoroughly "
						+ "defeated.<p>"
						+ "<i>\"I should have picked a different test subject. I didn�ft expect getting caught by Jett to be so embarrassing. "
						+ "I�fm going to remember that everytime I see him now.\"</i> You pat her on the head reassuringly. At least Jett wasn�ft "
						+ "particularly angry. Besides, Mara usually goes out of her way to try to appear sexier and more seductive. This could "
						+ "potentially help.<p>"
						+ "She slaps you lightly on the arm. <i>\"I only do that for you, dumbass! You�fre my boy, so I want you to think I�fm sexy. "
						+ "Jett is my teacher, so I want him to respect me for my brain.\"</i> She moves from leaning on your arm to leaning on "
						+ "your chest; a significantly more huggable position, which you take full advantage of.<p>"
						+ "<i>\"It would be nice if you respect me for my brain too, but I don�ft want Jett thinking sexy thoughts about me. "
						+ "That�fll just make things complicated.\"</i> You give her a tender kiss on the lips, which seems to cheer her up.<p>"
						+ "<i>\"Since we got caught, I didn�ft get to cum.\"</i> She smiles up at you seductively. <i>\"Can we return to your room "
						+ "and fix that?\"</i>");
			}
			mara.gainAffection(player, 3);
			player.gainAffection(mara, 3);
			Global.gui().endMatch();
		}
		if(response.startsWith("Stay in")){
			Global.gui().message("You settle in for a quiet night alone. You've worked up a real sleep deficit this week. When you check your email, you notice that "
					+ "you've been sent recordings of the recent matches. If you wanted to be particularly diligent, you could probably learn quite a bit by studying "
					+ "your opponents' techniques. Of course, just glancing over one of the videos has you pretty excited. You probably won't be able to calm down "
					+ "very well while watching these.<p>"
					+ "You could focus on studying combat techniques, sexual techniques, or clever tricks. However, it would probably be beneficial to both body "
					+ "and mind if you spend all night just resting.");
			this.doing = plans.STAYIN;
			Global.gui().choose("Study Power techniques");
			Global.gui().choose("Study Seduction techniques");
			Global.gui().choose("Study Cunning techniques");
			Global.gui().choose("Rest");			
		}
		if(response.startsWith("Invite Cassie")){
			cassieSprite.dress();
			cassieSprite.setBlush(1);
			cassieSprite.setMood(Emotion.horny);
			Global.gui().loadPortrait(player, cassieSprite);
			this.doing = plans.CASSIEMOVIE;
			Global.gui().message("Dinner and a movie isn't exactly Cassie's ideal romantic date, but it is a close second, and a weekend in Paris "
					+ "wasn't really reasonable. The two of you have reservations at a nearby Italian restaurant. As you arrive, the atmosphere "
					+ "is perfect. Cassie practically glows with excitement at the taste of amour in the air. You glow with excitement at the "
					+ "taste of spaghetti in your mouth.<p>"
					+ "The conversation turns to topics that seem \"right\" to discuss on a romantic date, "
					+ "such as hopes and dreams for the future. Neither of you have given much thought to such things, though, since you've "
					+ "both been focused on the Games, so the conversation dries up quickly. Cassie suggests finishing up and heading to an "
					+ "earlier showing of the movie you were planning on seeing.<p>"
					+ "The movie itself is nothing special. It's standard romcom fare, and the theater is mostly empty. However, the lonely "
					+ "dark is still the height of romance for Cassie, as if being so close together is a treat enough. She snuggles close "
					+ "to you as soon as you get into your seats.<p>"
					+ "As the movie gets underway, though, you notice Cassie fidgeting and fussing more and more. She grows more impatient "
					+ "throughout the movie, and, around the halfway mark, sits upright. By now, Cassie has realized why she is restless, and "
					+ "you realize it too when she moves her hand onto your crotch. The two of you are sitting in the back of the theater with "
					+ "few people around, so you don't try move her hand away.<p>"
					+ "Before she really thinks about what she's doing, she pulls your dick out of your pants and begins gently rubbing. You "
					+ "follow her lead and slide your hand into her shirt, copping a feel of her breast. After a minute or two of gentle "
					+ "fondling, Cassie starts getting restless again. She's torn between thrill and terror at the idea of being caught giving "
					+ "a handjob in public, but her shyness shortly wins out. She whispers in your ear, "
					+ "<i>\"Let's get back to your room, so we can really go all out.\"</i>");
			Global.gui().choose("Head back","Some good times surely await you back at your dorm room.");
			if(cassie.getRank()>=2){
				Global.gui().choose("Stay here","Why not stay here? The theatre is almost empty, and Cassie can't be that shy after all you've done together.");
			}
		}
		if(response.startsWith("Go help Mara")){
			maraSprite.dress();
			maraSprite.setBlush(2);
			maraSprite.setMood(Emotion.confident);
			Global.gui().loadPortrait(player, maraSprite);
			this.doing = plans.MARATESTING;
			Global.gui().message("You head over to Jett�fs workshop to tune up your equipment, and more importantly, spend some time with Mara. "
					+ "She seems hard at work assembling some new device, but immediately greets you excitedly when you arrive.<p>"
					+ "Jett is also working on something in the back of the lab, but he just gives you a cordial nod before returning to whatever "
					+ "he�fs doing.<p>"
					+ "Mara is the one you�fre here to see, so you sit down next to her at the workbench and offer to lend to hand. However, she "
					+ "immediately covers up the parts she�fs working on until you back off dejectedly.<p>"
					+ "<i>\"Not yet!\"</i> Mara stands up some scrap material as in improvised wall, so she can continue to work in secret. "
					+ "<i>\"It�fs a surprise.\"</i><p>"
					+ "Jett glances up from his own workbench. <i>\"She won�ft tell me what she�fs up to either. Do me a favor and yell if you see "
					+ "her pick up anything explosive.\"</i><p>"
					+ "With your offer to help firmly rejected, you decide to make the best of your time and tune up your own equipment. It�fs weird "
					+ "though. Mara invited you to come collaborate, but you�fre each working separately.<p>"
					+ "�c<p>"
					+ "An hour passes in relative silence, before Mara sidles up to you, excitedly. She whispers to you quietly so that Jett won�ft "
					+ "notice, but mischief is still clear in her voice and expression.<i>\"It�fs done. Take a look.\"</i> She opens her hands "
					+ "secretively to show you what she�fs holding. It�fs a small plastic ovaloid and an adjustable remote. It looks exactly like a "
					+ "vibrating egg you�fd see in a sex shop.<p>"
					+ "<i>\"It basically is, but those eggs don�ft work like they do in book or movies.\"</i> She�fs presumably referring to porn. "
					+ "There can�ft be that many mainstream books and movies with sex toys. <i>\"The motor buzzes really loudly when they�fre on, "
					+ "so you�fd never be able to use them in public without someone noticing. This one is basically silent though.\"</i><p>"
					+ "She hits the switch to demonstrate. The egg vibrates in her palm, but you can barely hear it. Ok, that�fs kinda cool. She�fs "
					+ "built a better vibrator. You�fd be happy to indulge her with some public naughtiness later, but it still doesn�ft explain why "
					+ "she�fs being so secretive.<p>"
					+ "<i>\"Well, it wouldn�ft be an effective test if the subject knew what to look for.\"</i> She meaningfully nods in Jett�fs "
					+ "direction while covertly pushing the egg and remote into your hand. <p>"
					+ "<i>\"Put it inside me while you pick this up.\"</i> She casually nudges a screwdriver off the workbench, causing it to fall "
					+ "underneath. You crouch down to retrieve the screwdriver. While your actions are shielded from Jett�fs line of sight, you "
					+ "quickly slip a hand inside Mara�fs panties and place the toy at her hot entrance. If you weren�ft so used to handling her "
					+ "nethers, you probably couldn�ft do it as smoothly, but you�fre pretty sure Jett didn�ft notice.<p>"
					+ "You flick on the remote on the lowest setting to test it out. Mara jumps slightly at the initial sensation, but there�fs "
					+ "no other indication that she has a vibrator between her legs. She takes a deep breath to compose herself and picks up "
					+ "her multitool off a nearby workbench.<p>"
					+ "<i>\"Let�fs see if I can hold a conversation and �efinish�f without Jett noticing anything.\"</i> She walks over to the "
					+ "workbench where her mentor is sitting. Since you know what to look for, you notice she�fs walking slightly awkwardly, "
					+ "but she�fs doing a pretty good job hiding it. You pretend to continue working, so it�fs not obvious you�fre watching.<p>"
					+ "<i>\"Hey Jett, I�fve been unstable power usage from my multitool since I added this new attachment. Can you take a look "
					+ "at it?\"</i> She sets down the device on Jett�fs workbench and he sets aside his own work to take a look at it. He doesn�ft "
					+ "show any sign of suspicion. So far so good.<p>"
					+ "<i>\"I assume this is what you�fve been working so secretly on?\"</i> He leans in to take a closer look. <i>\"This looks "
					+ "like an electron gun. What is it supposed to do?\"</i> He�fs focused on the hardware now, so you take this opportunity to "
					+ "turn up intensity to about 50%. Mara�fs breathing catches, but Jett doesn�ft appear to notice.<p>"
					+ "<i>\"I-It generates a "
					+ "localized stream of charged particles that causes a tingling sensation on skin.\"</i> Mara looks a little flushed now, "
					+ "and she�fs fidgeting in place. <i>\"I think it could be effective on exposed g-genitals.\"</i> She starts to lean against "
					+ "the workbench. She may be having a bit of trouble standing.<p>"
					+ "Jett glances up from the device <i>\"I sure as hell wouldn�ft want an electron gun pointed at my junk, especially if you�fre "
					+ "having power regulation issues.\"</i> He gives her a suspicious look. You can�ft tell whether it�fs because he�fs concerned "
					+ "about the new attachment on Mara�fs multitool or he noticed her unusual behavior. Her knees have started shaking a little "
					+ "and she�fs more visibly flushed.<p>"
					+ "This prolonged stimulation seems to be taking its toll on Mara�fs composure. You should probably do something. You could "
					+ "either turn the vibration down to let her recover, or turn it up to finish her (and the test) faster.");
			Global.gui().choose("Turn it down");
			Global.gui().choose("Turn it up");
		}

	}
	@Override
	public boolean play(String response) {
		if(response.startsWith("Choices")){
			Global.gui().message("There are no matches on Sunday nights. While the Games have guaranteed you an eventful sex life, they've "
					+ "really dominated your schedule. This is the only night each week you have to yourself.");
			offerChoices();
		}
		return false;
	}
	@Override
	public String morning() {
		return "";
	}
	@Override
	public String mandatory() {
		return "";
	}
	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		// TODO Auto-generated method stub
		
	}
	private enum plans{
		NONE,
		STAYIN,
		LILLYDRINKS,
		CASSIEMOVIE,
		MARATESTING
		
	}

}
