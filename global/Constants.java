package global;

import java.awt.Color;

public class Constants {
	public static int STARTINGPOWER = 5;
	public static int STARTINGSEDUCTION = 5;
	public static int STARTINGCUNNING = 5;
	public static int STARTINGPERCEPTION = 5;
	public static int STARTINGSPEED = 5;
	public static int STARTINGSTAMINA = 25;
	public static int STARTINGAROUSAL = 50;
	public static int STARTINGMOJO = 35;
	public static int STARTINGSTATPOINTS = 2;
	public static int STARTINGCASH = 50;
	public static int EASYSPEED = 10;
	public static int EASYSTAMINA = 75;
	public static int EASYAROUSAL = 100;
	public static int EASYMOJO = 50;
	public static int EASYSTATPOINTS = 10;
	public static int EASYCASH = 5000;
	public static int TRAININGSCALING = 100;
	
	public static Color PRIMARYFRAMECOLOR = new Color(240,238,235);
	public static Color PRIMARYBGCOLOR = new Color(255,250,240);
	public static Color PRIMARYTEXTCOLOR = new Color(20,20,40);
	
	public static Color ALTFRAMECOLOR = new Color(40,62,76);
	public static Color ALTBGCOLOR = new Color(20,42,56);
	public static Color ALTTEXTCOLOR = new Color(220,220,224);
	
}
