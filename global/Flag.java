package global;

public enum Flag {
	//Introductions
	metBroker,
	metAisha,
	metRin,
	metJett,
	metYui,
	metAlice,
	metSuzume,
	metLilly,
	
	//Information
	basicStores,
	blackMarket,
	meditation,
	girlAdvice,
	CassieKnown,
	JewelKnown,
	AngelKnown,
	MaraKnown,
	magicstore,
	blackMarketPlus,
	dojo,
	workshop,
	darkness,
	fetishism,
	
	//Story
	Cassievg1,
	Cassievg2,
	CassieMaravg,
	challengeAccepted,
	Maravg1,
	maravg2,
	MaraDayOff,
	MaraRoute,
	rank1,
	furry,
	catspirit,
	YuiAvailable,
	YuiUnlocking,
	YuiLoyalty,
	Clue1,
	
	//Unlocked NPCs
	Eve,
	Kat,
	Reyka,
	Samantha,
	Maya,
	Yui,
	
	//NPC Counters
	CassieLoneliness,
	CassieDWV,	
	JewelDWV,
	AngelDWV,
	MaraDWV,
	KatDWV,
	ReykaDWV,
	YuiAffection,
	CarolineAffection,
	YuiDWV,
	ChallengesCompleted,
	HandicapMatches,
	
	
	//Options
	autosave,
	noimage,
	exactimages,
	noportraits,
	portraits2,
	altcolors,
	largefonts,
	noReports,
	statussprites,
	
	//Difficulty
	hardmode,
	challengemode,
	doublexp,
	shortmatches,
	
	//Daily
	threesome,
	victory,
	AliceAvailable,
	night,
	
	//Progress
	VGskill
	 
}
