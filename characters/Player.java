package characters;
import items.Clothing;
import items.ClothingType;
import items.Component;
import items.Consumable;
import items.Flask;
import items.Item;
import items.Potion;
import items.Trophy;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.Icon;

import skills.Skill;
import skills.Cunnilingus;
import skills.Escape;
import skills.Finger;
import skills.Flick;
import skills.Focus;
import skills.FondleBreasts;
import skills.Fuck;
import skills.Kiss;
import skills.Knee;
import skills.LickNipples;
import skills.Maneuver;
import skills.Nothing;
import skills.PerfectTouch;
import skills.Recover;
import skills.Restrain;
import skills.Shove;
import skills.Slap;
import skills.Spank;
import skills.Straddle;
import skills.StripBottom;
import skills.StripTop;
import skills.Struggle;
import skills.SuckNeck;
import skills.Tackle;
import skills.Tactics;
import skills.Tickle;
import skills.Trip;
import stance.Behind;
import status.Feral;
import status.Horny;
import status.Status;
import status.Enthralled;
import status.Stsflag;
import trap.Trap;
import global.Global;
import global.Modifier;
import gui.ActionButton;
import gui.GUI;
import combat.Combat;
import combat.Encounter;
import combat.Result;
import actions.Action;
import actions.Leap;
import actions.Move;
import actions.Shortcut;
import areas.Area;
import areas.Deployable;


public class Player extends Character {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7547460660118646782L;
	public GUI gui;
	public int attpoints;
	public HashSet<Area> pings;

	public Player(String name) {
		super(name, 1);
		add(Trait.male);
		outfit[0].add(Clothing.Tshirt);
		outfit[1].add(Clothing.boxers);
		outfit[1].add(Clothing.jeans);
		closet.add(Clothing.Tshirt);
		closet.add(Clothing.boxers);
		closet.add(Clothing.jeans);
		change(Modifier.normal);
		attpoints=0;
		setUnderwear(Trophy.PlayerTrophy);
		pings = new HashSet<Area>();
	}

	@Override
	public String describe(int x) {
		String description="<i>";
		for(Status s:status){
			if(s.describe()!=null&&s.describe()!=""){
				description = description+s.describe()+"<br>";
			}
		}
		description = description+"</i>";
		if(top.empty()&&bottom.empty()){
			description = description+"You are completely naked.";
		}
		else{
			if(top.empty()){
				description = description+"You are shirtless and ";
				if(!bottom.empty()){
					description=description+"wearing ";
				}
			}
			else{
				description = description+"You are wearing a "+top.peek().getName()+" and ";
			}
			if(bottom.empty()){
				description = description+"are naked from the waist down.";
			}
			else{
				description = description+bottom.peek().getName()+".";
			}
		}
		return description;
	}

	@Override
	public void victory(Combat c, Result flag) {
		Character target;
		target = c.getOther(this);
		this.gainXP(15+lvlBonus(target));
		target.gainXP(10+target.lvlBonus(this));
		if(c.stance.penetration(this)){
			getMojo().gain(2);
			if(has(Trait.mojoMaster)){
				getMojo().gain(2);
			}
		}
		if(!has(Trait.nosatisfaction)){
			arousal.empty();
		}		
		undress(c);
		target.undress(c);
		target.giveTrophy(this, c);
		dress(c);
		target.defeated(this);
		c.getOther(this).defeat(c,flag);
		gainAttraction(target,1);
		target.gainAttraction(this,2);
		if(has(Trait.insatiable)){
			arousal.set(Math.round(arousal.max()*.5f));
		}

	}

	@Override
	public void defeat(Combat c, Result flag) {
		c.write("Bad thing");
	}

	@Override
	public void act(Combat c) {
		gui.clearCommand();
		Character target;
		if(c.p1==this){
			target=c.p2;
		}
		else{
			target=c.p1;
		}
		
		ArrayList<Skill> skillArray = new ArrayList<Skill>();
		ArrayList<Skill> demandsArray = new ArrayList<Skill>();
		ArrayList<Skill> flaskArray = new ArrayList<Skill>();
		ArrayList<Skill> potionArray = new ArrayList<Skill>();
		
		for(Skill a:skills){
			if (a.usable(c,  target)){
				if (a.type()==Tactics.demand){
					demandsArray.add(a);
				}
				else if (a.type()==Tactics.damage){
					if (Global.getMatch().condition!=Modifier.pacifist){
						skillArray.add(a);
					}
				}
				else{
					skillArray.add(a);
				}
			}
		}
		
		for (Skill s:flaskskills)
			if (s.usable(c,  target)){
				flaskArray.add(s);
			}
		for (Skill s:potionskills)
			if (s.usable(c,  target)){
				potionArray.add(s);
			}
		
		// sort them.
		Collections.sort(skillArray);
		Collections.sort(demandsArray);
		Collections.sort(flaskArray);
		Collections.sort(potionArray);
		
		// add them to the gui.
		for(Skill s:skillArray)
			gui.addSkill(c, s);
		for(Skill s:demandsArray)
			gui.addDemands(s, c);
		for(Skill s:flaskArray)
			gui.addFlasks(s, c);
		for(Skill s:potionArray)
			gui.addPotions(s, c);
		// change end -GD
		
		gui.showSkills();
	}

	@Override
	public boolean human() {
		return true;
	}

	@Override
	public void draw(Combat c, Result flag) {
		arousal.empty();
		if(c.stance.penetration(this)){
			c.p1.getMojo().gain(3);
			c.p2.getMojo().gain(3);
		}
		if(c.p1.human()){
			c.p2.draw(c,flag);
		}
		else{
			c.p1.draw(c,flag);
		}

	}
	public void change(Modifier rule){
		if(rule==Modifier.nudist){
			top.clear();
			bottom.clear();
		}
		else if(rule==Modifier.pantsman){
			top.clear();
			bottom.clear();
			for(Clothing article: outfit[Player.OUTFITBOTTOM]){
				if(article.getType()==ClothingType.UNDERWEAR){
					wear(article);
				}
			}
			for(Clothing article: outfit[Player.OUTFITTOP]){
				if(article.getType()==ClothingType.TOPUNDER){
					wear(article);
				}
			}
		}
		else{
			top=(Stack<Clothing>) outfit[Player.OUTFITTOP].clone();
			bottom=(Stack<Clothing>) outfit[Player.OUTFITBOTTOM].clone();
		}
	}
	
	@Override
	public String bbLiner() {
		return null;
	}

	@Override
	public String nakedLiner() {
		return null;
	}

	@Override
	public String stunLiner() {
		return null;
	}

	@Override
	public String winningLiner() {
		return null;
	}

	@Override
	public String taunt() {
		return null;
	}

	@Override
	public void detect() {
		pings.clear();
		for(Area distant:Global.getMatch().getAreas()){
			if(distant != location && !location.adjacent.contains(distant) && distant.alarmTriggered()){
				Global.gui().message("You can faintly hear a far-away noise from the <b>"+distant.name+"</b>.");
				pings.add(distant);
			}
		}
		for(Area adjacent:location.adjacent){
			if(adjacent.ping(get(Attribute.Perception))){
				Global.gui().message("You hear something in the <b>"+adjacent.name+"</b>.");
				pings.add(adjacent);
			}
		}
	}
	public boolean opponentDetected(Area location){
		return pings.contains(location);
	}
	@Override
	public void faceOff(Character opponent, Encounter enc) {
		gui.message("You run into <b>"+opponent.name+"</b> and you both hesitate for a moment, deciding whether to attack or retreat.");
		assessOpponent(opponent);
		for(Character nearby: location.present){
			if(nearby!=this && nearby!=opponent){
				gui.message("<b>"+nearby.name+"</b> is watching nearby and may get involved.");
			}
		}
		gui.promptFF(enc,opponent);
	}

	private void assessOpponent(Character opponent){
		String arousal;
		String stamina;
		if(opponent.state==State.webbed){
			gui.message("She is naked and helpless<br>");
			return;
		}
		if(get(Attribute.Perception)>=6){
			gui.message("She is level "+opponent.getLevel());
		}
		if(get(Attribute.Perception)>=8){
			gui.message("Her Power is "+opponent.get(Attribute.Power)+", her Cunning is "+opponent.get(Attribute.Cunning)+", and her Seduction is "+opponent.get(Attribute.Seduction));
		}
		if(opponent.nude()||opponent.state==State.shower){
			gui.message("She is completely naked.");
		}
		else{
			gui.message("She is dressed and ready to fight.");
		}
		if(get(Attribute.Perception)>=4){
			if(opponent.getArousal().percent()>70){
				arousal="horny";
			}
			else if(opponent.getArousal().percent()>30){
				arousal="slightly aroused";
			}
			else{
				arousal="composed";
			}
			if(opponent.getStamina().percent()<50){
				stamina="tired";
			}
			else{
				stamina="eager";
			}
			gui.message("She looks "+stamina+" and "+arousal+".");
		}
	}
	@Override
	public void spy(Character opponent, Encounter enc) {
		gui.message("You spot <b>"+opponent.name+"</b> but she hasn't seen you yet. You could probably catch her off guard, or you could remain hidden and hope she doesn't notice you.");
		assessOpponent(opponent);
		for(Character nearby: location.present){
			if(nearby!=this && nearby!=opponent){
				gui.message("<b>"+nearby.name+"</b> is watching nearby and may get involved.");
			}
		}
		gui.promptAmbush(enc, opponent);
	}

	@Override
	public void move() {
		gui.clearCommand();
		if(state==State.combat){
			if(!location.fight.battle()){
				Global.getMatch().resume();
			}
		}
		else if(busy>0){
			busy--;
		}
		else if (this.is(Stsflag.enthralled)) {
			Character master;
			master = ((Enthralled)getStatus(Stsflag.enthralled)).master;
			if(master!=null){
			Move compelled = findPath(master.location());
			gui.message("You feel an irresistible compulsion to head to the <b>"
					+ master.location().name+"</b>");
			if (compelled != null)
				this.gui.addAction(compelled, this);
			}
		}
		else if(state==State.shower||state==State.lostclothes){
			bathe();
		}
		else if(state==State.crafting){
			craft();
		}
		else if(state==State.searching){
			search();
		}
		else if(state==State.resupplying){
			resupply();
		}
		else if(state==State.webbed){
			gui.message("You eventually manage to get an arm free, which you then use to extract yourself from the trap.");
			state=State.ready;
		}
		else if(state==State.masturbating){
			masturbate();
		}
		else{
			gui.messageHead(location.description+"<p>");
			for(Deployable trap: location.env){
				if(trap.owner()==this){
					gui.message("You've set a "+trap.toString()+" here.");
				}
			}
			detect();
			if(!location.encounter(this)){
				for(Area path:location.adjacent){
					gui.addAction(new Move(path),this);
				}
				if(getPure(Attribute.Cunning)>=28){
					for(Area path:location.shortcut){
						gui.addAction(new Shortcut(path),this);
					}
				}
				if(getPure(Attribute.Ninjutsu)>=5){
					for(Area path:location.jump){
						gui.addAction(new Leap(path),this);
					}
				}
				for(Action act:Global.getActions()){
					if(act.usable(this)){
						gui.addAction(act,this);
					}
				}
			}
		}
	}
	public void ding(){
		xp-=95+(level*5);
		level++;
		attpoints+=2;
		gui.clearText();
		gui.message("You've gained a Level!<br>Select which attributes to increase.");
		gui.ding();
	}
	public void flee(Area location2) {
		Area[] adjacent = location2.adjacent.toArray(new Area[location2.adjacent.size()]);
		Area destination = adjacent[Global.random(adjacent.length)];
		gui.message("You dash away and escape into the <b>"+destination.name+"</b>");
		travel(destination);
		location2.endEncounter();
	}
	public void bathe(){
		status.clear();
		stamina.fill();
		if(location.name=="Showers"){
			gui.message("You let the hot water wash away your exhaustion and soon you're back to peak condition");
		}
		if(location.name=="Pool"){
			gui.message("The hot water soothes and relaxes your muscles. You feel a bit exposed, skinny-dipping in such an open area. You decide it's time to get moving.");
		}
		if(state==State.lostclothes){
			gui.message("Your clothes aren't where you left them. Someone must have come by and taken them.");
		}
		state=State.ready;
	}

	public void masturbate(){
		gui.message("You hurriedly stroke yourself off, eager to finish before someone catches you. After what seems like an eternity, you ejaculate into a tissue and " +
				"throw it in the trash. Looks like you got away with it.");
		arousal.empty();
		state=State.ready;
	}

	@Override
	public void showerScene(Character target, Encounter encounter) {
		if(target.location().name=="Showers"){
			gui.message("You hear running water coming from the first floor showers. There shouldn't be any residents on this floor right now, so it's likely one " +
					"of your opponents. You peek inside and sure enough, <b>"+target.name()+"</b> is taking a shower and looking quite vulnerable. Do you take advantage " +
					"of her carelessness?");
		}
		else if(target.location().name=="Pool"){
			gui.message("You stumble upon <b>"+target.name+"</b> skinny dipping in the pool. She hasn't noticed you yet. It would be pretty easy to catch her off-guard.");
		}
		assessOpponent(target);
		gui.promptShower(encounter, target);
	}

	@Override
	public void intervene(Encounter enc, Character p1, Character p2) {
		gui.message("You find <b>"+p1.name()+"</b> and <b>"+p2.name()+"</b> fighting too intensely to notice your arrival. If you intervene now, it'll essentially decide the winner.");
		gui.promptIntervene(enc,p1,p2);
	}
	public boolean resist3p(Combat combat, Character intruder, Character assist){
		if(has(Trait.cursed)){
			return true;
		}
		else{
			return false;
		}
	}
	@Override
	public void intervene3p(Combat c, Character target, Character assist) {
		this.gainXP(20+lvlBonus(target));
		target.defeated(this);
		assist.gainAttraction(this, 1);
		c.write("You take your time, approaching "+target.name()+" and "+assist.name()+" stealthily. "+assist.name()+" notices you first and before her reaction " +
				"gives you away, you quickly lunge and grab "+target.name()+" from behind. She freezes in surprise for just a second, but that's all you need to " +
				"restrain her arms and leave her completely helpless. Both your hands are occupied holding her, so you focus on kissing and licking the " +
				"sensitive nape of her neck.<p>");
	}

	@Override
	public void victory3p(Combat c, Character target, Character assist) {
		this.gainXP(20+lvlBonus(target));
		target.gainXP(10+target.lvlBonus(this)+target.lvlBonus(assist));
		target.arousal.empty();
		if(target.has(Trait.insatiable)){
			target.arousal.restore((int) (arousal.max()*.2));
		}
		dress(c);
		target.undress(c);
		target.giveTrophy(this, c);
		target.defeated(this);
		if(target.hasDick()){
			c.write(String.format("You position yourself between %s's legs, gently forcing them open with your knees. %s dick stands erect, fully exposed and "
					+ "ready for attention. You grip the needy member and start jerking it with a practiced hand. %s moans softly, but seems to be able to handle "
					+ "this level of stimulation. You need to turn up the heat some more. Well, if you weren't prepared to suck a cock or two, you may have joined "
					+ "the wrong competition. You take just the glans into your mouth, attacking the most senstitive area with your tongue. %s lets out a gasp and "
					+ "shudders. That's a more promising reaction.<p>"
					+ "You continue your oral assault until you hear a breathy moan, <i>\"I'm gonna cum!\"</i> You hastily remove %s dick out of your mouth and "
					+ "pump it rapidly. %s shoots %s load into the air, barely missing you.", 
					target.name(),target.possessive(true),target.name(),target.pronounSubject(true),target.possessive(false),target.name(),target.possessive(false)));
		}else{
			c.write(target.name()+"'s arms are firmly pinned, so she tries to kick you ineffectually. You catch her ankles and slowly begin kissing and licking your way " +
					"up her legs while gently, but firmly, forcing them apart. By the time you reach her inner thighs, she's given up trying to resist. Since you no " +
					"longer need to hold her legs, you can focus on her flooded pussy. You pump two fingers in and out of her while licking and sucking her clit. In no " +
					"time at all, she's trembling and moaning in orgasm.");
		}
		gainAttraction(target,1);
		target.gainAttraction(this,1);
	}
	public void gain(Item item) {
		gui.message("<b>You've gained "+item.pre()+item.getName()+"</b>");
		inventory.add(item);
	}
	public void gain(Clothing item) {
		gui.message("<b>You've gained "+item.pre()+item.getName()+"</b>");
		closet.add(item);
	}

	@Override
	public void save(PrintWriter saver) {
		saver.write("P\n");
		saver.write(name+"\n");
		saver.write(level+"\n");
		saver.write(getRank()+"\n");
		saver.write(xp+"\n");
		saver.write(money+"\n");
		for(Attribute a: att.keySet()){
			saver.write(a+" "+getPure(a)+"\n");
		}
		saver.write("@\n");
		saver.write(stamina.max()+"\n");
		saver.write(arousal.max()+"\n");
		saver.write(mojo.max()+"\n");
		for(Character player:affections.keySet()){
			saver.write(player.name()+" "+getAffection(player)+"\n");
		}
		saver.write("*\n");
		for(Character player2:attractions.keySet()){
			saver.write(player2.name()+" "+getAttraction(player2)+"\n");
		}
		saver.write("*\n");
		for(Clothing c:outfit[0]){
			saver.write(c.toString()+"\n");
		}
		saver.write("!\n");
		for(Clothing c:outfit[1]){
			saver.write(c.toString()+"\n");
		}
		saver.write("!!\n");
		for(Clothing c:closet){
			saver.write(c.toString()+"\n");
		}
		saver.write("!!!\n");
		for(Trait t: traits){
			saver.write(t.name()+"\n");
		}
		saver.write("@\n");
		for(Item i: inventory){
			saver.write(i.toString()+"\n");
		}
		saver.write("$\n");
	}

	@Override
	public void load(Scanner loader) {
		name=loader.next();
		level=Integer.parseInt(loader.next());
		setRank(Integer.parseInt(loader.next()));
		xp=Integer.parseInt(loader.next());
		money=Integer.parseInt(loader.next());
		String e = loader.next();
		while(!e.equals("@")){
			set(Attribute.valueOf(e),Integer.parseInt(loader.next()));
			e = loader.next();
		}
		stamina.setMax(Integer.parseInt(loader.next()));
		arousal.setMax(Integer.parseInt(loader.next()));
		mojo.setMax(Integer.parseInt(loader.next()));
		e = loader.next();
		while(!e.equals("*")){
			gainAffection(Global.lookup(e),Integer.parseInt(loader.next()));
			e = loader.next();
		}
		e = loader.next();
		while(!e.equals("*")){
			gainAttraction(Global.lookup(e),Integer.parseInt(loader.next()));
			e = loader.next();
		}
		e = loader.next();
		outfit[0].clear();
		while(!e.equals("!")){
			outfit[0].add(Clothing.valueOf(e));
			e = loader.next();
		}
		e=loader.next();
		outfit[1].clear();
		while(!e.equals("!")&&!e.equals("!!")){
			outfit[1].add(Clothing.valueOf(e));
			e = loader.next();
		}
		if(e.equals("!!")){
			e=loader.next();
			closet.clear();
			while(!e.equals("!!!")){
				closet.add(Clothing.valueOf(e));
				e = loader.next();
			}
		}
		e=loader.next();
		while(!e.equals("@")){
			traits.add(Trait.valueOf(e));
			e = loader.next();
		}
		e=loader.next();
		while(!e.equals("$")){
			inventory.add(Global.getItem(e));
			e = loader.next();
		}
		change(Modifier.normal);
		Global.learnSkills(this);
		stamina.fill();
		arousal.empty();
		mojo.empty();
	}

	@Override
	public String challenge(Character opponent) {
		return null;
	}

	@Override
	public void promptTrap(Encounter enc,Character target,Trap trap) {
		Global.gui().message("Do you want to take the opportunity to ambush <b>"+target.name()+"</b>?");
		assessOpponent(target);
		gui.promptOpportunity(enc, target, trap);		
	}
	public HashMap<Item,Integer> listinventory() {
		String inv = "";
		HashMap<Item,Integer> tally = new HashMap<Item,Integer>();
		for(Item i:inventory){
			if(tally.containsKey(i)){
				tally.put(i, tally.get(i)+1);
			}
			else{
				tally.put(i, 1);
			}
		}
		return tally;
	}

	@Override
	public void afterParty() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void counterattack(Character target, Tactics type, Combat c) {
		switch(type){
		case damage:
			c.write("You dodge "+target.name()+"'s slow attack and hit her sensitive tit to stagger her.");
			target.pain(4+Global.random(get(Attribute.Cunning)),Anatomy.chest,c);
			break;
		case pleasure:
			if(!target.nude()){
				c.write("You pull "+target.name()+" off balance and lick her sensitive ear. She trembles as you nibble on her earlobe.");
			}
			else{
				c.write("You pull "+target.name()+" to you and rub your thigh against her girl parts.");
			}
			target.pleasure(4+Global.random(get(Attribute.Cunning)),Anatomy.genitals,c);
			break;
		case positioning:
			c.write(target.name()+" loses her balance while grappling with you. Before she can fall to the floor, you catch her from behind and hold her up.");
			c.stance=new Behind(this,target);
			break;
		default:
		}
	}

	@Override
	public void eot(Combat c, Character opponent, Skill last) {
		dropStatus();
		for(Status s:status){
			s.turn(c);
		}
		if(opponent.pet!=null&&canAct()&&c.stance.mobile(this)&&!c.stance.prone(this)){
			if(get(Attribute.Speed)>opponent.pet.ac()*Global.random(20)){
				opponent.pet.caught(c,this);
			}
		}
		if(opponent.has(Trait.pheromones)&&opponent.getArousal().percent()>=50&&!is(Stsflag.horny)&&Global.random(5)==0){
			c.write("Whenever you're near "+opponent.name()+", you feel your body heat up. Something in her scent is making you extremely horny.");
			add(new Horny(this,2+2*opponent.getSkimpiness(),3));
		}
		if(has(Trait.RawSexuality)){
			tempt(1);
			opponent.tempt(1);
		}
		if(getPure(Attribute.Animism)>=4&&getArousal().percent()>=50){
			if(!is(Stsflag.feral)){
				add(new Feral(this));
			}
		}
	}

	@Override
	public String getPortrait() {
		return null;
	}

	@Override
	public Emotion moodSwing() {
		Emotion current = mood;
		int max=emotes.get(current);
		for(Emotion e: emotes.keySet()){
			if(emotes.get(e)>max){
				mood=e;
				max=emotes.get(e);
			}
		}
		return mood;
	}

}
