package characters;

import combat.Combat;
import global.Modifier;

public class ReykaNPC extends NPC {
	private boolean hasTrophy = true;
	public ReykaNPC(String name, int level, Personality ai) {
		super(name, level, ai);
	}
	@Override
	public void giveTrophy(Character victor, Combat c) {
		if(hasTrophy(c)) {
			victor.gain(this.getUnderwear());
			hasTrophy = false;
		}
	}
	@Override
	public boolean hasTrophy(Combat c) {
		return hasTrophy;
	}
	@Override
	public void change(Modifier rule) {
		super.change(rule);
		hasTrophy = true;
	}
}