package characters;

import global.Flag;
import global.Global;
import global.Modifier;
import items.Clothing;
import items.Item;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentSituation;
import skills.Skill;
import skills.Tactics;
import actions.Action;
import actions.Movement;

import combat.Combat;
import combat.Result;

public class Yui implements Personality {
	public NPC character;
	public Yui(){
		character = new NPC("Yui",1,this);
		character.outfit[0].add(Clothing.kunoichitop);
		character.outfit[1].add(Clothing.panties);
		character.outfit[1].add(Clothing.ninjapants);
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.kunoichitop);
		character.closet.add(Clothing.halfcloak);
		character.closet.add(Clothing.panties);
		character.closet.add(Clothing.ninjapants);
		character.change(Modifier.normal);
		character.setUnderwear(Trophy.YuiTrophy);
		character.add(Trait.female);
		character.add(Trait.shy);
		character.plan = Emotion.sneaking;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.sneaking, 5);
		character.strategy.put(Emotion.bored, 2);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Fertility Rite")){
				mandatory.add(a);
			}
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar) {
		Action proposed = character.parseMoves(available, radar);
		return proposed;
	}

	@Override
	public NPC getCharacter() {
		return character;
	}

	@Override
	public void rest(int time) {
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Dojo");
		available.add("Play Video Games");
		available.add("Workshop");
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Crop)||character.has(Toy.Crop2))&&character.money>=200){
			character.gain(Toy.Crop);
			character.money-=200;
		}
		for(int i=0;i<time-1;i++){
			loc = available.get(Global.random(available.size()));
			Global.getDay().visit(loc, character, Global.random(character.money));
		}
		character.visit(1);
	}

	@Override
	public String bbLiner() {
		return "Yui gives a quick bow of apology. <i>\"Sorry Master. I was trained to always target my opponent's weakest point.\"</i>";
	}

	@Override
	public String nakedLiner() {
		return "A deep blush colors Yui's cheeks, but she doesn't cover herself. <i>\"It's fine,\"</i> she whispers to herself unconvincingly. "
				+ "<i>\"A kunoichi doesn't get embarrassed, especially not in front of her master.\"</i>";
	}

	@Override
	public String stunLiner() {
		return "Yui lets out a quiet grunt of exertion as she tries to get back to her feet. <i>\"That was a good hit. I wasn't quite fast enough to dodge it.\"</i>";
	}

	@Override
	public String winningLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String taunt() {
		return "<i>\"Master, I thought you were better than this. Are you letting me win? Ah! You must be pent up. I'll help you cum immediately.\"</i>";
	}

	@Override
	public String victory(Combat c, Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag == Result.intercourse){
			return "You pull Yui in to your body, feeling her breasts push up against your chest. You lean in and kiss her passionately, "
					+ "separating your lips to let your tongue play over hers. Her body trembles underneath your fingertips as you trace "
					+ "the curves of her torso, slowly working your way down to her ass.<p>"
					+ "You give it a slap which pushes her now wet thighs around the length of your cock. She moans as your length rubs "
					+ "along the lips of her slick pussy. You manage two or three thrusts before you feel her knees give out from under "
					+ "her, and you realise it is only your strength that is holding her upright.<p>"
					+ "<i>\"Master...\"</i> she moans as you lean forwards, lowering her to the ground. Her intense physical training "
					+ "obviously didn�ft cover this eventuality, and she offers no resistance as you spread her legs - she even reaches "
					+ "down and starts to rub her clit.<p>"
					+ "You push your head teasingly against her lips and she verily writhes with pleasure, squirming against the floor "
					+ "in an attempt to shuffle towards you and on to your cock. After a short moment of teasing you give her what you want "
					+ "and slip inside her easily, despite your size.<p> "
					+ "<i>\"Ah, master!...\"</i> she cries, her back arching as she bucks against you.  You�fve barely touched her and yet "
					+ "the girl seems on the brink of orgasm. �gMy training never�c. Never prepared me for�c.�h she trails off into a melodic "
					+ "series of moans and you can feel her pussy clench around your length as her entire body convulses in pleasure.<p> "
					+ "She may be lost in a world of delight but you�fre nowhere near done, and you remind her of that by playfully slapping "
					+ "one of her breasts. She yelps in surprise before grabbing at your arm and pulling you deep inside her.<p>"
					+ "<i>\"I�fm�c I�fm cumming again!\"</i> she whimpers, grinding and bucking her hips against your pelvis. She lets out a little "
					+ "yelp each time you thrust into her, and she throws her arms to the ground, pushing herself up against you as her pussy "
					+ "tightens around you yet again.<p>"
					+ "As her pleasure subsides she scrambles off you, turning over and onto her knees. One hand dives furiously between her legs, "
					+ "pumping her clit, whilst the other reaches out for you in a frantic attempt to pull you back in.<p>"
					+ "You willingly oblige, and grab her hips firmly, digging your fingertips into her sweat covered skin, ensuring your grip. "
					+ "She moans wildly, her free hand flying around wildly as she tries to grasp hold of something to solidify her trembling body.<p>"
					+ "You hold her steady with your hands as you thrust deeply into her pussy. With each pump she whimpers with pleasure and "
					+ "there is a little �eschlick�f noise emitted from between her legs. Her fingers dance over her clit and her entire body "
					+ "weight pushes her face into the floor with each movement of your hips, but it does nothing to muffle her cries of "
					+ "pleasure which can be heard in every corner of the building.<p>"
					+ "<i>\"Ah, master.. Please, please cum deep inside me!\"</i> she cries, and despite your best efforts to hold out for "
					+ "longer you are swiftly pushed over the edge.<p>"
					+ "Thrusting as deeply as you can you unload stream after stream of thick cum into her pussy. She bucks back against "
					+ "you once, twice, and on the third time she cums again, spraying your thighs with a mix of your own cum and her squirt.<p>"
					+ "You bask in a few moments of post-fuck bliss before pulling out of Yui�fs still shivering body. As soon as you let go "
					+ "she collapses to the floor in a sweaty, sticky heap of pleasure.";
		}
		return "You're getting dangerously aroused in your fight against Yui. You need to take a more aggressive tactic before she finishes you off. "
				+ "You grab the smaller girl and force her onto her back, your dominant hand firmly on her groin. She gasps softly as your fingers "
				+ "explore her sensitive parts.<p>"
				+ "<i>\"Ah! Master, your fingers are heavenly, but I'm afraid you've fallen into my trap.\"</i> She locks her legs over your shoulder, "
				+ "pinning your arm in place. Before you can free yourself, another Yui jumps on you from behind and pins your free arm. She prepared "
				+ "a shadow clone in advance to ambush you!?<p>"
				+ "<i>\"I'd love to let you keep touching me, but I need your arms immobilized so you can't protect your groin.\"</i> You don't quite understand "
				+ "her plan until you feel another shadow clone's fingers on your scrotum. You can just barely hear her muttering. <i>\"...Jin, Retsu, Zai, Zen...\"</i><p>"
				+ "The shadow clones vanish as soon as the incantation is finished, and you immediately start to feel an urgent heat building in your testicles. "
				+ "With a hand free, you have little trouble breaking Yui's hold and putting her in a more secure pin. She doesn't even resist. Unfortunately, "
				+ "as the heat continues to build in your groin, it creates an almost overwhelming need to ejaculate. The warmth flows from your balls to the base "
				+ "of your cock and you struggle to hold it back. You slide two fingers into Yui pussy and try to make her cum as fast as possible.<p>"
				+ "<i>\"Master, I know firsthand how much pleasure your fingers can bring, but even you can't make me orgasm fast enough to win. Even if I don't "
				+ "touch you, you will ejaculate very soon.\"</i> You know she's telling the truth. You're resisting with all your willpower, but precum is leaking "
				+ "liberally from your penis. <i>\"You should just relax and let me service you. It will feel much more satisfying.\"</i><p>"
				+ "You don't answer her. It takes all of your concentration to just hold back your climax and keep moving your fingers aimlessly. Yui can't reach "
				+ "your groin with her hands, but she starts rubbing your dick with the soft soles of her feet. You could try grabbing her legs or changing position to "
				+ "stop her, but you can't muster the energy. You <b>need</b> to cum right now, and her footjob feels amazing.<p>"
				+ "Yui holds your face and pulls you into a passionate kiss as your resistance breaks down. Her dexterous feet milk your straining cock until you feel "
				+ "a powerful surge of pleasure shoot through your body. Your dick spasms repeatedly as it paints the floor with your semen. The flood of pleasure and "
				+ "relief leaves you completely spent, and you collapse onto her bosom, exhausted.<p>"
				+ "<i>\"...Master? Master...?\"</i> Yui quietly calls out to you, but you're too tired to respond. <i>\"You're asleep...? I'll wake you up in a little "
				+ "while.\"</i> You aren't actually unconscious, but you don't feel like correcting her at the moment. You feel her hand wrap around the base of your "
				+ "penis, inexplicably still erect despite your orgasm. <i>\"Still hard? It must be because you resisted for so long. You built up too much for a single "
				+ "ejaculation.\"</i><p>"
				+ "She moves a little beneath you and starts to rub your sensitive member against her wet slit. <i>\"It's ok if I'm a little naughty, "
				+ "right? I'll be done before you wake up.\"</i> Her voice is thick with arousal. <i>\"It doesn't vibrate, but the real thing feels better than any of "
				+ "my toys.\"</i><p>"
				+ "Yui vigorously frots against the length of your dick, pleasuring herself without penetration. The stimulation is intense for you too. Despite having "
				+ "just cum, you're filled with the need to ejaculate again. Combined with your exhaustion, you feel powerless to do anything but moan quietly as Yui humps "
				+ "you. Soon, she shudders in orgasm and your overstimulated dick shoots another load of jizz on her abdomen.<p>"
				+ "Yui diligently cleans you both up as you finally muster the effort to sit up. <i>\"Master, you're awake! Your orgasm was so intense you passed out. "
				+ "I've been watching over you to make sure none of the other competitors take advantage.\"</i> You gently stroke her head. It's probably best not to "
				+ "bring up the fact that you were awake for what just happened.<p>"
				+ "<i>\"Since you lost the fight, you'll have to proceed naked for now. As you are both exhausted and nude, you seem very vulnerable. Shall I escort you "
				+ "for now?\"</i> You decline as politely as you can. Her words are supportive enough, but there's a hunger in her eyes that makes you uncomfortable. "
				+ "Yui means well, but you're starting to realize she may be the most dangerous opponent to lose to.";
	}

	@Override
	public String defeat(Combat c, Result flag) {	
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		return "Yui trembles with pleasure as your experienced fingers work on her sensitive pussy. You kiss her on the lips and she embraces you desperately, "
		+ "too overwhelmed by passion to fight back. She's on the brink of orgasm, but you want to play with her a bit more, so you slow your fingering "
		+ "to light, teasing touches.<p>"
		+ "<i>\"Master! That feels so good, but please let me cum!\"</i> You tickle her clit lightly, making her hips jump. She's supposed to be "
		+ "a trained kunoichi, but she's acting as needy as a horny virgin. Shouldn't she be better as resisting pleasure than this?<p>"
		+ "Yui lets out a plaintive moan at your teasing. <i>\"That's not fair, Master! My traini-AH! My training relies on detaching pleasure "
		+ "from emotion. It doesn-AH! It won't help me against you, Master!\"</i> She's so cute that you just have to give her another kiss. As "
		+ "your tongue slips into her mouth, you feel her body start to convulse in orgasm. You're barely touching her, but apparently the kiss was "
		+ "enough to set her off. You finger her in earnest to maximize her climax and she screams into your mouth.<p>"
		+ "You break the kiss as her orgasm subsides, leaving her in a blissful daze. </i>\"Master... That was wonderful... I...\"</i> She regains her "
		+ "focus as her eyes land on your erection. She firmly pushes you onto your back and settles between your legs. <i>\"Allow me to "
		+ "pleasure you, too.\"</i><p>"
		+ "Without waiting for your response, Yui starts sucking your dick passionately. You groan quietly and gently stroke her head as you enjoy her "
		+ "earnest effort. Her tongue and lips are extremely skilled, and you're soon brought to the brink of orgasm without any of her weird ninja techniques. "
		+ "She takes as much of your shaft as she can manage and sucks strongly. Your hips buck as you shoot your load into her mouth. She continues sucking "
		+ "until your ejaculation finishes, swallowing every drop of your seed.<p>"
		+ "She wipes her mouth after she finishes and looks at you hopefully. <i>\"Master, did my feelings reach you? Your kiss touched me so deeply, "
		+ "I overflowed with joy. I wanted to share it with you as much as I could.\"</i> You pull the naked girl into a tight embrace and kiss her "
		+ "softly on the forehead. Her devotion and affection definitely reached you.";
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Yui stands over you and looks over your naked, restrained body excitedly. <i>\"I'm sorry Master, but this is how the game works, right?\"</i> "
					+ "She tries to sound calm, but you can tell she's practically drooling with anticipation. She straddles your hips and positions her slick "
					+ "entrance above your cock. <i>\"I'll finish you with the utmost care and pleasure.\"</i><p>"
					+ "Yui slowly lowers her hips to take your length inside her. You both let out a moan in unison as she starts to move her hips rhythmically. This "
					+ "seems like a risky strategy when she's as turned on as you are, but you know she has some tricks up her sleeve. Her vaginal walls suddenly contract "
					+ "around your shaft and your hips buck involuntarily. <i>\"How do you like the Ishida Kunoichi's third hidden art?\"</i> Her voice is slightly strained, "
					+ "but hot with desire. <i>\"It takes a lot of concentration, but if you can't resist, I can make you cum quickly.\"</i><p>"
					+ "Her insides continue to contract and relax in time with her hip movements, rapidly milking you to orgasm. You shoot your load inside her and feel "
					+ "her orgasm about 20 seconds later.";
		}
		else{
			if(target.hasDick()){
				return String.format("Yui stares at %s's exposed penis with intense curiosity. <i>\"It's very different than master's. I guess dicks really "
						+ "do come in all shapes and sizes...\"</i> She grabs the shaft with both hands and begins a practiced handjob. <i>\"But they all cum, just the same.\"</i><br>"
						+ "She flashes you an expectant grin. Her pun only earns a shrug, but you give her a smile for the effort. She seems sufficiently "
						+ "encouraged anyway. <i>\"Of course, your penis is still my favorite, Master. If you see any techniques you want me to use on you after "
						+ "the match, just let me know. Oh! Like this one.\"</i> She pulls a black cloth from her outfit and wraps it around %s's cock. <i>\"It's pure silk, "
						+ "very pleasant on sensitive skin.\"</i> She rapidly moves the cloth back and forth, like she's shining a shoe. %s lets out a passionate "
						+ "moan at the sensation. In no time at all, %s covers the cloth in hot cum.<br>"
						+ "Yui gives you another exceptant look. This time she gets a thumbs up for execution.",
						target.name(),target.name(),target.pronounSubject(false));
			}
			else{
				return String.format("Yui looks down at %s with a dominance you aren't used to seeing from her. It's slightly comical since her fight left her nude. "
						+ "<i>\"You were a worthy opponent for me alone, but you were no match for the unbreakable trust between me and my Master.\"</i> "
						+ "She kneels confidently between the helpless girl's legs. <i>\"Don't worry, I'll give you a nice pleasant orgasm.\"</i><br> "
						+ "Yui begins to finger %s, who lets out a gasp of surprise. Yui notices her victim's confusion and holds up a compact, fingertip-size "
						+ "vibrator. <i>\"Are you enjoying the Ishida Kunoichi Hidden Weapon technique? You make some really cute sounds. "
						+ "Let Master and I hear some more of them.\"</i><br>"
						+ "%s lets out quite a few moans before, during, and even slightly after she climaxes at Yui's relentless hands. The cute ninja seems "
						+ "to have a bit of a dominant streak when it comes to other girls.",
						target.name(),target.name(),target.name());
			}
		}
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "Your fight with "+assist.name()+" has barely started when you hear a familiar voice call out to you. <i>\"Master! I was hoping you would be here.\"</i> " +
					"Before you can react, Yui grabs you and eagerly kisses you on the lips. Your surprise quickly gives way to extreme lightheadedness and drowsiness. " +
					"Your legs give out and you collapse into her arms. Did Yui drug you? <i>\"Please forgive this betrayal, Master. You work so hard fighting and training " +
					"every night. For the sake of your health, I thought it was necessary to make you take a break.\"</i> She sounds genuinely apologetic, but also a little " +
					"excited. <br>"
					+ "<i>\"Don't worry. We'll take good care of you until you can move again.\"</i> She carefully lowers your limp upper body onto her lap as "+
					assist.name()+" fondles your dick to full hardness. <i>\"I'm sure we can relieve some of your built up stress too.\"</i><br>";
		}
		else{
			return "This fight could certainly have gone better than this. You're completely naked and have your hands bound behind your back. "+target.name()+" is just taking " +
					"her time to finish you off. A familiar voice calls out to her. <i>\"I see you've caught my master. I've always wanted to get him in this position.\"</i> You " +
					"both surprised to see Yui standing nearby. She hadn't made a sound when she approached. <i>\"Do you mind if I play with him for a moment? I promise I " +
					"won't make him cum.\"</i><p>"
					+ "She kneels in front of you, fondling your balls playfully, but you suddenly feel her freeing your hands. She leans close to your ear and whispers quietly. "
					+ "<i>\"I'll create an opening so you can finish her off, Master.\"</i><br>"
					+ "She stands up casually as if to walk away, but suddenly grabs "+target.name()+"'s arms. "
					+ "You scramble to your feet and join the fight. In no time at all, you and your ninja companion have her completely immobilized.";
		}
	}

	@Override
	public String describe() {
		return "Yui is a much more convincing kunoichi in her combat outfit. Her stylish ninja top gives her freedom of movement and is skimpy enough "
				+ "to show off her toned midriff and a fair bit of side-boob. It's a little distracting, which is probably the intention. "
				+ "In contrast to her exposed body, her face is hidden behind her long blonde bangs. A real shame, since you know she's quite beautiful.";
	}

	@Override
	public String draw(Combat c, Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		return "You feel your arousal building dangerously high, but you haven't reached the point of no return yet. Yui does her best to pin you down and "
				+ "ride your dick, but she's too aroused to maintain control. You manage to free your hands and grab her "
				+ "slim hips. She's light enough that you're able to set the pace, despite her superior position.<p>"
				+ "<i>\"Ah! Master!\"</i> The inexperienced kunoichi loses herself in the pleasure and can't seem to muster an effective counterattack. Instead "
				+ "she moans uncontrollably and can barely hold herself up. What you can see of her face behind her obscuring hair is a mask of wanton desire.<p>"
				+ "For a short while, you are confident you can finish her this way without cumming, but suddenly her vaginal walls squeeze your cock tightly. "
				+ "At first you think she's orgasming, but as her insides tighten and relax repeatedly, you realize this is actually a sexual technique. Whether "
				+ "she's doing it intentionally, or her training is just so thoroughly ingrained in her, her pussy is doing a damn good job milking you. Your "
				+ "best chance now is to pull out and finish her off by hand.<p>"
				+ "<i>\"Master, I love your dick! Let's cum together!\"</i> Yui pulls you into a passionate kiss as your hips grind together. You might be able "
				+ "to claim victory if you pull out now, but it would probably break the poor girl's heart. Some things are more important than winning.<p>"
				+ "You thrust deep into Yui and shoot your pent-up load into her womb. Her body shudders and presses tight against you as she hits her own climax. "
				+ "You embrace her closely as you both indulge in your shared moment of blissful pleasure.<p>"
				+ "In the afterglow, you hear Yui giggle quietly to herself as her head rests on your shoulder. It's cute, but you want in on the joke. You tickle "
				+ "her neck softly and ask what she finds so funny.<p>"
				+ "<i>\"It's not funny, I'm just happy. I was just thinking about our first time together. We came together that time too.\"</i> She's cute enough "
				+ "to warm your heart, but as her master should be a little strict. You gently reprimand her for her lack of discipline. If you had simply pulled "
				+ "out at the end, she would have lost the fight completely.<p>"
				+ "She just smiles and cuddles against you contently. <i>\"But you didn't pull out. Thank you, Master.\"</i> There's really nothing you can say in "
				+ "the face of such pure trust. Instead you just hold her for a bit longer.";
	}

	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude()||opponent.nude();
	}

	@Override
	public boolean attack(Character opponent) {
		return true;
	}

	@Override
	public void ding() {
		character.mod(Attribute.Ninjutsu, 1);
		int rand;
		for(int i=0; i<(Global.random(3)/2)+1;i++){
			rand=Global.random(4);
			if(rand==0){
				character.mod(Attribute.Power, 1);
			}
			else if(rand==1){
				character.mod(Attribute.Seduction, 1);
			}
			else if(rand==2){
				character.mod(Attribute.Cunning, 1);
			}
			else {
				character.mod(Attribute.Ki, 1);
			}
		}
		character.getStamina().gain(5);
		character.getArousal().gain(5);
		character.getMojo().gain(3);
	}

	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case ninjapreparation:		
				return "As you approach Yui, she suddenly sprints away. You quickly give chase, but are blindsided by a spring-propelled net "
						+ "after a few steps. As you struggle to untangle yourself from the net, your head starts to swim, and you realize Yui "
						+ "got you with a couple of her drugged needles while you were distracted.<p>"
						+ "The kunoichi girl approaches to finish you off. <i>\"Sorry Master, but you should know it's dangerous to persue a ninja.\"</i>";
			case flash:
				return "After your last fight, you confidently stide toward Yui, but she dashes backward to keep a safe distance. She seems pretty cautious. "
						+ "More importantly, she's a lot quicker than you realized.<p>"
						+ "<i>\"Of course, Master. I am a ninja, after all. I can be extremely nimble when I need to be.\"</i>";
			case ishida3rdart:
				return "Yui looks a little more nervous than usual, but smiles at you excitedly. <i>\"Master, I know I sometimes have "
						+ "trouble concentrating when we're doing... intense things,\"</i> her hands moves unconsciously to her groin. "
						+ "<i>\"But I've been doing image training to prepare. If you put it inside me again, I'm sure I'll do my techniques "
						+ "properly this time.\"</i>";
			default:
				break;
	
			}
		}
		if(character.nude()){
			return "Yui gives you a sheepish grin as she tries to cover her naked body. <i>\"I guess I got a little careless, Master. Normally I wouldn't "
					+ "let anyone but you undress me.\"</i>";
		}
		if(opponent.pantsless()){
			return "<i>\"Ah! Master!\"</i> Yui starts to bow to you, but freezes partway through. She seems to be staring at your naked crotch with intensity. "
					+ "You cough to get her attention.<p>"
					+ "<i>\"Oh, right! The match! I was just planning my strategy. I definitely wasn't daydreaming about how I lost my virginity.\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Yui smiles brightly as she sees you. ";
		}
		return "Yui gives you a polite, eager bow. <i>\"Master, let's have a good sparring match. Just like our training, but "
				+ "with more pleasure!\"</i>";
	}

	@Override
	public boolean fit() {
		return character.getStamina().percent()>=60&&character.getArousal().percent()<=20&&!character.nude();
	}

	@Override
	public String night() {
		return "After the match, you hang around to chat with the girls for a few minutes. Yui in particular seems restless when you "
				+ "talk to her. It's clear she's hoping for something, but she's restraining herself from asking. Maybe she doesn't want "
				+ "to appear needy. When you invite her to your room, she looks as eager as an excited puppy.<p>"
				+ "You reach the dorm, and she gets a little self-conscious about how much she was sweating during the match and excuses "
				+ "herself to use the shower. Realizing you also smell of sweat (and other fluids), you decide to join her.<p>"
				+ "After washing each other clean, and then messy, and then clean again, the two of you eventually collapse naked into "
				+ "your bed. You're exhausted enough that you could fall asleep immediately, but Yui is still feeling a bit frisky, so "
				+ "you decide to go one last round.<p>"
				+ "You make love to her slowly and tenderly, the way you never have time for during a match. When it's finished, she "
				+ "snuggles comfortably in your arms as you start to drift off to sleep.<p>"
				+ "<i>\"Master, when I moved here, I never imagined I'd meet someone like you.\"</i> You hear Yui whisper softly. It's not "
				+ "clear whether she knows you're conscious, so you just listen quietly. <i>\"I'm glad you were my first. I know you may "
				+ "fall in love with one of the other girls, but I'll be your kunoichi for as long as you'll let me.\"</i><p>"
				+ "She goes quiet after that. For a long moment, you consider whether to answer her, but soon her soft, regular breathing "
				+ "tells you she's fallen asleep.";
	}

	@Override
	public void advance() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case confident:
			return value>=30;
		default:
			return value>=70;
		}
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case confident:
			return 1.2f;
		default:
			return .8f;
		}
	}

	@Override
	public String image() {
		return "assets/yui_"+ character.mood.name()+".jpg";
	}

	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	public Map<CommentSituation, String> getComments() {
		HashMap<CommentSituation,String> comments = new HashMap<CommentSituation,String>();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "Master, don't hold back. I'll happily accept your seed.");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "Master, don't hold back. I'll happily accept your seed.");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "It feels so good! I can't hold back!");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "Master! Ah! Even on top, your dick is irresistable!");
		comments.put(CommentSituation.BEHIND_DOM_WIN, "Master, you shouldn't leave your back open when a ninja is nearby.");
		comments.put(CommentSituation.BEHIND_SUB_LOSE, "From behind!? How!?");
		comments.put(CommentSituation.SIXTYNINE_WIN, "Your dick is really throbbing. Are you enjoying my technique?");
		comments.put(CommentSituation.SIXTYNINE_LOSE, "Master, you're too good at this! I can't keep up!");
		comments.put(CommentSituation.OTHER_BOUND, "Master, you look a little tied up. Don't worry, I'll handle your needy penis.");
		comments.put(CommentSituation.SELF_BOUND, "Well done, Master. You caught your very own ninja girl. I'll be out of this in a snap!");
		comments.put(CommentSituation.SELF_HORNY, "Please, Master... I don't care if I lose, just give me your love!");
		comments.put(CommentSituation.SELF_CHARMED, "Yes, Master. Whatever you need.");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "In my butt!? That's a really advanced technique!");
		comments.put(CommentSituation.OTHER_STUNNED, "I didn't expect you to go down so quickly. I'll give you lots of pleasure if you don't resist.");
		comments.put(CommentSituation.OTHER_OILED, "Preparations are complete. Time for a well-lubricated handjob!");
		comments.put(CommentSituation.SELF_SHAMED, "M-Master, don't look! It's embarrassing...");
		comments.put(CommentSituation.PIN_DOM_WIN, "I've practiced this hold, you won't get away so easily.");
		comments.put(CommentSituation.PIN_SUB_LOSE, "I know how to break this hold...ah...um... This might be trickier than I thought.");
		
		return comments;
	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(c.eval(character)==Result.intercourse){
			character.addGrudge(opponent, Trait.ishida3rdart);
		}else{
			switch(Global.random(2)){
			case 0:
				character.addGrudge(opponent,Trait.flash);
				break;
			case 1:
				character.addGrudge(opponent,Trait.ninjapreparation);
				break;
			default:
				break;
			}
		}
		
	}

}
