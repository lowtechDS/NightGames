package skills;

import status.Abuff;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class ArmBar extends Skill {

	public ArmBar(Character self) {
		super("Armbar", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.dom(self)&&c.stance.reachTop(self)&&self.canAct()&&!self.has(Trait.undisciplined)&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			int m = Global.random(10)+self.get(Attribute.Power)/2;
			if(self.human()){
				c.write(self,deal(m,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(m,Result.normal,target));
			}
			target.pain(m,Anatomy.arm,c);
			target.add(new Abuff(target, Attribute.Power,-4,5),c);
			target.emote(Emotion.angry,25);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	public boolean requirements() {
		return self.getPure(Attribute.Power)>=20 && !self.has(Trait.undisciplined);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=20 && !user.has(Trait.undisciplined);
	}

	@Override
	public Skill copy(Character user) {
		return new ArmBar(user);
	}
	
	public int speed(){
		return 2;
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You grab at %s's arm, but %s pulls it free.",target.name(),target.pronounSubject(false));
		}
		else{
			return String.format("You grab %s's arm at the wrist and pull it to your chest in the traditional judo submission technique.",
					target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("%s grabs your wrist, but you pry it out of %s grasp.",self.name(),target.possessive(false));
		}
		else{
			return String.format("%s pulls your arm between %s legs, forcibly overextending your elbow. The pain almost makes you tap out, but you manage to yank your arm " +
				"out of %s grip.",self.name(),self.possessive(false),self.possessive(false));
		}
	}

	@Override
	public String describe() {
		return "A judo submission hold that hyperextends the arm.";
	}
}
