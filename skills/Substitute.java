package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import stance.Behind;
import stance.Pin;
import status.Stsflag;

public class Substitute extends Skill {

	public Substitute(Character self) {
		super("Substitution", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Ninjutsu)>=21;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !c.stance.mobile(self)&&c.stance.sub(self)&&!self.bound()&&!self.stunned()&&!self.distracted()&&self.canSpend(10)&&!self.is(Stsflag.enthralled);
	}

	@Override
	public String describe() {
		return "Use a decoy to slip behind your opponent: 10 Mojo.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		
		c.stance=new Behind(self,target);
		target.emote(Emotion.nervous, 10);
		self.emote(Emotion.dominant, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new Substitute(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("By the time %s realizes %s's pinning a dummy, you're already behind %s.",
				target.name(),target.pronounSubject(false),target.pronounTarget(false));
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("You straddle %s's unresisting body. Wait... This is a blow-up doll. "
				+ "The real %s is standing behind you. When-How- did %s make the switch?",
				self.name(),self.name(),self.pronounSubject(false));
	}

}
