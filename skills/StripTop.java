package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;

import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class StripTop extends Skill {

	public StripTop(Character self) {
		super("Strip Top", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachTop(self)&&!target.topless()&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		int strip = self.get(Attribute.Cunning);
		if(target.stripAttempt(strip,self, c, target.top.peek())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Jewel")){
					c.offerImage("Strip Top Female.jpg", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			target.strip(0, c);
			if(self.getPure(Attribute.Cunning)>=30 &&! target.topless()){
				if(target.stripAttempt(strip,self, c, target.top.peek())){
					if(self.human()){
						c.write(self,deal(0,Result.strong,target));
						
					}
					else if(target.human()){
						c.write(self,receive(0,Result.strong,target));
					}
					target.strip(0, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if(self.human()&&target.nude()){
				c.write(target,target.nakedLiner());
			}
			target.emote(Emotion.nervous, 10);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripTop(user);
	}
	public int speed(){
		return 3;
	}
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You attempt to strip off %s's %s, but %s shoves you away.",
					target.name(),target.top.peek().getName(),target.pronounSubject(false));
		}else if(modifier==Result.strong){
			return String.format("Taking advantage of the situation, you also manage to snag %s %s", target.possessive(false), target.top.peek().getName());
		}else{
			if(target.canAct()){
				return "After a brief struggle, you manage to pull off "+target.name()+"'s "+target.top.peek().getName()+".";
			}else{
				return "You remove "+target.name()+"'s "+target.top.peek().getName()+" without any resistence.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to yank off your "+target.top.peek().getName()+", but you manage to hang onto it.";
		}else if(modifier==Result.strong){
			return String.format("Before you can react, %s also strips off your %s", self.pronounSubject(false), target.top.peek().getName());
		}else{
			if(target.canAct()){
				return String.format("%s grabs a hold of your %s and yanks it off before you can stop her.",self.name(),target.top.peek().getName());
			}else{
				return String.format("%s easily strips off your %s while you are unable to fight back.",self.name(),target.top.peek().getName());

			}
		}
	}

	
	@Override
	public String describe() {
		return "Attempt to remove opponent's top. More likely to succeed if she's weakened and aroused";
	}
}
