package skills;

import items.Item;
import items.Toy;
import status.Hypersensitive;
import global.Flag;
import global.Global;
import global.Modifier;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Tickle extends Skill {

	public Tickle(Character self) {
		super("Tickle", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&(c.stance.mobile(self)||c.stance.dom(self))&&(c.stance.reachTop(self)||c.stance.reachBottom(self));
	}

	@Override
	public void resolve(Combat c, Character target) {
		Result mod = Result.normal;
		boolean ticklemonsterBoost = false;
		if(target.roll(this, c, accuracy()+self.tohit())){
			float t = Math.max(2+Global.random(3+target.get(Attribute.Perception))-(target.top.size()+target.bottom.size()),1);
			float w = Math.max(Global.random(3+target.get(Attribute.Perception))-(target.top.size()+target.bottom.size()),1);
			if(target.has(Trait.ticklish)){
				t = Math.round(1.3*t);
				w += Math.round(1.3*t);
			}
			if(self.has(Trait.ticklemonster)&&target.nude()){
					ticklemonsterBoost = true;
					mod = Result.special;
					t *= 1.5;
					w *= 1.5;						
			}
			else if(hastickler()&&Global.random(2)==1&&(!self.human()||Global.getMatch().condition!=Modifier.notoys)){
				if(target.pantsless()&&c.stance.reachBottom(self)){	
					mod = Result.strong;
					t *= 1.3;
					w *= 1.3;
				}
				else if(target.topless()&&c.stance.reachTop(self)){
					mod = Result.item;
					t *= 1.2;
					w *= 1.2;
				}
				else{
					mod = Result.weak;
					t *= 1.1;
					w *= 1.1;
				}
			}
			else{				
				mod = Result.normal;
			}
			if(self.has(Toy.Tickler2)&&Global.random(2)==1&&self.canSpend(10)&&(!self.human()||Global.getMatch().condition!=Modifier.notoys)){
				mod = Result.critical;
				self.spendMojo(10);
				target.add(new Hypersensitive(target),c);
			}
			if(self.human()){
				c.write(self,deal(0,mod,target));
			}
			else if(target.human()){
				c.write(self,receive(0,mod,target));
			}
			if(ticklemonsterBoost){
				target.tempt(Math.round(t),c);
				target.pleasure(1, Anatomy.genitals,c);
				target.weaken(Math.round(w),c);
				self.buildMojo(15);
			}else{
				target.tempt(Math.round(t), Result.foreplay,c);
				target.weaken(Math.round(w),c);
				self.buildMojo(10);
			}
			if(self.human()){
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Mara")){
					c.offerImage("Tickle.jpg", "Art by AimlessArt");
				}
			}
		}
		else{
			mod = Result.miss;
			if(self.human()){
				c.write(self,deal(0,mod,target));
			}
			else if(target.human()){
				c.write(self,receive(0,mod,target));
			}
		}
		
	}

	@Override
	public boolean requirements() {
		return !self.has(Trait.cursed);
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new Tickle(user);
	}
	public int speed(){
		return 7;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You try to tickle "+target.name()+", but she squirms away.";
		}
		else if(modifier == Result.special){
			return "You work your fingers across "+target.name()+"'s most ticklish and most erogenous zones until she's a writhing in pleasure and can't even make coherent words.";
		}
		else if(modifier == Result.critical){
			return "You brush your tickler over "+target.name()+"'s body, causing her to shiver and retreat. When you tickle her again, she yelps and almost falls down. " +
					"It seems like your special feathers made her more sensitive than usual.";
		}
		else if(modifier == Result.strong){
			return "You run your tickler across "+target.name()+"'s sensitive thighs and pussy. She can't help but let out a quiet whimper of pleasure.";
		}
		else if(modifier == Result.item){
			return "You tease "+target.name()+"'s naked upper body with your feather tickler, paying close attention to her nipples.";
		}
		else if(modifier == Result.weak){
			return "You catch "+target.name()+" off guard by tickling her neck and ears.";
		}
		else{
			return "You tickle "+target.name()+"'s sides as she giggles and squirms.";
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" tries to tickle you, but fails to find a sensitive spot.";
		}
		else if(modifier == Result.special){
			return self.name()+" tickles your nude body mercilessly, gradually working her way to your dick and balls. As her fingers start tormenting you privates, you struggle to " +
					"clear your head enough to keep from ejaculating immediately.";
		}
		else if(modifier == Result.special){
			return "You work your fingers across "+target.name()+"'s most ticklish and most erogenous zones until she's a writhing in pleasure and can't even make coherent words.";
		}
		else if(modifier == Result.critical){
			return self.name()+" teases your dick and balls with her feather tickler. After she stops, you feel an unnatural sensitivity where the feathers touched you.";
		}
		else if(modifier == Result.strong){
			return self.name()+" brushes her tickler over your balls and teases the sensitive head of your penis.";
		}
		else if(modifier == Result.item){
			return self.name()+" runs her feather tickler across your nipples and abs.";
		}
		else if(modifier == Result.weak){
			return self.name()+" pulls out a feather tickler and teases any exposed skin she can reach.";
		}
		else{
			return self.name()+" suddenly springs toward you and tickles you relentlessly until you can barely breathe.";
		}
	}

	@Override
	public String describe() {
		return "Tickles opponent, weakening and arousing her. More effective if she's nude";
	}
	private boolean hastickler(){
		return self.has(Toy.Tickler)||self.has(Toy.Tickler2);
	}
}
