package skills;

import items.Component;
import items.Item;
import status.Winded;
import global.Global;
import characters.Anatomy;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class StunBlast extends Skill {

	public StunBlast(Character self) {
		super("Stun Blast", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Science)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science)>=9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.behind(self)&&self.has(Component.Battery,4);
	}

	@Override
	public String describe() {
		return "A blast of light and sound with a chance to stun: 4 Battery";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Component.Battery, 4);
		if(Global.random(10)>=5){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			target.pain(target.getStamina().get(),Anatomy.head,c);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new StunBlast(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You overload the emitter on your arm, but "+target.name()+" shields her face to avoid the flash.";
		}
		else{
			return "You overload the emitter on your arm, duplicating the effect of a flashbang. "+target.name()+" staggers as the blast disorients her.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" covers her face and points a device in your direction. Sensing danger, you shield you eyes just as the flashbang goes off.";
		}
		else{
			return self.name()+" points a device in your direction that glows slightly. A sudden flash of light disorients you and your ears ring from the blast.";
		}
	}

}
