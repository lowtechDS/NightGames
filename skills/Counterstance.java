package skills;

import status.Alert;
import combat.Combat;
import combat.Result;

import characters.Attribute;
import characters.Character;

public class Counterstance extends Skill{

	public Counterstance(Character self) {
		super("Stance", self);
	}

	@Override
	public boolean requirements() {
		return self.get(Attribute.Ki)>=2;
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Ki)>=2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&!c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Increases the likelihood of counterattacking a miss";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(receive(0,Result.normal,target));
		}
		self.add(new Alert(self),c);
	}

	@Override
	public Skill copy(Character user) {
		return new Counterstance(user);
	}

	@Override
	public Tactics type() {
		return Tactics.debuff;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You hone your concentration and prepare to counter as soon as "+target.name()+" makes a mistake.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" shifts her wait slightly and it seems like she's more prepared for your attacks.";
	}

}
