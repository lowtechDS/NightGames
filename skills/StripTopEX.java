package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;

import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class StripTopEX extends Skill {

	public StripTopEX(Character self) {
		super("Tricky Strip Top", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachTop(self)&&!target.topless()&&self.canAct()&&self.canSpend(15);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int strip = 0;
		Result style = Result.clever;
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			style = Result.powerful;
		}else if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning)){
			style = Result.seductive;
		}
		self.spendMojo(15);
		if(style == Result.powerful){
			strip = self.get(Attribute.Power);
		}else if(style == Result.clever){
			strip = self.get(Attribute.Cunning);
			strip *= 3;
		}else{
			strip = self.get(Attribute.Seduction);
		}
			
		
		if(target.stripAttempt(strip,self, c, target.top.peek())){
			if(self.human()){
				c.write(self,deal(0,style,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Jewel")){
					c.offerImage("Strip Top Female.jpg", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(0,style,target));
			}
			target.strip(0, c);
			if(self.getPure(Attribute.Cunning)>=30 &&! target.topless()){
				if(target.stripAttempt(strip,self, c, target.top.peek())){
					if(self.human()){
						c.write(self,deal(0,Result.strong,target));
						
					}
					else if(target.human()){
						c.write(self,receive(0,Result.strong,target));
					}
					target.strip(0, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if(self.human()&&target.nude()){
				c.write(target,target.nakedLiner());
			}
			target.emote(Emotion.nervous, 10);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripTopEX(user);
	}
	public int speed(){
		return 3;
	}
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You attempt to strip off %s's %s, but %s shoves you away.",
					target.name(),target.top.peek().getName(),target.pronounSubject(false));
		}else if(modifier==Result.strong){
			return String.format("Taking advantage of the situation, you also manage to snag %s %s", target.possessive(false), target.top.peek().getName());
		}if(modifier==Result.powerful){
			return String.format("You grab %s's %s and yank it off with enough force that you almost pull %s off the ground.",
					target.name(),target.top.peek().getName(),target.pronounTarget(false));
		}
		else if(modifier==Result.clever){
			return String.format("You slip your hands under %s's %s to caress %s. You swipe the garment with a flick of your wrist when %s least expects it.",
					target.name(),target.top.peek().getName(),target.pronounTarget(false),target.pronounSubject(false));
		}
		else{
			return String.format("You turn up the charm to maximium, and help %s out of %s %s before %s can remember to resist.",
					target.name(),target.possessive(false),target.top.peek().getName(),target.pronounSubject(false));
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to yank off your "+target.top.peek().getName()+", but you manage to hang onto it.";
		}else if(modifier==Result.strong){
			return String.format("Before you can react, %s also strips off your %s", self.pronounSubject(false), target.top.peek().getName());
		}if(modifier==Result.powerful){
			return String.format("%s forcefully pulls your %s over your head, blinding you. As you struggle to restore your vision, %s easily yanks "
					+ "the garment completely off.",
					self.name(),target.top.peek().getName(),self.pronounSubject(false));
		}
		else if(modifier==Result.clever){
			return String.format("You brace yourself for an attack as %s lunges toward you, only to playfully lick your cheek. You're so shocked by the "
					+ "unexpected action that %s manages to steal your %s in the confusion.",
					self.name(),self.pronounSubject(false),target.top.peek().getName());
		}
		else{
			return String.format("%s coyly saunters up to you and gently removes your %s. Since %s is acting much more intimate than competitive, "
					+ "you end up letting %s out of habit.",
					self.name(),target.top.peek().getName(),self.pronounSubject(false),self.pronounTarget(false));
		}
	}

	@Override
	public String toString(){
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Forceful Strip Top";
		}else if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning)){
			return "Charming Strip Top";
		}
		return name;
	}
	
	@Override
	public String describe() {
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Attempt to remove opponent's top by force: 15 Mojo";
		}else if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning)){
			return "Attempt to remove opponent's top by seduction: 15 Mojo";
		}
		return "Attempt to remove opponent's top with extra Mojo: 15 Mojo";
	}
}
