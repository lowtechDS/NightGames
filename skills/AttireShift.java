package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Primed;

public class AttireShift extends Skill {

	public AttireShift(Character self) {
		super("Attire Shift", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Temporal)>=6;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal)>=6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&(!target.nude())&&self.getStatusMagnitude("Primed")>=2&&!c.stance.prone(self)&&self.canAct()&&!c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "Seperate your opponent from her clothes: 2 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.add(new Primed(self,-2));
		target.nudify();
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.emote(Emotion.dominant, 15);
		target.emote(Emotion.nervous, 10);

	}

	@Override
	public Skill copy(Character user) {
		return new AttireShift(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You trigger a small temporal disturbance, sending %s's clothes a couple seconds back in time. "
				+ "Due to the speed and rotation of the Earth, they probably ended up somewhere over the Pacific Ocean.", target.name());
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s triggers a device on her arm and your clothes suddenly vanish. "
				+ "What the fuck did %s just do?",self.name(),self.pronounSubject(false));
	}

}
