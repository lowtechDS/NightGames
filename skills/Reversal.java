package skills;

import stance.Cowgirl;
import stance.Doggy;
import stance.Flying;
import stance.Missionary;
import stance.Neutral;
import stance.Pin;
import stance.ReverseCowgirl;
import stance.Stance;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class Reversal extends Skill {

	public Reversal(Character self) {
		super("Reversal", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Cunning)>=24;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !c.stance.mobile(self)&&c.stance.sub(self)&&self.canSpend(15)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(15);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			if(c.stance.penetration(self)){
				if(self.hasDick()||self.has(Trait.strapped)){
					if(c.stance.behind(self)){
						if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Kat")){
							c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
						}
						c.stance=new Doggy(self,target);
					}
					else if (c.stance.en==Stance.flying) {
						if(self.getPure(Attribute.Dark)>=18){
							c.stance = new Flying(self,target);
						}else if(self.getPure(Attribute.Ninjutsu)>=5){
							c.stance = new Neutral(self,target);
						}else{	
							c.stance = c.stance.insert(self);
						}
						
					}
					else{
						if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Angel")){
							c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
						}
						c.stance=new Missionary(self,target);
					}
				}else{
					if(c.stance.prone(self)){
						if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Cassie")){
							c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
						}
						c.stance=new Cowgirl(self,target);
					}else if (c.stance.en==Stance.flying) {
						if(self.getPure(Attribute.Dark)>=18){
							c.stance = new Flying(self,target);
						}else if(self.getPure(Attribute.Ninjutsu)>=5){
							c.stance = new Neutral(self,target);
						}else{	
							c.stance = c.stance.insert(self);
						}
						
					}
					else{
						c.stance=new ReverseCowgirl(self,target);
					}
				}

			}
			else{
				c.stance=new Pin(self,target);
			}
			target.emote(Emotion.nervous, 10);
			self.emote(Emotion.dominant, 10);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning)>=24;
	}

	@Override
	public Skill copy(Character user) {
		return new Reversal(user);
	}
	public int speed(){
		return 4;
	}
	public int accuracy(){
		return 4;
	}
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You try to get on top of "+target.name()+", but she's apparently more ready for it than you realized.";
		}
		else{
			return "You take advantage of "+target.name()+"'s distraction and put her in a pin.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" tries to reverse your hold, but you stop her.";
		}
		else{
			return self.name()+" rolls you over and ends up on top.";
		}
	}

	@Override
	public String describe() {
		return "Take dominant position: 15 Mojo";
	}
}
