package skills;

import status.Bound;
import characters.Attribute;
import characters.Character;
import characters.Emotion;

import combat.Combat;
import combat.Result;

public class Binding extends Skill {

	public Binding(Character self) {
		super("Binding", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Arcane)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane)>=9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !c.stance.sub(self)&&!c.stance.prone(self)&&!c.stance.prone(target)&&self.canAct()&&self.canSpend(20);
	}

	@Override
	public String describe() {
		return "Bind your opponent's hands with a magic seal: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.add(new Bound(target,10+self.get(Attribute.Arcane),"seal"),c);
		target.emote(Emotion.nervous, 5);
		self.emote(Emotion.confident, 20);
		self.emote(Emotion.dominant, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new Binding(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You cast a binding spell on "+target.name()+", holding her in place.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" gestures at you and casts a spell. A ribbon of light wraps around your wrists and holds them in place.";
	}

}
