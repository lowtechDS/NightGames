package skills;

import status.Charmed;
import status.Stsflag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;

public class Beg extends Skill {

	public Beg(Character self) {
		super("Beg", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Submissive)>=12;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive)>=12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&!c.stance.dom(self);
	}

	@Override
	public String describe() {
		return "Beg your opponent to go easy on you";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if((Global.random(30)<=self.get(Attribute.Submissive)-(target.get(Attribute.Cunning)/2)&&!target.is(Stsflag.cynical) ||
				target.getMood()==Emotion.dominant) &&
				target.getMood()!=Emotion.angry && target.getMood() != Emotion.desperate){
			target.add(new Charmed(target),c);
			if(self.human()){
				c.write(deal(0,Result.normal,target));
			}else if(target.human()){
				c.write(receive(0,Result.normal,target));
			}
		}
		else{
			if(self.human()){
				c.write(deal(0,Result.miss,target));
			}else if(target.human()){
				c.write(receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Beg(user);
	}

	@Override
	public Tactics type() {
		return Tactics.debuff;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			if(Global.random(2)==0){
				return "You give in and humble yourself while pleading with "+target.name()+" to go easier on you. "
						+ "She just smiles and shakes her head and you get the feeling it had the opposite effect.";
			}else{
				return "You throw away your pride and ask "+target.name()+" for mercy. This just seems to encourage her sadistic side.";

			}
		}else{
			if(Global.random(2)==0){
				return "You plead with "+target.name()+" for a brief moment of reprieve. You aren�ft sure how you manage, "
						+ "but she agrees and you have a moment to rest.";
			}else{
				return "You put yourself completely at "+target.name()+"'s mercy. She takes pity on you and gives you a moment to recover.";

			}
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			if(Global.random(2)==0){
				return self.name()+" bats her eyes at you and gives you a desperate plea to ease up. "
						+ "You think it�fs time you remind her you�fre here to win, not show mercy.";
			}else{
				return self.name()+" gives you a pleading look and asks you to go light on her. "
						+ "She's cute, but she's not getting away that easily.";
			}
		}else{
			if(Global.random(2)==0){
				return "When "+self.name()+" looks at you like a kicked puppy and requests you give her a fighting chance, "
						+ "you can�ft help but give her a second to recover.";
			}else{
				return self.name()+" begs you for mercy, looking ready to cry. Maybe you should give her a break.";
			}
		}
	}

}
