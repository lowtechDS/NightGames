package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import stance.Flying;
import combat.Result;
import global.Global;

public class Fly extends Skill {

	public Fly(Character self) {
		super("Fly", self);
	}
	
	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Dark)>=18;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (this.self.canAct()) && (c.stance.mobile(this.self))
				&& (!c.stance.prone(this.self)) && (this.self.pantsless())
				&& (target.pantsless())
				&& (this.self.getStamina().get() >= 15)
				&& (this.self.getArousal().get() >= 15)
				&& (target.getArousal().get() >= 15)
				&& (!c.stance.penetration(this.self));
	}

	@Override
	public String describe() {
		return "Take off while holding your opponent and have your way "
				+ "with them in the air.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (this.self.human()) {
			c.write(self,deal(0, Result.normal, target));
		} else if (target.human()) {
			c.write(self,receive(0, Result.normal, this.self));
		}
		int m = self.get(Attribute.Dark)/2+target.get(Attribute.Perception);
		if(self.has(Trait.strapped)){
			m += +(self.get(Attribute.Science)/2);
			m = self.bonusProficiency(Anatomy.toy, m);
		}else{
			m = self.bonusProficiency(Anatomy.genitals, m);
		}
		target.pleasure(m,Anatomy.genitals,c);
		self.pleasure(self.get(Attribute.Dark)/2+self.get(Attribute.Perception),Anatomy.genitals,c);
		self.emote(Emotion.dominant,50);
		self.emote(Emotion.horny, 30);
		target.emote(Emotion.desperate, 50);
		target.emote(Emotion.nervous, 75);
		c.stance = new Flying(this.self, target);
	}

	@Override
	public Skill copy(Character target) {
		return new Fly(target);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	public boolean isPrefferedBySuccubus(Character target) {
		return target.nude();
	}
	
	@Override
	public String deal(int amount, Result modifier,
			Character target) {
		return "Summoning demonic wings, you grab " + target.name() + " tightly and take off, "
				+ (target.hasDick()&&self.hasPussy() ? "inserting his dick into your hungry pussy." :
					 " holding her helpless in the air as you thrust deep into her wet pussy.");
	}

	@Override
	public String receive(int amount, Result modifier,
			Character target) {
		return "Suddenly, " + target.name() + " leaps at you, embracing you tightly"
				+ ". She then flaps her powerful wings hard and before you know it"
				+ " you are twenty feet in the sky held up by her arms and legs."
				+ " Somehow, your dick ended up inside of her in the process and"
				+ " the rhythmic movements of her flying arouse you to no end";
	}

}
