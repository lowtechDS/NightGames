package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;

import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class StripBottom extends Skill {

	public StripBottom(Character self) {
		super("Strip Bottoms", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachBottom(self)&&!target.pantsless()&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		int strip = self.get(Attribute.Cunning);
		if(target.stripAttempt(strip,self, c, target.bottom.peek())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Yui")){
					c.offerImage("Strip Bottom Female.jpg", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			target.strip(1, c);
			if(self.getPure(Attribute.Cunning)>=30 &&! target.pantsless()){
				if(target.stripAttempt(strip,self, c, target.bottom.peek())){
					if(self.human()){
						c.write(self,deal(0,Result.strong,target));
						
					}
					else if(target.human()){
						c.write(self,receive(0,Result.strong,target));
					}
					target.strip(1, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if(self.human()&&target.nude()){
				c.write(target,target.nakedLiner());
			}
			if(target.human()&&target.pantsless()){
				if(target.getArousal().get()>=15){
					c.write("Your boner springs out, no longer restrained by your pants.");
				}
				else{
					c.write(self.name()+" giggles as your flacid dick is exposed");
				}
			}
			target.emote(Emotion.nervous, 10);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripBottom(user);
	}
	public int speed(){
		return 3;
	}
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You grab %s's %s, but %s scrambles away before you can strip %s.",
					target.name(),target.bottom.peek().getName(),target.pronounSubject(false),target.pronounTarget(false));
		}else if(modifier==Result.strong){
			return String.format("Taking advantage of the situation, you also manage to snag %s %s", target.possessive(false), target.bottom.peek().getName());
		}
		else{
			if(target.canAct()){
				return "After a brief struggle, you manage to pull off "+target.name()+"'s "+target.bottom.peek().getName()+".";
			}else{
				return "You strip off "+target.name()+"'s "+target.bottom.peek().getName()+" without much difficulty.";

			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to pull down your "+target.bottom.peek().getName()+", but you hold them up.";
		}else if(modifier==Result.strong){
			return String.format("Before you can react, %s also strips off your %s", target.pronounSubject(false), target.bottom.peek().getName());
		}
		else{
			return self.name()+" grabs the waistband of your "+target.bottom.peek().getName()+" and pulls them down.";
		}
	}

	@Override
	public String describe() {
		return "Attempt to remove opponent's pants. More likely to succeed if she's weakened and aroused";
	}
}
