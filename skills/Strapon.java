package skills;

import items.Clothing;
import items.Item;
import items.Toy;
import global.Global;
import global.Modifier;
import characters.Character;
import characters.Emotion;

import combat.Combat;
import combat.Result;

public class Strapon extends Skill {

	public Strapon(Character self) {
		super("Strap On", self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&self.pantsless()&&
				(self.has(Toy.Strapon)||self.has(Toy.Strapon2))&&
				!self.hasDick()&&!c.stance.penetration(self)&&!c.stance.penetration(target)&&
				(!self.human()||Global.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public String describe() {
		return "Put on the strapon dildo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.wear(Clothing.strapon);
		if(self.has(Toy.Strapon2)){
			c.drop(self, Toy.Strapon2);
		}
		if(self.has(Toy.Strapon)){
			c.drop(self, Toy.Strapon);
		}
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.buildMojo(10);
		target.buildMojo(-10);
		target.emote(Emotion.nervous, 10);
		self.emote(Emotion.confident, 30);
		self.emote(Emotion.dominant, 40);
	}

	@Override
	public Skill copy(Character user) {
		return new Strapon(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You put on a strap on dildo.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" straps on a thick rubber cock and grins at you in a way that makes you feel a bit nervous.";
	}

}
