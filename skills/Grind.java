package skills;

import stance.Stance;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Grind extends Skill {

	public Grind(Character self) {
		super("Grind", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=14;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=14;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.dom(self)&&c.stance.penetration(self)&&c.stance.en!=Stance.anal;
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 3+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception);
		if(self.human()){
			c.write(self,deal(m,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(m,Result.normal,self));
		}
		if(self.has(Trait.strapped)){
			m += +(self.get(Attribute.Science)/2);
			m = self.bonusProficiency(Anatomy.toy, m);
		}else{
			m = self.bonusProficiency(Anatomy.genitals, m);
		}
		target.pleasure(m,Anatomy.genitals,c);
		if(self.has(Trait.experienced)){
			self.pleasure(m/6+target.bonusRecoilPleasure(),Anatomy.genitals,c);
		}
		else{
			self.pleasure(m/3+target.bonusRecoilPleasure(),Anatomy.genitals,c);
		}
		c.stance.setPace(0);
		self.buildMojo(10);
	}

	@Override
	public Skill copy(Character user) {
		return new Grind(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You grind your hips against "+target.name()+" without thrusting. She trembles and gasps as the movement stimulates her clit and the walls of her pussy.";
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		return self.name()+" grinds against you, stimulating your entire manhood and bringing you closer to climax.";
	}

	@Override
	public String describe() {
		return "Grind against your opponent with minimal thrusting. Extremely consistent pleasure";
	}

}
