package skills;

import stance.Stance;
import status.ProfMod;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class FingerEX extends Skill {

	public FingerEX(Character self) {
		super("Shining Finger", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachBottom(self)&&(target.pantsless()||(self.has(Trait.dexterous)&&target.bottom.size()<=1))&&target.hasPussy()&&
				self.canAct()&&(!c.stance.penetration(target)||c.stance.en==Stance.anal)&&
				self.canSpend(20);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		Result style = Result.seductive;
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			style = Result.powerful;
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			style = Result.clever;
		}
		self.spendMojo(20);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(style == Result.powerful){
				m = 4 + Global.random(self.get(Attribute.Power)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
			}else if(style == Result.clever){
				m = 4 + Global.random(self.get(Attribute.Cunning)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
			}else{
				m = 4 + Global.random(self.get(Attribute.Seduction)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
				m*=1.3f;
			}

			if(self.getPure(Attribute.Professional)>=3){
				self.add(new ProfMod("Dexterous Momentum",self,Anatomy.fingers,self.get(Attribute.Professional)*5),c);
			}
			if(self.human()){
				c.write(self,deal(m,style,target));
				c.offerImage("Fingering.jpg", "Art by AimlessArt");
			}
			else if(target.human()){
				c.write(self,receive(m,style,target));
			}
			m = self.bonusProficiency(Anatomy.fingers, m);
			target.pleasure(m,Anatomy.genitals,Result.finisher,c);

		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}
	@Override
	public boolean requirements() {
		return self.get(Attribute.Seduction)>=8;
	}
	public int accuracy(){
		return 7;
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Seduction)>=8;
	}

	@Override
	public Skill copy(Character user) {
		return new FingerEX(user);
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			if(Global.random(2)==0){
				return "You try to slide your hand down to "+target.name()+"�fs pussy, but she bats your hand away.";
			}else{
				return "You grope at "+target.name()+"'s pussy, but miss.";

			}
		}
		if(modifier==Result.powerful){
			return String.format("You plunge two fingers deep into %s's wet pussy. She gasps at the strong penetration, and can barely stay "
					+ "standing as you jackhammer your fingers inside her.",target.name());
		}
		else if(modifier==Result.clever){
			return String.format("You explore %s's feminine garden with your finger, watching her reactions closely. Whenever you find a spot "
					+ "that provokes a stronger reaction, you focus on rubbing and teasing it until she's trembling with pleasure.",target.name());
		}
		else{
			return String.format("You skillfully pleasure %s's eager vagina with your best fingering technique. You cycle between tracing her labia, "
					+ "teasing her clit, and rubbing her G-spot. You switch targets frequently enough that she never gets used to the sensation.", target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		return "";
	}
	
	@Override
	public String toString(){
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Intense Finger";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Probing Finger";
		}
		return name;
	}


	@Override
	public String describe() {
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Strong forceful fingering: 20 Mojo";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Exploratory fingering to identify sensitive areas: 20 Mojo";
		}else{
			return "Mojo boosted shining finger: 20 Mojo";
		}
	}
}
