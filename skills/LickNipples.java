package skills;

import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class LickNipples extends Skill {

	public LickNipples(Character self) {
		super("Lick Nipples", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=14 && !self.has(Trait.cursed);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.topless()&&c.stance.reachTop(self)&&!c.stance.behind(self)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Cassie")){
					c.offerImage("LickNipples.jpg", "Art by Fujin Hitokiri");
				}
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			m = Global.random(4)+self.get(Attribute.Seduction)/4+target.get(Attribute.Perception)/2;
			m = self.bonusProficiency(Anatomy.mouth, m);
			target.pleasure(m,Anatomy.chest,c);
			self.buildMojo(10);
			target.emote(Emotion.horny, 5);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
		
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=14 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new LickNipples(user);
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return "You go after "+target.name()+"'s nipples, but she pushes you away.";
		}
		else{
			return "You slowly circle your tongue around each of "+target.name()+"'s nipples, making her moan and squirm in pleasure.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to suck on your chest, but you avoid her.";
		}
		else{
			return self.name()+" licks and sucks your nipples, sending a surge of excitement straight to your groin.";
		}
	}

	@Override
	public String describe() {
		return "Suck your opponent's nipples";
	}
}
