package skills;

import combat.Combat;
import combat.Result;
import characters.Character;

public class CommandStrip extends PlayerCommand {

	public CommandStrip(Character self) {
		super("Force Strip Self", self);
	}

	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && !target.nude();
	}
	
	@Override
	public String describe() {
		return "Force your opponent to strip naked.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		target.undress(c);
		if (target.human())
			c.write(self,receive(0, Result.normal, target));
		else
			c.write(self,deal(0, Result.normal, target));
	}

	@Override
	public Skill copy(Character user) {
		return new CommandStrip(user);
	}


	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "You look "
				+ target.name()
				+ " in the eye, sending a psychic command for"
				+ " her to strip. She complies without question, standing before you nude only"
				+ " seconds later.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandStrip-receive>>";
	}

}
