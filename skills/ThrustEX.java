package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import status.ProfMod;

public class ThrustEX extends Skill {

	public ThrustEX(Character self) {
		super("Smooth Thrust", self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.dom(self)&&c.stance.penetration(self)&&self.canSpend(15);
	}

	@Override
	public String describe() {
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "A deep, powerful Thrust: 15 Mojo";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "An accurate, cunning Thrust: 15 Mojo";
		}else{
			return "A more potent fuck: 15 Mojo";
		}
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m=0;
		Anatomy inside;
		Result style = Result.seductive;
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			style = Result.powerful;
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			style = Result.clever;
		}
		self.spendMojo(15);
		if(style == Result.powerful){
			m = Global.random(5+self.get(Attribute.Power))+target.get(Attribute.Perception);
		}else if(style == Result.clever){
			m = Global.random(5+self.get(Attribute.Cunning))+target.get(Attribute.Perception);
		}else{
			m = Global.random(5+self.get(Attribute.Seduction))+target.get(Attribute.Perception);
			m*=1.3f;
		}
		if(c.stance.anal()){
			if(self.human()){
				c.write(self,deal(0,Result.anal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.anal,target));
			}
			inside = Anatomy.ass;
		}
		else{
			if(self.getPure(Attribute.Professional)>=11){
				if(self.human()){
					c.write(self,deal(0,Result.special,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.special,target));
				}
				self.add(new ProfMod("Sexual Momentum",self,Anatomy.genitals,self.get(Attribute.Professional)*5),c);
			}
			if(self.human()){
				c.write(self,deal(0,style,target));
			}
			else if(target.human()){
				c.write(self,receive(0,style,target));
			}
			inside = Anatomy.genitals;
		}
		if(self.has(Trait.strapped)){
			m += +(self.get(Attribute.Science)/2);
			m = self.bonusProficiency(Anatomy.toy, m);
		}else{
			m = self.bonusProficiency(Anatomy.genitals, m);
		}
		target.pleasure(m,inside,Result.finisher,c);
		if(self.has(Trait.experienced)){
			self.pleasure(m/4+target.bonusRecoilPleasure(),Anatomy.genitals,c);
		}
		else{
			self.pleasure(m/2+target.bonusRecoilPleasure(),Anatomy.genitals,c);
		}
		c.stance.setPace(0);
	}

	@Override
	public Skill copy(Character user) {
		return new ThrustEX(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.powerful){
			return String.format("You thrust deep into %s's pussy, in slow, powerful strokes. You push so deep with every thrust that your "
					+ "cock bumps against cervix.",target.name());
		}
		else if(modifier==Result.clever){
			return String.format("You angle your hips and thrust into %s, carefully aiming for her G-spot. You make sure each stroke brushes your "
					+ "pelvis against her clit, maximizing her pleasure.",target.name());
		}
		else{
			return String.format("You thrust your hips rhythmically, creating a smooth and contant motion. Your sensual strokes have no clear "
					+ "start or finish, and %s is quickly pulled into your rhythm as you pleasure her pussy.",target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			String using = "cock";
			if(self.has(Trait.strapped)){
				using = "strapon dildo";
			}
			if(modifier==Result.powerful){
				return String.format("%s shoves her %s deep into ass with such a powerful stroke that you barely stay standing.",self.name());
			}
			else if(modifier==Result.clever){
				return String.format("%s skillfully prods and torments your prostate with the head of her %s.",self.name(),using);
			}
			else{
				return String.format("%s thrusts her hips, pumping her %s in and out of your ass like a well-oiled machine. The rhythmic strokes "
						+ "send dizzying jolts of pleasure up your spine."
						,self.name(),using);
			}
		}
		else{
			if(modifier==Result.powerful){
				return String.format("%s lifts herself almost off your cock before tightening her vaginal walls around your glans. She "
						+ "drops her hips, taking your dick completely inside. The tight sensation along your entire length takes your "
						+ "breath away.",self.name());
			}
			else if(modifier==Result.clever){
				return String.format("%s switches up her movements, stopping briefly at the top of each thrust to stimulate your sensitive "
						+ "cockhead with her entrance. The extra attention to the sensitive area arouses you faster than usual.",self.name());
			}
			else{
				return String.format("%s moves her hips like a skilled dancer, riding you with smooth, continuous motion. The constant stimulation "
						+ "effectively milks your dick, brining you closer and closer to cumming.",self.name());
			}
		}
	}
	
	@Override
	public String toString(){
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Strong Thrust";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Precise Thrust";
		}
		return name;
	}
}
