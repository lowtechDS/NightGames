package skills;

import stance.Stance;
import status.ProfMod;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Finger extends Skill {

	public Finger(Character self) {
		super("Finger", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachBottom(self)&&(target.pantsless()||(self.has(Trait.dexterous)&&target.bottom.size()<=1))&&target.hasPussy()&&self.canAct()&&(!c.stance.penetration(target)||c.stance.en==Stance.anal);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			int m = 0;
			if(self.getPure(Attribute.Professional)>=3){
				m = 4+ Global.random(self.get(Attribute.Seduction)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
				if(self.human()){
					c.write(self,deal(m,Result.special,target));
					c.offerImage("Fingering.jpg", "Art by AimlessArt");
				}
				else if(target.human()){
					c.write(self,receive(m,Result.special,target));
					c.offerImage("Fingering.jpg", "Art by AimlessArt");
				}
				
				self.buildMojo(15);
				self.add(new ProfMod("Dexterous Momentum",self,Anatomy.fingers,self.get(Attribute.Professional)*5),c);
			}
			else if(self.get(Attribute.Seduction)>=8){
				m = 4+ Global.random(self.get(Attribute.Seduction))+target.get(Attribute.Perception);
				if(self.human()){
					c.write(self,deal(m,Result.normal,target));
					c.offerImage("Fingering.jpg", "Art by AimlessArt");
				}
				self.buildMojo(10);
			}
			else{
				m = 4+target.get(Attribute.Perception);
				if(self.human()){
					c.write(self,deal(m,Result.weak,target));
					c.offerImage("Fingering.jpg", "Art by AimlessArt");
				}
				self.buildMojo(5);
			}
			m = self.bonusProficiency(Anatomy.fingers, m);
			target.pleasure(m,Anatomy.genitals,Result.finisher,c);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}
	@Override
	public boolean requirements() {
		return true;
	}
	public int accuracy(){
		return 7;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Finger(user);
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			if(Global.random(2)==0){
				return "You try to slide your hand down to _�fs pussy, but she bats your hand away.";
			}else{
				return "You grope at "+target.name()+"'s pussy, but miss.";

			}
		}
		if(!target.bottom.isEmpty()){
			if(modifier == Result.weak){
				return "You fumble as you slip your hand under "+target.name()+"'s "+target.bottom.peek().getName()+". You manage to get under it and run your fingers across her "
						+ "soft flesh, hoping for a positive response. After some poking and prodding, you withdraw your hand.";
			}else{
				if(Global.random(2)==0){
					return "You slip your hand under "+target.name()+"�fs "+target.bottom.peek().getName()+" and curl a finger inside of her.";
				}else{
					return "You slip your hand under "+target.name()+"'s "+target.bottom.peek().getName()+" and stroke her sensitive petals.";
				}
			}
			
		}
		else if(modifier == Result.weak){
			return "You grope between "+target.name()+"'s legs, not really knowing what you're doing. You don't know where she's the most sensitive, so you rub and " +
					"stroke every bit of moist flesh under your fingers.";
		}
		else{
			if(target.getArousal().get()<=15){
				
				return "You softly rub the petals of "+target.name()+"'s closed flower.";
			}
			else if(target.getArousal().percent()<50){
				if(Global.random(2)==0){
					return "You are gently stroking up and down "+target.name()+"�fs sex when you feel your fingertip become wet with her juices. "
							+ "You speed up your movement, coating her in the supplies lubricant.";
				}else{
					return target.name()+"'s sensitive lower lips start to open up under your skilled touch and you can feel her becoming wet.";
				}
			}
			else if(target.getArousal().percent()<80){
				if(Global.random(2)==0){
					return "You spread "+target.name()+"'s lower lips and tease her wet entrance with another finger.";
				}else{
					return "You locate "+target.name()+"'s clitoris and caress it directly, causing her to tremble from the powerful stimulation.";

				}
			}
			else{
				if(Global.random(2)==0){
					return "Two of your fingers gracefully penetrate "+target.name()+" and press against her G-spot, making her gasp. She moans in chorus as your fingers continue their skilled work.";
				}else{
					return "You stir "+target.name()+"'s increasingly soaked pussy with your fingers and rub her clit with your thumb.";

				}
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString(){
		if(self.getPure(Attribute.Professional)>=3){
			return "Pro Finger";
		}else{
			return "Finger";
		}
	}


	@Override
	public String describe() {
		if(self.getPure(Attribute.Professional)>=3){
			return "A professional fingering technique that increases effectiveness with repeated use";

		}else{
			return "Digitally stimulate opponent's pussy";
		}
	}
}
