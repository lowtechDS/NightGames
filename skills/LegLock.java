package skills;

import status.Abuff;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class LegLock extends Skill {

	public LegLock(Character self) {
		super("Leg Lock", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.dom(self)&&c.stance.reachBottom(self)&&c.stance.prone(target)&&self.canAct()&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			target.add(new Abuff(target, Attribute.Speed,-2,5),c);
			target.pain(Global.random(10)+7,Anatomy.leg,c);
			target.emote(Emotion.angry,25);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Power)>=24 && !self.has(Trait.cursed);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=24 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new LegLock(user);
	}
	public int speed(){
		return 2;
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return "You grab "+target.name()+"'s leg, but she kicks free.";
		}
		else{
			return "You take hold of "+target.name()+"'s ankle and force her leg to extend painfully.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to put you in a leglock, but you slip away.";
		}
		else{
			return self.name()+" pulls your leg across her in a painful submission hold.";
		}
	}

	@Override
	public String describe() {
		return "A submission hold on your opponent's leg";
	}
}
