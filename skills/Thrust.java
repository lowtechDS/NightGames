package skills;

import stance.Stance;
import status.ProfMod;
import global.Global;
import combat.Combat;
import combat.Result;

import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

public class Thrust extends Skill {

	public Thrust(Character self) {
		super("Thrust", self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.dom(self)&&c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		Anatomy inside;
		if(c.stance.anal()){
			if(self.human()){
				c.write(self,deal(0,Result.anal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.anal,target));
			}
			inside = Anatomy.ass;
		}
		else{
			if(self.getPure(Attribute.Professional)>=11){
				if(self.human()){
					c.write(self,deal(0,Result.special,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.special,target));
				}
				self.add(new ProfMod("Sexual Momentum",self,Anatomy.genitals,self.get(Attribute.Professional)*5),c);
			}
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			inside = Anatomy.genitals;
		}
		m = Global.random(5+self.get(Attribute.Seduction))+target.get(Attribute.Perception);
		self.buildMojo(10);
		if(self.has(Trait.strapped)){
			m += +(self.get(Attribute.Science)/2);
			m = self.bonusProficiency(Anatomy.toy, m);
		}else{
			m = self.bonusProficiency(Anatomy.genitals, m);
		}
		target.pleasure(m,inside,Result.finisher,c);
		if(self.has(Trait.experienced)){
			self.pleasure(m/4+target.bonusRecoilPleasure(),Anatomy.genitals,c);
		}
		else{
			self.pleasure(m/2+target.bonusRecoilPleasure(),Anatomy.genitals,c);
		}
		c.stance.setPace(0);
	}

	@Override
	public Skill copy(Character user) {
		return new Thrust(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			return "You thrust steadily into "+target.name()+"'s ass, eliciting soft groans of pleasure.";
		}
		else{
			return "You thrust into "+target.name()+" in a slow, steady rhythm. She lets out soft breathy moans in time with your lovemaking. You can't deny you're feeling " +
					"it too, but by controlling the pace, you can hopefully last longer than she can.";
		}
				
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			if(self.has(Trait.strapped)){
				return self.name()+" thrusts her hips, pumping her artificial cock in and out of your ass and pushing on your prostate.";
			}
			else{
				return self.name()+"'s cock slowly pumps the inside of your rectum.";
			}
		}
		else{
			if(modifier == Result.special){
				switch (Global.random(3)) {
					case 0:
						return String.format(
								"%s is holding just the tip of your dick inside of her slick pussy,"
										+ " rubbing it against the soft, tight entrance. Slowly	moving"
										+ " her hips, she is driving you crazy.",
								self.name());
					case 1:
						return String.format(
								"%s slides down fully onto you, squeezing you tightly"
										+ " with her vaginal walls. The muscles are wound so tight that it's nearly"
										+ " impossible to move at all, but she pushes down hard and eventually"
										+ " all of your cock is lodged firmly inside of her.",
								self.name());
					default:
						return String.format(
								"%s moves up and down your rock-hard cock while the velvet vise"
										+ " of her pussy is undulating on your shaft, sending ripples along it"
										+ " as if milking it. Overcome with pleasure, your entire body tenses up and"
										+ " you throw your head back, trying hard not to cum instantly.",
								self.name());
				}
			}
			else{
				return self.name()+" rocks her hips against you, riding you smoothly and deliberately. Despite the slow pace, the sensation of her hot, wet pussy surrounding " +
					"your dick is gradually driving you to your limit.";
			}
		}
				
	}

	@Override
	public String toString(){
		if(self.getPure(Attribute.Professional)>=11){
			return "Pro Thrust";
		}
		else{
			return "Thrust";
		}
	}	
	
	@Override
	public String describe() {
		if(self.getPure(Attribute.Professional)>=11){
			return "A professional thrusting technique that gains momentum over time";
		}
		else{
			return "Slow fuck, minimizes own pleasure";
		}
	}

}
