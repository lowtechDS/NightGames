package skills;

import stance.Mount;
import characters.Character;

import combat.Combat;
import combat.Result;

public class Straddle extends Skill {

	public Straddle(Character self) {
		super("Mount", self);
		
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		
		return c.stance.mobile(self)&&c.stance.mobile(target)&&c.stance.prone(target)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		c.stance=new Mount(self,target);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Straddle(user);
	}
	public int speed(){
		return 6;
	}
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You straddle "+target.name()+" using your body weight to hold her down.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" plops herself down on top of your stomach.";
	}

	@Override
	public String describe() {
		return "Straddles opponent";
	}
}
