package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;

public class HoneyTrap extends Skill {

	public HoneyTrap(Character self) {
		super("Honey Trap", self);
	}

	@Override
	public boolean requirements() {
		return self.get(Attribute.Submissive)>=21;
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Submissive)>=21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (target.pantsless()||target.has(Trait.strapped))
				&&self.pantsless()
				&&c.stance.mobile(target)
				&&!c.stance.mobile(self)
				&&((target.hasDick()||(target.has(Trait.strapped))&&!c.stance.behind(self))||!c.stance.behind(target))
				&&self.canAct()
				&&target.canAct()
				&&!c.stance.penetration(self)
				&&!c.stance.penetration(target)
				&&self.canSpend(20);
	}

	@Override
	public String describe() {
		return "A sneak attack right at the moment of greatest anticipation: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
			if(Global.random(5)>=3){
				c.write(self,self.bbLiner());
			}
		}
		int m = Math.max(Math.max(self.get(Attribute.Power),self.get(Attribute.Cunning)),self.get(Attribute.Seduction));
		int bonus = self.get(Attribute.Submissive);
		if(target.getMood()==Emotion.dominant || target.getMood()==Emotion.horny){
			bonus*=2;
		}
		target.pain(m+bonus, Anatomy.genitals,c);
		if(self.has(Trait.wrassler)){
			target.calm(m/2,c);
		}
		else{
			target.calm(m,c);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new HoneyTrap(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You slide your eager cock against %s's pussy lips, making it clear what you're craving. She grins, confident in her victory, and positions herself over you. "
				+ "You wait patiently until the moment her guard is completely down. Before she can fuck you, you deliver a sudden painful slap to her exposed pussy. For a moment, her "
				+ "expression is pure shock, then her eyes water as the pain hits her.", target.name());
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s opens her lower lips in a lewd invitation and looks at you with needy eyes. There's no way you'd turn down such a tempting offer. You line up your cock with "
				+ "her dripping entrance and prepare to penetrate her. Suddenly her knee darts up and impacts your dangling balls. It's so fast that for a moment, you aren't even sure what happened. "
				+ "The pain hits you like a truck, and you're soon unable to do anything but clutch your poor testicles."
				, self.name());
	}

}
