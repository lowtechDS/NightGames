package skills;

import stance.ReverseMount;
import stance.SixNine;
import status.ProfMod;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Blowjob extends Skill {

	public Blowjob(Character self) {
		super("Blow", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless()&&target.hasDick()&&c.stance.oral(self)&&!c.stance.behind(self)&&self.canAct()&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			int m;
			if(self.getPure(Attribute.Professional)>=5){
				if(self.has(Trait.silvertongue)){
					m = Global.random(8)+self.get(Attribute.Professional)+(2*self.get(Attribute.Seduction)/3)+target.get(Attribute.Perception);
					if(target.human()){
						if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Cassie")){
							c.offerImage("Blowjob.png", "Art by Phanaxial");
						}						
						c.write(self,receive(m,Result.special,target));
					}
					else if(self.human()){
						c.write(self,deal(m,Result.special,target));
					}
				}
				else{
					m = Global.random(6)+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception);
					if(target.human()){
						if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Cassie")){
							c.offerImage("Blowjob.png", "Art by Phanaxial");
						}
						c.write(self,receive(m,Result.special,target));
					}
					else if(self.human()){
						c.write(self,deal(m,Result.special,target));
					}
				}
				self.add(new ProfMod("Oral Momentum",self,Anatomy.mouth,self.get(Attribute.Professional)*5),c);
			}
			else if(self.has(Trait.silvertongue)){
				m = Global.random(8)+(2*self.get(Attribute.Seduction)/3)+target.get(Attribute.Perception);
				if(target.human()){
					if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Cassie")){
						c.offerImage("Blowjob.png", "Art by Phanaxial");
					}
					c.write(self,receive(m,Result.strong,target));
				}
				else if(self.human()){
					c.write(self,deal(m,Result.strong,target));
				}
			}
			else{
				m = Global.random(6)+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception);
				if(target.human()){
					if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Cassie")){
						c.offerImage("Blowjob.png", "Art by Phanaxial");
					}
					c.write(self,receive(m,Result.normal,target));
				}
				else if(self.human()){
					c.write(self,deal(m,Result.normal,target));
				}
			}
			m += self.getMojo().get()/10;
			if(target.has(Trait.lickable)){
				m*=1.5;
			}
			m = self.bonusProficiency(Anatomy.mouth, m);
			target.pleasure(m,Anatomy.genitals,c);
			if(ReverseMount.class.isInstance(c.stance)){
				c.stance=new SixNine(self,target);
			}
		}
		else{
			if(self.human()){
				c.write(deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=10;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=10;
	}
	public int accuracy(){
		return 6;
	}
	@Override
	public Skill copy(Character user) {
		return new Blowjob(user);
	}
	public int speed(){
		return 2;
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			if(Global.random(2)==0){
				return "You thrust your head towards "+target.name()+"'s penis, but miss and it brushes against your cheek instead.";
			}else{
				return "You try to take "+target.name()+"'s penis into your mouth, but she manages to pull away.";

			}
		}
		if(target.getArousal().get()<15){
			return "You suck on "+target.name()+" flaccid little penis until it grows into an intimidatingly large erection.";
		}
		else if(target.getArousal().percent()>=90){
			if(Global.random(2)==0){
				return "You suck away on "+target.name()+"'s girl-cock and you�fve developed a new appreciation for all the blowjobs you�fve "
						+ "received in the past. You lick and slurp the underside of the shaft before choking on a mix of your "
						+ "own saliva and precum. Thankfully, this display only seems to turn her on more.";
			}else{
				return target.name()+"'s girl-cock seems ready to burst, so you suck on it strongly and attack the glans with your tongue fiercely.";
			}
		}
		else if(modifier==Result.strong){
			return "You put your skilled tongue to good use tormenting and teasing her unnatural member.";
		}
		else if(modifier==Result.special){
			switch(Global.random(3)){
			case 0:
				return String.format("You go to town on %s's cock, licking it all over."
								+ " Long, slow licks along the shaft and small, swift licks"
								+ " around the head cause %s to groan in pleasure.",
								target.name(),target.pronounTarget(false));
			case 1:
				return String.format("You lock your lips around the head of %s's hard, wet dick "
								+ "and suck on it forcefully while swirling your tongue rapidly"
								+ " around. At the same time, your hands massage and"
								+ " caress every bit of sensitive flesh not covered by"
								+ " your mouth.",
								target.name());
			default:
				return String
						.format("You are bobbing up and down now, hands still working"
								+ " on any exposed skin while you lick, suck and even nibble all over %s's"
								+ " over-stimulated manhood. %s is not even trying to hide %s"
								+ " enjoyment, and %s grunts loudly every time your teeth graze"
								+ " %s shaft.",
								target.name(), target.pronounSubject(true),
								target.possessive(false), target.pronounSubject(false),target.possessive(false));
		}
		}
		else{
			if(Global.random(2)==0){
				return "You settle into a rhythm and find yourself surprisingly sucked into teasing "+target.name()+"'s cock with your mouth. "
						+ "Based on her groans, you are definitely doing something right. As if in confirmation, she thrusts "
						+ "her girl-cock against the back of your throat and you both jerk in reaction.";
			}else{
				return "You feel a bit odd, faced with "+target.name()+"'s rigid cock, but as you lick and suck on it, you discover the taste is quite palatable. Besides, " +
						"making "+target.name()+" squirm and moan in pleasure is well worth it.";
			}
			
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			if(Global.random(2)==0){
				return self.name()+"clearly intends to try and suck you off, so as she lowers her head, you juke to the side instead.";
			}else{
				return self.name()+" tries to suck your cock, but you pull your hips back to avoid her.";
			}
			
		}
		if(target.getArousal().get()<15){
			if(Global.random(2)==0){
				return "Your penis reacts almost immediately to the warmth of "+self.name()+"'s mouth and instantly hardens in her mouth.";
			}else{
				return self.name()+" takes your soft penis into her mouth and sucks on it until it hardens";
			}
			
		}
		else if(target.getArousal().percent()>=90){
			return self.name()+" laps up the precum leaking from your cock and takes the entire length into her mouth, sucking relentlessly";
		}
		else if(modifier==Result.strong){
			return self.name()+"'s soft lips and talented tongue work over your dick, drawing out dangerously irresistible pleasure with each touch.";
		}
		else if(modifier==Result.special){
			switch(Global.random(3)){
				case 0:
					return String.format("%s goes to town on your cock, licking it all over."
									+ " Long, slow licks along the shaft and small, swift licks"
									+ " around the head cause you to groan in pleasure.",
									self.name());
				case 1:
					return String.format("%s locks %s lips around the head of your hard and wet dick "
									+ "and sucks on it forcefully while swirling %s tongue rapidly"
									+ " around. At the same time, %s hands are massaging and"
									+ " caressing every bit of sensitive flesh not covered by"
									+ " %s mouth.",
									self.name(),self.possessive(false),self.possessive(false),
									self.possessive(false),self.possessive(false));
				default:
					return String
							.format("%s is bobbing up and down now, hands still working"
									+ " on any exposed skin while %s licks, sucks and even nibbles all over your"
									+ " over-stimulated manhood. you are not even trying to hide your"
									+ " enjoyment, and you grunt loudly every time %s teeth graze"
									+ " your shaft.",
									self.name(), self.pronounSubject(false),
									self.possessive(false));
			}
		}
		else{
			int r = Global.random(4);
			if(r==0){
				return self.name()+" runs her tongue up the length of your dick, sending a jolt of pleasure up your spine. She slowly wraps her lips around your dick and sucks.";
			}
			else if(r==1){
				return self.name()+" sucks on the head of your cock while her hand strokes the shaft.";
			}
			else if(r==2){
				return self.name()+" licks her way down to the base of your cock and gently sucks on your balls.";
			}
			else{
				return self.name()+" runs her tongue around the glans of your penis and teases your urethra.";
			}
		}
	}

	@Override
	public String describe() {
		return "Lick and suck your opponent's dick, more effective at high Mojo";
	}
}
