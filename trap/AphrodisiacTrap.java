package trap;

import items.Component;
import items.Flask;
import items.Item;
import status.Flatfooted;
import global.Global;
import combat.Combat;
import combat.Encounter;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class AphrodisiacTrap implements Trap {
	private Character owner;
	@Override
	public void trigger(Character target) {
		if(!target.check(Attribute.Perception, 15-(target.get(Attribute.Perception)+target.bonusDisarm()))){
			if(target.human()){
				Global.gui().message("You spot a liquid spray trap in time to avoid setting it off. You carefully manage to disarm the trap and pocket the potion.");
				target.gain(Flask.Aphrodisiac);
				target.location().remove(this);
			}
		}
		else{
			if(target.human()){
				Global.gui().message("There's a sudden spray of gas in your face and the room seems to get much hotter. Your dick goes rock-hard and you realize you've been " +
						"hit with an aphrodisiac.");
			}
			else if(target.location().humanPresent()){
				Global.gui().message(target.name()+" is caught in your trap and sprayed with aphrodisiac. She flushes bright red and presses a hand against her crotch. It seems like " +
						"she'll start masturbating even if you don't do anything.");
			}
			target.tempt(40);
			target.location().opportunity(target,this);
		}
	}

	@Override
	public boolean decoy() {
		return false;
	}

	@Override
	public boolean recipe(Character owner) {
		return owner.has(Flask.Aphrodisiac)&&owner.has(Component.Tripwire)&&owner.has(Component.Sprayer)&&!owner.has(Trait.direct);
	}

	@Override
	public String setup(Character owner) {
		this.owner=owner;
		owner.consume(Component.Tripwire, 1);
		owner.consume(Flask.Aphrodisiac, 1);
		owner.consume(Component.Sprayer, 1);
		return "You set up a spray trap to coat an unwary opponent in powerful aphrodisiac.";
	}

	@Override
	public Character owner() {
		return owner;
	}
	public String toString(){
		return "Aphrodisiac Trap";
	}

	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Cunning)>=12 && !owner.has(Trait.direct);
	}

	@Override
	public void capitalize(Character attacker, Character victim,Encounter enc) {
		victim.add(new Flatfooted(victim,1));
		enc.engage(new Combat(attacker,victim,attacker.location()));
		attacker.location().remove(this);
	}
	@Override
	public void resolve(Character active) {
		if(active!=owner){
			trigger(active);
		}
	}

	@Override
	public int priority() {
		return 3;
	}
	public String description() {
		return "Sprays an aphrodisiac to arouse whoever trips it";
	}
}
