package trap;

import items.Component;
import items.Consumable;
import items.Flask;
import items.Item;
import status.Flatfooted;
import global.Global;
import combat.Combat;
import combat.Encounter;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class DissolvingTrap implements Trap {
	private Character owner;
	@Override
	public void trigger(Character target) {
		if(!target.check(Attribute.Perception, 20-(target.get(Attribute.Perception)+target.bonusDisarm()))){
			if(target.human()){
				Global.gui().message("You spot a liquid spray trap in time to avoid setting it off. You carefully manage to disarm the trap and pocket the potion.");
				target.gain(Flask.DisSol);
				target.location().remove(this);
			}
		}
		else{
			if(target.human()){
				if(target.nude()){
					Global.gui().message("Your bare foot hits a tripwire and you brace yourself as liquid rains down on you. You hastely do your best to brush the liquid off, " +
							"but after about a minute you realize nothing has happened. Maybe the trap was a dud.");
				}
				else{
					Global.gui().message("You are sprayed with a clear liquid. Everywhere it lands on clothing, it immediately dissolves it, but it does nothing to your skin. " +
							"You try valiantly to save enough clothes to preserve your modesty, but you quickly end up naked.");
				}
			}
			else if(target.location().humanPresent()){
				if(target.nude()){
					Global.gui().message(target.name()+" is caught in your clothes dissolving trap, but she was already naked. Oh well.");
				}
				else{
					Global.gui().message(target.name()+" is caught in your trap and is showered in dissolving solution. In seconds, her clothes vanish off her body, leaving her " +
							"completely nude.");
				}
			}
			target.nudify();
			target.location().opportunity(target,this);
		}
	}

	@Override
	public boolean decoy() {
		return false;
	}

	@Override
	public boolean recipe(Character owner) {
		return owner.has(Component.Tripwire)&&owner.has(Flask.DisSol)&&owner.has(Component.Sprayer)&&!owner.has(Trait.direct);
	}

	@Override
	public String setup(Character owner) {
		this.owner=owner;
		owner.consume(Component.Tripwire, 1);
		owner.consume(Flask.DisSol, 1);
		owner.consume(Component.Sprayer, 1);
		return "You rig up a trap to dissolve the clothes of whoever triggers it.";
	}

	@Override
	public Character owner() {
		return owner;
	}

	public String toString(){
		return "Dissolving Trap";
	}
	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Cunning)>=11 && !owner.has(Trait.direct);
	}

	@Override
	public void capitalize(Character attacker, Character victim, Encounter enc) {
		victim.add(new Flatfooted(victim,1));
		enc.engage(new Combat(attacker,victim,attacker.location()));
		attacker.location().remove(this);	
	}
	@Override
	public void resolve(Character active) {
		if(active!=owner){
			trigger(active);
		}
	}

	@Override
	public int priority() {
		return 3;
	}

	@Override
	public String description() {
		return "Will completely strip the victim";
	}
}
