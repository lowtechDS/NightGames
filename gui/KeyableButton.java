package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public abstract class KeyableButton extends JButton {
    private static final long serialVersionUID = -2379908542190189603L;
    private final String text;

    public KeyableButton(String text) {
        super(text);
        this.text = text;

    }
    public void call() {
        doClick();
    }

    public void setHotkeyTextTo(String string) {
        setText(String.format("%s [%s]", text, string));
    }

    public void clearHotkeyText() {
        setText(text);
    }

}