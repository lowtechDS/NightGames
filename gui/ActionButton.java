package gui;

import global.Global;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import skills.Tactics;

import actions.Action;
import characters.Character;

public class ActionButton extends KeyableButton{
	protected Action action;
	protected Character user;
	public ActionButton(Action action,Character user){
		super(action.toString());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		this.action=action;
		this.user=user;
		if(!action.tooltip().isEmpty()){
			this.setToolTipText(action.tooltip());
		}
		setBackground(action.consider().getColor());
		addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Global.gui().clearText();
				ActionButton.this.action.execute(ActionButton.this.user);
				if(!ActionButton.this.action.freeAction())
					Global.getMatch().resume();
			}			
		});
	}
}
