package gui;

import global.Constants;
import global.Flag;
import global.Global;
import global.Modifier;
import items.Clothing;
import items.ClothingType;
import items.Consumable;
import items.Flask;
import items.Item;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.Painter;
import javax.swing.border.CompoundBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.ColorUIResource;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;

import skills.Skill;
import skills.TacticGroup;
import skills.Tactics;
import status.Stsflag;
import trap.Trap;
import actions.Action;
import actions.Locate;
import characters.Attribute;
import characters.Character;
import characters.Meter;
import characters.Player;
import characters.Trait;
import combat.Combat;
import combat.Encounter;
import combat.Encs;
import daytime.Activity;
import daytime.Store;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import java.awt.Component;
import javax.swing.Box;
import javax.swing.border.EtchedBorder;

public class GUI extends JFrame implements Observer {

	protected Combat combat;
	private Player player;
	private HashMap<TacticGroup, ArrayList<SkillButton>> skills;
	private ArrayList<SkillButton> flasks;
	private ArrayList<SkillButton> potions;
	private ArrayList<SkillButton> demands;
	private String bodytext;
	private CommandPanel commandPanel;
	private Box groupBox;
	private JTextPane textPane;
	private Title titlePanel;
	private JLabel stamina;
	private JLabel arousal;
	private JLabel mojo;
	private JLabel lvl;
	private JLabel xp;
	private JProgressBar staminaBar;
	private JProgressBar arousalBar;
	private JProgressBar mojoBar;
	private JPanel topPanel;
	private JLabel loclbl;
	private JLabel timelbl;
	private JLabel cashlbl;
	private Panel panel0;
	private CreationGUI creation;
	private JScrollPane textScroll;
	private JPanel mainpanel;
	private JToggleButton invbtn;
	private JToggleButton stsbtn;
	private JPanel inventoryPanel;
	private JPanel statusPanel;
	private JPanel centerPanel;
	private JPanel clothesPanel;
	private JLabel clothesdisplay;
	private JPanel optionspanel;
	private JPanel portraitPanel;
	private JLabel portrait;
	private JLabel sprite;
	private JComponent map;
	private JLabel imgPanel;
	//Options
	private JRadioButton rdnormal;
	private JRadioButton rdhard;
	private JRadioButton rdautosaveon;
	private JRadioButton rdautosaveoff;
	private JRadioButton rdpor1;
	private JRadioButton rdpor2;
	private JRadioButton rdpor3;
	private JRadioButton rdimgon;
	private JRadioButton rdimgoff;
	private JRadioButton rdimgexact;
	private JRadioButton rdstsimgon;
	private JRadioButton rdstsimgoff;
	private JRadioButton rdfntnorm;
	private JRadioButton rdnfntlrg;
	private JRadioButton rdrpt1;
	private JRadioButton rdrpt2;
	private JRadioButton rdcol1;
	private JRadioButton rdcol2;
	//ColorScheme
	private Color textColor;
	private Color frameColor;
	private Color backgroundColor;
	private int width;
	private int height;
	public int fontsize;
	private JMenuItem mntmQuitMatch;
	private JMenuItem mntmOptions;
	private JPanel midPanel;
	private ClothesChangeGUI closet;
	private TacticGroup currentTactics;
	private boolean lowres;
	
    private final static String USE_PORTRAIT = "PORTRAIT";
    private final static String USE_MAP = "MAP";
    private final static String USE_NONE = "NONE";
    private final static String USE_SPRITE = "SPRITE";
    private static final String USE_MAIN_TEXT_UI = "MAIN_TEXT";
    private static final String USE_CLOSET_UI = "CLOSET";
	private JPanel groupPanel;
	
	BufferedImage BarrierFilter = null;
	BufferedImage BondageFilter = null;
	BufferedImage BoundFilter = null;
	BufferedImage BracedFilter = null;
	BufferedImage BuzzedFilter = null;
	BufferedImage CaffeineFilter = null;
	BufferedImage CharmedFilter = null;
	BufferedImage DissolvingFilter = null;
	BufferedImage DistractedFilter = null;
	BufferedImage DrowsyFilter = null;
	BufferedImage EnthralledFilter = null;
	BufferedImage FeralFilter = null;
	BufferedImage FireformFilter = null;
	BufferedImage HornyFilter = null;
	BufferedImage HypersensitiveFilter = null;
	BufferedImage IceformFilter = null;
	BufferedImage MasochismFilter = null;
	BufferedImage OiledFilter = null;
	BufferedImage ShamedFilter = null;
	BufferedImage StoneformFilter = null;
	BufferedImage WaterformFilter = null;
	BufferedImage WindedFilter = null;
	
	
	public GUI() {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}
		
		setBackground(Color.GRAY);

		// frame title
		setTitle("Night Games");

		// closing operation
		setDefaultCloseOperation(3);

		// resolution resolver
		if(Toolkit.getDefaultToolkit().getScreenSize().getHeight()>=1000){
			height=900;
		}else{
			height=720;
			lowres = true;
		}
		if(Toolkit.getDefaultToolkit().getScreenSize().getWidth()>=1600){
			width=1600;
		}else{
			width=1024;
			lowres = true;
		}
		setPreferredSize(new Dimension(width, height));
		// center the window on the monitor

		int y = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		int x = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();

		int x1 = x / 2 - width / 2;
		int y1 = y / 2 - height / 2;

		int centerX = x1;
		int centerY = y1;

		this.setLocation(centerX, centerY);
		
		titlePanel = new Title(this, width, height);
		getContentPane().add(titlePanel);
		pack();
		setVisible(true);

	}
	public void BuildGUI(){
		getContentPane().remove(titlePanel);
		// Colors
		textColor = Constants.PRIMARYTEXTCOLOR;
		frameColor = Constants.PRIMARYFRAMECOLOR;
		backgroundColor = Constants.PRIMARYBGCOLOR;

		
		// menu bar
		getContentPane().setLayout(new BoxLayout(getContentPane(), 1));
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenuItem mntmNewgame = new JMenuItem("New Game");
		mntmNewgame.setForeground(textColor);
		mntmNewgame.setBackground(frameColor);
		mntmNewgame.setHorizontalAlignment(SwingConstants.CENTER);
		mntmNewgame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int result = JOptionPane.showConfirmDialog(GUI.this,
						"Do you want to restart the game? You'll lose any unsaved progress.", "Start new game?",
						JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
				if(result==JOptionPane.OK_OPTION){
					Global.reset();
				}
			}
		});
		menuBar.add(mntmNewgame);
		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.setForeground(textColor);
		mntmLoad.setBackground(frameColor);
		mntmLoad.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mntmLoad);
		mntmLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Global.load();
			}
		});
		// menu bar - options
		mntmOptions = new JMenuItem("Options");
		mntmOptions.setForeground(textColor);
		mntmOptions.setBackground(frameColor);
		menuBar.add(mntmOptions);
		// options submenu creator
		optionspanel = new JPanel();
		optionspanel.setLayout(new GridLayout(0,3,0,0));
		JLabel lbldif = new JLabel("AI Difficulty");
		ButtonGroup dif = new ButtonGroup();
		rdnormal = new JRadioButton("Normal");
		rdhard = new JRadioButton("Hard");
		dif.add(rdnormal);
		dif.add(rdhard);
		optionspanel.add(lbldif);
		optionspanel.add(rdnormal);
		optionspanel.add(rdhard);
		JLabel lblauto = new JLabel("Autosave (saves to auto.sav)");
		ButtonGroup auto = new ButtonGroup();
		rdautosaveon = new JRadioButton("On");
		rdautosaveoff = new JRadioButton("Off");
		auto.add(rdautosaveon);
		auto.add(rdautosaveoff);
		optionspanel.add(lblauto);
		optionspanel.add(rdautosaveon);
		optionspanel.add(rdautosaveoff);
		ButtonGroup portype = new ButtonGroup();
		rdpor1 = new JRadioButton("Sprite");
		rdpor2 = new JRadioButton("Portrait");
		rdpor3 = new JRadioButton("None");
		portype.add(rdpor1);
		portype.add(rdpor2);
		portype.add(rdpor3);
		optionspanel.add(rdpor1);
		optionspanel.add(rdpor2);
		optionspanel.add(rdpor3);
		ButtonGroup image = new ButtonGroup();
		rdimgon = new JRadioButton("All Skill Images");
		rdimgoff = new JRadioButton("No Skill Images ");
		rdimgexact = new JRadioButton("Exact Matches Only");
		image.add(rdimgon);
		image.add(rdimgoff);
		image.add(rdimgexact);
		optionspanel.add(rdimgon);
		optionspanel.add(rdimgoff);
		optionspanel.add(rdimgexact);
		JLabel lblstsimg = new JLabel("Status Effects on Sprites");
		ButtonGroup stsimg = new ButtonGroup();
		rdstsimgon = new JRadioButton("On");
		rdstsimgoff = new JRadioButton("Off");
		stsimg.add(rdstsimgon);
		stsimg.add(rdstsimgoff);
		optionspanel.add(lblstsimg);
		optionspanel.add(rdstsimgon);
		optionspanel.add(rdstsimgoff);
		JLabel lblfnt = new JLabel("Font Size");
		ButtonGroup size = new ButtonGroup();
		rdfntnorm = new JRadioButton("Normal");
		rdnfntlrg = new JRadioButton("Large");
		size.add(rdfntnorm);
		size.add(rdnfntlrg);
		optionspanel.add(lblfnt);
		optionspanel.add(rdfntnorm);
		optionspanel.add(rdnfntlrg);
		JLabel lblrpt = new JLabel("Combat Reports");
		ButtonGroup reports = new ButtonGroup();
		rdrpt1 = new JRadioButton("On");
		rdrpt2 = new JRadioButton("Off");
		reports.add(rdrpt1);
		reports.add(rdrpt2);
		optionspanel.add(lblrpt);
		optionspanel.add(rdrpt1);
		optionspanel.add(rdrpt2);
		JLabel lblcol = new JLabel("Color Set");
		ButtonGroup colors = new ButtonGroup();
		rdcol1 = new JRadioButton("Default");
		rdcol2 = new JRadioButton("Dark");
		colors.add(rdcol1);
		colors.add(rdcol2);
		optionspanel.add(lblcol);
		optionspanel.add(rdcol1);
		optionspanel.add(rdcol2);
		mntmOptions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Global.checkFlag(Flag.hardmode)){
					rdhard.setSelected(true);
				}
				else{
					rdnormal.setSelected(true);
				}
				if(Global.checkFlag(Flag.autosave)){
					rdautosaveon.setSelected(true);
				}
				else{
					rdautosaveoff.setSelected(true);
				}
				if(Global.checkFlag(Flag.noportraits)){
					rdpor3.setSelected(true);
				}
				else if(Global.checkFlag(Flag.portraits2)){
					rdpor2.setSelected(true);
				}
				else{
					rdpor1.setSelected(true);
				}
				if(Global.checkFlag(Flag.noimage)){
					rdimgoff.setSelected(true);
				}
				else if(Global.checkFlag(Flag.exactimages)){
					rdimgexact.setSelected(true);
				}
				else{
					rdimgon.setSelected(true);
				}
				if(Global.checkFlag(Flag.statussprites)){
					rdstsimgon.setSelected(true);
				}else{
					rdstsimgoff.setSelected(true);
				}
				if(Global.checkFlag(Flag.largefonts)){
					rdnfntlrg.setSelected(true);
				}
				else{
					rdfntnorm.setSelected(true);
				}
				if(Global.checkFlag(Flag.noReports)){
					rdrpt2.setSelected(true);
				}else{
					rdrpt1.setSelected(true);
				}
				if(Global.checkFlag(Flag.altcolors)){
					rdcol2.setSelected(true);
				}else{
					rdcol1.setSelected(true);
				}
				int result = JOptionPane.showConfirmDialog(GUI.this,optionspanel,"Options",JOptionPane.OK_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE);
				if(result==JOptionPane.OK_OPTION){
					if(rdnormal.isSelected()){
						Global.unflag(Flag.hardmode);
					}
					else{
						Global.flag(Flag.hardmode);
					}
					if(rdautosaveoff.isSelected()){
						Global.unflag(Flag.autosave);
					}
					else{
						Global.flag(Flag.autosave);
					}
					if(rdpor2.isSelected()){
						Global.flag(Flag.portraits2);
						Global.unflag(Flag.noportraits);
					}else if(rdpor3.isSelected()){
						Global.flag(Flag.noportraits);
						Global.unflag(Flag.portraits2);
						showNone();
					}
					else{
						Global.unflag(Flag.portraits2);
						Global.unflag(Flag.noportraits);
					}
					if(rdimgon.isSelected()){
						Global.unflag(Flag.noimage);
						Global.unflag(Flag.exactimages);
					}
					else if(rdimgexact.isSelected()){
						Global.unflag(Flag.noimage);
						Global.flag(Flag.exactimages);
					}
					else{
						Global.flag(Flag.noimage);
						Global.unflag(Flag.exactimages);
					}
					if(rdstsimgon.isSelected()){
						Global.flag(Flag.statussprites);
					}else{
						Global.unflag(Flag.statussprites);
					}
					if(rdnfntlrg.isSelected()){
						Global.flag(Flag.largefonts);
						fontsize=7;
					}
					else{
						Global.unflag(Flag.largefonts);
						fontsize=5;
					}
					if(rdrpt2.isSelected()){
						Global.flag(Flag.noReports);
					}else{
						Global.unflag(Flag.noReports);
					}
					if(rdcol2.isSelected()){
						Global.flag(Flag.altcolors);
						textColor = Constants.ALTTEXTCOLOR;
						frameColor = Constants.ALTFRAMECOLOR;
						backgroundColor = Constants.ALTBGCOLOR;
						refreshColors();
					}else{
						Global.unflag(Flag.altcolors);
						textColor = Constants.PRIMARYTEXTCOLOR;
						frameColor = Constants.PRIMARYFRAMECOLOR;
						backgroundColor = Constants.PRIMARYBGCOLOR;
						refreshColors();
					}
				}
			}
		});
				
		// menu bar - credits
		
		JMenuItem mntmCredits = new JMenuItem("Credits");
		mntmCredits.setForeground(textColor);
		mntmCredits.setBackground(backgroundColor);
		menuBar.add(mntmCredits);
		
		// menu bar - supporters
		JMenuItem mntmPatrons = new JMenuItem("Supporters");
		mntmPatrons.setForeground(textColor);
		mntmPatrons.setBackground(backgroundColor);
		menuBar.add(mntmPatrons);
		
		// menu bar - quit match
		mntmQuitMatch = new JMenuItem("Quit Match");
		mntmQuitMatch.setEnabled(false);
		mntmQuitMatch.setForeground(textColor);
		mntmQuitMatch.setBackground(backgroundColor);
		mntmQuitMatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int result = JOptionPane.showConfirmDialog(GUI.this,"Do you want to quit for the night? Your opponents will continue to fight and gain exp.","Retire early?",JOptionPane.OK_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE);
				if(result==JOptionPane.OK_OPTION){
					Global.getMatch().quit();
				}
			}
		});
		menuBar.add(mntmQuitMatch);
		mntmCredits.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane
						.showMessageDialog(GUI.this,
								"Night Games created by The Silver Bard\n" +
								"Reyka and Samantha created by dndw\n" +
								"Upgraded Strapon created by ElfBoyEni\n" +
								"Strapon victory scenes created by Legion\n" +
								"Hardmode AI by Jos\n"
								+ "Character Sprites by AimlessArt\n" +
								"Character portraits by Nergantre\n" +
								"Magic Training scenes by Legion\n"
								+ "Ki Water Form Training scene by KVernik\n" +
								"Jewel 2nd Victory scene by Legion\n" +
								"Combat Art by Fujin Hitokiri, AimlessArt, Sky Relyks, and Phanaxial\n" +
								"Video Games scenes 1-9 by Onyxdime\n" +
								"Kat Penetration Victory and Defeat scenes by Onyxdime\n" +
								"Kat Non-Penetration Draw scene by Onyxdime\n" +
								"Mara/Angel threesome scene by Onyxdime\n"
								+ "Eve Penetration Defeat scene by Onyxdime\n"
								+ "Improved UI by InvalidCharacter\n"
								+ "Dynamic comment framework by dndw\n"
								+ "Minimap by Nergantre and InvalidCharacter\n"
								+ "Reorganized UI by GruntledDev\n"
								+ "Mara 2nd Penetration victory scene by GruntledDev\n"
								+ "Samantha Pegging and Draw scenes by Jotor\n"
								+ "Mara 3rd Draw scene by Sakruff\n"
								+ "Rin Training scenes by KajunKrust\n"
								+ "Eve Intercourse draw scene by Marcus Koen\n"
								+ "Yui Daytime sex scene by CK\n"
								+ "Yui Intercourse defeat scene by Rex\n"
								+ "Cassie Night off scene by Vica\n"
								+ "Workshop Training Shrink Ray scene by DJWriter");
			}
		});
		mntmPatrons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane
						.showMessageDialog(GUI.this,
								"Sevrin Alain, Ken Arthur, Joe Barrass, Jonas Bergmeister, Jeremy Birch, Rob Brance, Dylan Bye,\n"
								+ "Amiri Capers, Derek Cheong, Josh Corrigan, Andrei Dergalin, Derp Derpersson, Dylan Devenny, \n"
								+ "Nicholas Diamante, Ramon Diaz, Sapph1re Dra90n, Lazarus Draken, Kevin Eassom, John Echols, \n"
								+ "Mr. Face, Roger Faux, Nick Finley, Rick Fox, Sniper Fox, Bob Fruman, Patrick Graham, Jacob Grindstaff, \n"
								+ "Julian Groot, Some Guy, Clap Hands, Patrick Hastings, Evan Haukenfrers, Matt Hicks, Kan Ho, Tom Hourie, \n"
								+ "timothy huff, S Hunter, Matvei Ianovich, Aidan Isner, Brad J, Jason Janovits, Mr. Jester, \n"
								+ "Rasmus Johnsson, Zachary Katis, Yi Ko, Magnus Kuerner, Emil Larsen, Adam Larson, Warwick Lawson, \n"
								+ "Matt Lee, Daniel Lewin�ski, Sherwood Lin, Steven Lindsey, Florid Lunacy, Victor Magnus, Hans mann, \n"
								+ "Dave McCl, Tom Meyers, Brent Miller, Daniel Moran, Alex Moreno, Max Mustermann-34615, C. N., \n"
								+ "Sakio Nagaoka, Yuri Nakamura, fictional name, Christopher Nicdao, Kyle O'Neale, Jeffrey Pack, \n"
								+ "Jasraj Panesar, Dustin Parrish, Tai Parry, Nick Pazer, Christopher Power, Rakun Procyon, Anomander Rake, \n"
								+ "Benjamin Reinhart, Youshou Rhea, Max Richter, Jeffrey Robertson, Jahkari Robinson, Michael Ryan, \n"
								+ "David Rybicki, t s, Abdullah Saba, Jakub Sachmacin�ski, Jason Sacrison, Timothy Scharf, Ron Selch, \n"
								+ "Carl Shelton, Brian Sheppard, Tentacle Shogun, Paul Silaghi, Joshua Smith, John Smith, Captain Snark, \n"
								+ "Jonathan Spence, Patrick Spencer, Tim Stevens, Patrick Strickland, Christopher Tennant, Jonathan Townsend, \n"
								+ "Gia Trang, Brandon U, Timothy VanSciver, Necro Vee, Josh white, Kasper Wind, Jacob Woods, \n"
								+ "SomebodyLookingToHelp, Kochi, Laioken, K, FranklyStein, Xike, Banana, Kazuki, Pugmeyer, wtiptop, Thomas, \n"
								+ "Kaine, Bobstick5, Israfel, SHADOWFOX, 0, GuardianSoul, Necessarily, Zxc2000, EWasabi316, Steve, Thror, \n"
								+ "Lehamme, JackNTheBox, nf, Jerry, zb123, Matt, Baval, Horizon, Faiez, dumbname, Zivich, Rewind, sanatloc, \n"
								+ "Fuzzylogick, Syoh, Drew, lennysaver, Den, Chit, enemygunman, Kevin, TheTerror, NA, wormroom, Cameron, \n"
								+ "Leenix, stephan, griege, Eric, Purpleash, arctic, Magnus, willows_puppy, Afrohawk, DragonPiggy, EzarTek, \n"
								+ "Larius, Deathevn, Carl, Flunkmaster, Jekiam, Azione, Racecar, kemious, Atacama, Joshua, Stealth, D.M., \n"
								+ "Mike, Oldcityguy, Fandley, Kevin, Yuan, riceckazop, abomasal, Kelson, DibbyDabba, SomeGuy, Duality, \n"
								+ "FF, Somedude, Arcadion, LiPo, Grey, hitman5, Najal, hephasteus, Zeke, metroidreference, BadManWalking, \n"
								+ "Alias, cs, alreadydead, Ryan, CaldariSteel, Flintfire, Gwaxer, 64bitrobot, RHR, Ker, Soverigndreams, Joe, ");
			}
		});
		
		// panel layouts

		// mainpanel - everything is contained within it

		this.mainpanel = new JPanel();
		this.mainpanel.setBackground(backgroundColor);
		getContentPane().add(this.mainpanel);
		this.mainpanel.setLayout(new BoxLayout(this.mainpanel, 1));
		
		// panel0 - invisible, only handles topPanel
		this.panel0 = new Panel();
		this.mainpanel.add(this.panel0);
		this.panel0.setLayout(new BoxLayout(this.panel0, 0));

		// topPanel - invisible, menus
		this.topPanel = new JPanel();
		this.panel0.add(this.topPanel);
		this.topPanel.setLayout(new BoxLayout(this.topPanel,BoxLayout.X_AXIS));

		// centerPanel - invisible, body of GUI

		this.centerPanel = new JPanel();
		this.mainpanel.add(this.centerPanel);
		this.centerPanel.setLayout(new BorderLayout(0, 0));

		// inventoryPanel - visible, items and clothing
		this.inventoryPanel = new JPanel();
		this.inventoryPanel.setLayout(new BorderLayout(0,0));
		this.inventoryPanel.setBackground(frameColor);
		this.inventoryPanel.setMaximumSize(new Dimension(100,height));

		// statusPanel - visible, character status
		this.statusPanel = new JPanel();
		this.statusPanel.setLayout(new BoxLayout(this.statusPanel, BoxLayout.X_AXIS));
		this.statusPanel.setBackground(frameColor);

		//clothesPanel - visible, contains clothing diagram
		this.clothesPanel = new JPanel();
			

		// portraitPanel
		portraitPanel = new JPanel();
		centerPanel.add(portraitPanel, BorderLayout.WEST);
		portraitPanel.setLayout(new CardLayout());
		portraitPanel.setBackground(backgroundColor);
		portrait = new JLabel("");
		portrait.setVerticalAlignment(SwingConstants.TOP);
		sprite = new JLabel("");
		sprite.setVerticalAlignment(SwingConstants.BOTTOM);
		portraitPanel.add(portrait, USE_PORTRAIT);
		portraitPanel.add(sprite, USE_SPRITE);
		
        map = new MapComponent();
        portraitPanel.add(map, USE_MAP);
        JLabel logo = new JLabel("");
        portraitPanel.add(logo, USE_NONE);
        BufferedImage logoimg = null;
		try {
			logoimg = ImageIO.read(getClass().getResource(
					"assets/logo.png"));
		}catch (IOException localIOException9) {
		}
		if(logoimg!=null){
			logo.setIcon(new ImageIcon(logoimg));
		}
        map.setPreferredSize(new Dimension(300, 385));
		
        midPanel = new JPanel();
        midPanel.setLayout(new CardLayout());
        centerPanel.add(midPanel, BorderLayout.CENTER);
        
		// imgPanel - visible, contains imgLabel
		imgPanel = new JLabel();
		imgPanel.setLayout(new BorderLayout(2, 2));
		imgPanel.setBackground(backgroundColor);
		
		// textScroll
		this.textScroll = new JScrollPane();
		textScroll.setOpaque(false);
//		imgPanel.add(textScroll, BorderLayout.CENTER);		
		midPanel.add(textScroll, USE_MAIN_TEXT_UI);		
		
		// textPane
		this.textPane = new JTextPane();
		DefaultCaret caret = (DefaultCaret) textPane.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.textPane.setForeground(textColor);
		this.textPane.setBackground(backgroundColor);
		this.textPane.setPreferredSize(new Dimension(width, 400));
		this.textPane.setEditable(false);
		this.textPane.setContentType("text/html");
		bodytext = "";
		this.textScroll.setViewportView(this.textPane);


		fontsize=8;
					
		JButton debug = new JButton("Debug");
		debug.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Global.getMatch().resume();
			}
		});
		
		// commandPanel - visible, contains the player's command buttons
		groupBox = Box.createHorizontalBox();
        groupBox.setBackground(frameColor);
        groupBox.setBorder(new CompoundBorder());
        groupPanel = new JPanel();
        mainpanel.add(groupPanel);

        commandPanel = new CommandPanel(width, height);
        groupPanel.add(groupBox);
        groupPanel.add(commandPanel);
        commandPanel.setBackground(frameColor);

        groupPanel.setBackground(frameColor);
        groupPanel.setBorder(new CompoundBorder());
		skills = new HashMap<TacticGroup,ArrayList<SkillButton>>();
        currentTactics = TacticGroup.All;        
		flasks = new ArrayList<SkillButton>();
		potions = new ArrayList<SkillButton>();
		demands = new ArrayList<SkillButton>();
        clearCommand();
		createCharacter();
		setVisible(true);
		pack();
		
		JPanel panel = (JPanel) getContentPane();
        panel.setFocusable(true);
        panel.addKeyListener(new KeyListener() {
            /**
             * Space bar will select the first option, unless they are in the default actions list.
             */
            @Override
            public void keyReleased(KeyEvent e) {
                Optional<KeyableButton> buttonOptional = commandPanel.getButtonForHotkey(e.getKeyChar());
                if (buttonOptional.isPresent()) {
                    buttonOptional.get().call();
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {}

            @Override
            public void keyPressed(KeyEvent e) {}
        });

	}

    public void showMap() {
        map.setPreferredSize(new Dimension(300, 385));
        CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
        portraitLayout.show(portraitPanel, USE_MAP);
        portraitPanel.repaint();
        portraitPanel.revalidate();
//		pack();
    }
    public void showPortrait() {
        CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
        if(Global.checkFlag(Flag.portraits2)){
        	portrait.setPreferredSize(new Dimension(300, 385));
        	portraitLayout.show(portraitPanel, USE_PORTRAIT);
        }else{
        	sprite.setPreferredSize(new Dimension(300, 385));
        	portraitLayout.show(portraitPanel, USE_SPRITE);
        }
        portraitPanel.repaint();
        portraitPanel.revalidate();
//		pack();
    }
    
    public void showNone() {
        CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
        portraitLayout.show(portraitPanel, USE_NONE);
        midPanel.repaint();
        midPanel.revalidate();
//		pack();
    }

	// image loader
	public void displayImage(String path,String artist){
		if(Global.checkFlag(Flag.noimage)){
			return;
		}
		URL imgsrc = getClass().getResource(
				"assets/"+path);

		if(imgsrc!=null){
			message(String.format("<img src=\"%s\" title=\"%s\"/>",imgsrc.toString(),artist));
		}
	}

	public void resetPortrait() {
		portrait.setIcon(null);
	}


	// portrait loader
	public void loadPortrait(Character player, Character enemy) {
		if(!Global.checkFlag(Flag.noportraits)){
			String imagepath = null;
			BufferedImage spriteImg = null;
			if(!player.human()){
				imagepath = player.getPortrait();
				spriteImg = player.getSpriteImage();
			}
			else if(!enemy.human()){
				imagepath = enemy.getPortrait();
				spriteImg = enemy.getSpriteImage();
			}
			sprite.setIcon(null);
			if(imagepath!=null && Global.checkFlag(Flag.portraits2)){
				BufferedImage face = null;
				try {
					URL imgurl = getClass().getResource(imagepath);
					if(imgurl != null){
						face = ImageIO.read(imgurl);
					}
				}catch (IOException localIOException9 ) {
				}
				if(face!=null){
					portrait.setIcon(null);
					portrait.setIcon(new ImageIcon(face));
					CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
					portraitLayout.show(portraitPanel, USE_PORTRAIT);
				}
			}else if(spriteImg != null && !Global.checkFlag(Flag.portraits2)){
				sprite.setIcon(new ImageIcon(spriteImg));
				CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
				portraitLayout.show(portraitPanel, USE_SPRITE);
			}else if(imagepath!=null){
				BufferedImage face = null;
				try {
					URL imgurl = getClass().getResource(imagepath);
					if(imgurl != null){
						face = ImageIO.read(imgurl);
					}
				}catch (IOException localIOException9) {
				}
				if(face!=null){
					portrait.setIcon(null);
					portrait.setIcon(new ImageIcon(face));
					CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
					portraitLayout.show(portraitPanel, USE_PORTRAIT);
				}
			}
//			this.centerPanel.revalidate();
//			this.portraitPanel.revalidate();
			this.portraitPanel.repaint();
		}
	}
	public void loadTwoPortraits(Character first, Character second){
		if(!Global.checkFlag(Flag.noportraits)){
			String imagepath = null;
			BufferedImage spriteImg = null;
			String imagepath2 = null;
			BufferedImage spriteImg2 = null;
			BufferedImage sprites = null;
			if(!first.human()){
				imagepath = first.getPortrait();
				spriteImg = first.getSpriteImage();
			}
			if(!second.human()){
				imagepath2 = second.getPortrait();
				spriteImg2 = second.getSpriteImage();
			}
			sprite.setIcon(null);
			if((spriteImg!=null || spriteImg2!=null)&& !Global.checkFlag(Flag.portraits2)){
				if(spriteImg!=null && spriteImg2!=null){
					sprites = new BufferedImage(spriteImg.getWidth(), spriteImg.getHeight(), spriteImg.getType());
					Graphics2D g = sprites.createGraphics();
					g.drawImage(spriteImg, -60, 0,null);
					g.drawImage(spriteImg2, 60, 0,null);
				}else if(spriteImg!=null){
					sprites = spriteImg;
				}else{
					sprites = spriteImg2;
				}						
				sprite.setIcon(new ImageIcon(sprites));
				CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
				portraitLayout.show(portraitPanel, USE_SPRITE);
			}else if(imagepath!=null || imagepath2!=null){
				BufferedImage face = null;
				BufferedImage face2 = null;
				BufferedImage faces = null;
				int height=0;
				int width = 0;
				if(imagepath!=null){
					try {
						URL imgurl = getClass().getResource(imagepath);
						if(imgurl != null){
							face = ImageIO.read(imgurl);
						}
					}catch (IOException localIOException9) {
					}
					if(face!=null){
						height+=face.getHeight();
						width = face.getWidth();
					}
				}
				if(imagepath2!=null){
					try{
						URL imgurl2 = getClass().getResource(imagepath2);
						if(imgurl2 != null){
							face2 = ImageIO.read(imgurl2);
						}
					}catch (IOException localIOException9) {
					}
					if(face2!=null){
						height+=face2.getHeight();
						width = Math.max(face2.getWidth(),width);
					}
				}
				if(face!=null && face2!=null){
					faces = new BufferedImage(width, height, face.getType());
					Graphics2D g = faces.createGraphics();		
					g.drawImage(face, 0, 0,null);
					g.drawImage(face2, 0, face.getHeight(),null);												
				}
				else if(face!=null){
					faces = face;
				}
				else if(face2!=null){
					faces = face2;
				}
				if(faces!=null){
					portrait.setIcon(null);
					portrait.setIcon(new ImageIcon(faces));
					CardLayout portraitLayout = (CardLayout) (portraitPanel.getLayout());
					portraitLayout.show(portraitPanel, USE_PORTRAIT);
				}
			}
//			this.centerPanel.revalidate();
//			this.portraitPanel.revalidate();
			this.portraitPanel.repaint();
		}
	}
	
	public BufferedImage loadStatusFilters(Character NPC){
		BufferedImage filter = new BufferedImage(350, 540, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = filter.createGraphics();
		ArrayList<String> statuses = NPC.listStatus();
		if(statuses.contains("Feral")){
			if(FeralFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Feral_Status.png");
					if(imgurl!=null){
						FeralFilter = ImageIO.read(imgurl);
					}
					g.drawImage(FeralFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(FeralFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Dissolving")){
			if(DissolvingFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Dissolving_Status.png");
					if(imgurl!=null){
						DissolvingFilter = ImageIO.read(imgurl);
					}
					g.drawImage(DissolvingFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(DissolvingFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Oiled")){
			if(OiledFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Oiled_Status.png");
					if(imgurl!=null){
						OiledFilter = ImageIO.read(imgurl);
					}
					g.drawImage(OiledFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(OiledFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Drowsy")){
			if(DrowsyFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Drowsy_Status.png");
					if(imgurl!=null){
						DrowsyFilter = ImageIO.read(imgurl);
					}
					g.drawImage(DrowsyFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(DrowsyFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Caffeinated")){
			if(CaffeineFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Caffeine_Status.png");
					if(imgurl!=null){
						CaffeineFilter = ImageIO.read(imgurl);
					}
					g.drawImage(CaffeineFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(CaffeineFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Buzzed")){
			if(BuzzedFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Buzzed_Status.png");
					if(imgurl!=null){
						BuzzedFilter = ImageIO.read(imgurl);
					}
					g.drawImage(BuzzedFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(BuzzedFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Bondage")){
			if(BondageFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Bondage_Status.png");
					if(imgurl!=null){
						BondageFilter = ImageIO.read(imgurl);
					}
					g.drawImage(BondageFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(BondageFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Masochism")){
			if(MasochismFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Masochism_Status.png");
					if(imgurl!=null){
						MasochismFilter = ImageIO.read(imgurl);
					}
					g.drawImage(MasochismFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(MasochismFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Shamed")){
			if(ShamedFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Shamed_Status.png");
					if(imgurl!=null){
						ShamedFilter = ImageIO.read(imgurl);
					}
					g.drawImage(ShamedFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(ShamedFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Charmed")){
			if(CharmedFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Charmed_Status.png");
					if(imgurl!=null){
						CharmedFilter = ImageIO.read(imgurl);
					}
					g.drawImage(CharmedFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(CharmedFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Enthralled")){
			if(EnthralledFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Enthralled_Status.png");
					if(imgurl!=null){
						EnthralledFilter = ImageIO.read(imgurl);
					}
					g.drawImage(EnthralledFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(EnthralledFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Hypersensitive")){
			if(HypersensitiveFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Hypersensitive_Status.png");
					if(imgurl!=null){
						HypersensitiveFilter = ImageIO.read(imgurl);
					}
					g.drawImage(HypersensitiveFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(HypersensitiveFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Shield")){
			if(BarrierFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Barrier_Status.png");
					if(imgurl!=null){
						BarrierFilter = ImageIO.read(imgurl);
					}
					g.drawImage(BarrierFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(BarrierFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Braced")){
			if(BracedFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Braced_Status.png");
					if(imgurl!=null){
						BracedFilter = ImageIO.read(imgurl);
					}
					g.drawImage(BracedFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(BracedFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Tied Up")){
			if(BoundFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Bound_Status.png");
					if(imgurl!=null){
						BoundFilter = ImageIO.read(imgurl);
					}
					g.drawImage(BoundFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(BoundFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Flat-Footed")){
			if(DistractedFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Distracted_Status.png");
					if(imgurl!=null){
						DistractedFilter = ImageIO.read(imgurl);
					}
					g.drawImage(DistractedFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(DistractedFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Winded")){
			if(WindedFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Winded_Status.png");
					if(imgurl!=null){
						WindedFilter = ImageIO.read(imgurl);
					}
					g.drawImage(WindedFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(WindedFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Horny")){
			if(HornyFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/Horny_Status.png");
					if(imgurl!=null){
						HornyFilter = ImageIO.read(imgurl);
					}
					g.drawImage(HornyFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(HornyFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Stone Form")){
			if(StoneformFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/StoneForm_Status.png");
					if(imgurl!=null){
						StoneformFilter = ImageIO.read(imgurl);
					}
					g.drawImage(StoneformFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(StoneformFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Water Form")){
			if(WaterformFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/WaterForm_Status.png");
					if(imgurl!=null){
						WaterformFilter = ImageIO.read(imgurl);
					}
					g.drawImage(WaterformFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(WaterformFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Ice Form")){
			if(IceformFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/IceForm_Status.png");
					if(imgurl!=null){
						IceformFilter = ImageIO.read(imgurl);
					}
					g.drawImage(IceformFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(IceformFilter, 0, 0, null);
			}
		}
		if(statuses.contains("Fire Form")){
			if(FireformFilter == null){
				try {
					URL imgurl = Global.gui().getClass().getResource(
							"assets/FireForm_Status.png");
					if(imgurl!=null){
						FireformFilter = ImageIO.read(imgurl);
					}
					g.drawImage(FireformFilter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(FireformFilter, 0, 0, null);
			}
		}
		return filter;
	}
	
	// combat GUI
	public Combat beginCombat(Character player, Character enemy) {
		this.combat = new Combat(player, enemy, player.location());
		this.combat.addObserver(this);
		resetPortrait();
		if(!Global.checkFlag(Flag.noportraits)){
			loadPortrait(player,enemy);
			showPortrait();
		}
		return this.combat;
		
	}
	
	//Combat GUI
	public Combat beginCombat(Character player, Character enemy, int code) {
		this.combat = new Combat(player, enemy, player.location(), code);
		this.combat.addObserver(this);
		resetPortrait();
		message(this.combat.getMessage());
		if(!Global.checkFlag(Flag.noportraits)){
			loadPortrait(player,enemy);
			showPortrait();
		}
		return this.combat;
	}

	public void watchCombat(Combat c) {
		this.combat = c;
		this.combat.addObserver(this);
	}
	// getLabelString - handles all the meters (bars)

	public String getLabelString(Meter meter) {
		return Integer.toString(meter.get()) + "/" + meter.max();
	}
	public void populatePlayer(Player player) {
		if(Global.checkFlag(Flag.largefonts)){
			fontsize=8;
		}
		else{
			fontsize=6;
		}
		if(Global.checkFlag(Flag.altcolors)){
			textColor = Constants.ALTTEXTCOLOR;
			frameColor = Constants.ALTFRAMECOLOR;
			backgroundColor = Constants.ALTBGCOLOR;
		}
		getContentPane().remove(this.creation);
		getContentPane().add(this.mainpanel);
		getContentPane().validate();
		this.player = player;
		player.gui = this;
		player.addObserver(this);
		JPanel meter = new JPanel();
		meter.setBackground(frameColor);
		this.topPanel.add(meter);
		meter.setLayout(new GridLayout(0, 3, 0, 0));

		this.stamina = new JLabel("Stamina: " + getLabelString(player.getStamina()));
		this.stamina.setFont(new Font("Georgia", 1, 15));
		this.stamina.setHorizontalAlignment(0);
		this.stamina.setForeground(new Color(200, 14, 12));
		this.stamina
				.setToolTipText("Stamina represents your endurance and ability to keep fighting. If it drops to zero, you'll be temporarily stunned.");
		meter.add(this.stamina);
		this.arousal = new JLabel("Arousal: " + getLabelString(player.getArousal()));
		this.arousal.setFont(new Font("Georgia", 1, 15));
		this.arousal.setHorizontalAlignment(0);
		this.arousal.setForeground(new Color(254, 1, 107));
		this.arousal
				.setToolTipText("Arousal is raised when your opponent pleasures or seduces you. If it hits your max, you'll orgasm and lose the fight.");
		meter.add(this.arousal);
		this.mojo = new JLabel("Mojo: " + getLabelString(player.getMojo()));
		this.mojo.setFont(new Font("Georgia", 1, 15));
		this.mojo.setHorizontalAlignment(0);
		this.mojo.setForeground(new Color(51, 153, 255));
		this.mojo
				.setToolTipText("Mojo is the abstract representation of your momentum and style. It increases with normal techniques and is used to power special moves");
		meter.add(this.mojo);

		this.staminaBar = new JProgressBar();
		UIDefaults staminaOver = new UIDefaults();
		staminaOver.putAll(UIManager.getLookAndFeelDefaults());
		staminaOver.put("ProgressBar[Enabled+Finished].foregroundPainter", new MyPainter(new Color(200, 14, 12,180)));
		staminaOver.put("ProgressBar[Enabled].foregroundPainter", new MyPainter(new Color(200, 14, 12,180))); 
		staminaOver.put("ProgressBar[Enabled+Indeterminate].foregroundPainter", new MyPainter(new Color(200, 14, 12,180)));
		this.staminaBar.putClientProperty("Nimbus.Overrides", staminaOver);
		this.staminaBar
				.setBorder(new SoftBevelBorder(1, null, null, null, null));
		meter.add(this.staminaBar);
		this.staminaBar.setMaximum(player.getStamina().max());
		this.staminaBar.setValue(player.getStamina().get());

		this.arousalBar = new JProgressBar();
		this.arousalBar
				.setBorder(new SoftBevelBorder(1, null, null, null, null));
		this.arousalBar.setForeground(new Color(254, 1, 107));
		UIDefaults arousalOver = new UIDefaults();
		arousalOver.putAll(UIManager.getLookAndFeelDefaults());
		arousalOver.put("ProgressBar[Enabled+Finished].foregroundPainter", new MyPainter(new Color(254, 1, 107, 180)));
		arousalOver.put("ProgressBar[Enabled].foregroundPainter", new MyPainter(new Color(254, 1, 107, 180))); 
		arousalOver.put("ProgressBar[Enabled+Indeterminate].foregroundPainter", new MyPainter(new Color(254, 1, 107, 180)));
		this.arousalBar.putClientProperty("Nimbus.Overrides", arousalOver);
		meter.add(this.arousalBar);
		this.arousalBar.setMaximum(player.getArousal().max());
		this.arousalBar.setValue(player.getArousal().get());

		this.mojoBar = new JProgressBar();
		this.mojoBar.setBorder(new SoftBevelBorder(1, null, null, null, null));
		this.mojoBar.setForeground(new Color(51, 153, 255));
		this.mojoBar.setBackground(frameColor);
		UIDefaults mojoOver = new UIDefaults();
		mojoOver.putAll(UIManager.getLookAndFeelDefaults());
		mojoOver.put("ProgressBar[Enabled+Finished].foregroundPainter", new MyPainter(new Color(51, 153, 255, 180)));
		mojoOver.put("ProgressBar[Enabled].foregroundPainter", new MyPainter(new Color(51, 153, 255, 180))); 
		mojoOver.put("ProgressBar[Enabled+Indeterminate].foregroundPainter", new MyPainter(new Color(51, 153, 255, 180)));
		this.mojoBar.putClientProperty("Nimbus.Overrides", mojoOver);
		meter.add(this.mojoBar);
		this.mojoBar.setMaximum(player.getMojo().max());
		this.mojoBar.setValue(player.getMojo().get());

		JPanel bio = new JPanel();
		this.topPanel.add(bio);
		bio.setLayout(new GridLayout(2, 0, 0, 0));
		bio.setBackground(frameColor);

		JLabel name = new JLabel(player.name());
		name.setHorizontalAlignment(2);
		name.setFont(new Font("Georgia", 1, 15));
		name.setForeground(textColor);
//		bio.add(name);
		this.lvl = new JLabel("Lvl: " + player.getLevel());
		this.lvl.setFont(new Font("Georgia", 1, 15));
		this.lvl.setForeground(textColor);

//		bio.add(this.lvl);
		this.xp = new JLabel("XP: " + player.getXP());
		this.xp.setFont(new Font("Georgia", 1, 15));
		this.xp.setForeground(textColor);
//		bio.add(this.xp);

		this.timelbl = new JLabel();
		this.timelbl.setFont(new Font("Georgia", 1, 16));
		this.timelbl.setForeground(textColor);
		bio.add(this.timelbl);

		UIManager.put("ToggleButton.select", new Color(75, 88, 102));
		this.stsbtn = new JToggleButton("Status");
		stsbtn.setBackground(frameColor);
		stsbtn.setForeground(textColor);

		this.stsbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (GUI.this.stsbtn.isSelected()) {
					GUI.this.centerPanel.remove(GUI.this.portraitPanel);
					GUI.this.centerPanel.add(GUI.this.statusPanel, BorderLayout.WEST);
				} else {
					GUI.this.centerPanel.remove(GUI.this.statusPanel);
					GUI.this.centerPanel.add(GUI.this.portraitPanel, BorderLayout.WEST);
				}
				GUI.this.refresh();
				GUI.this.centerPanel.repaint();
				//GUI.this.centerPanel.validate();
			}
		});
		bio.add(this.stsbtn);		
		this.loclbl = new JLabel();
		this.loclbl.setFont(new Font("Georgia", 1, 16));
		this.loclbl.setForeground(textColor);
//		bio.add(this.loclbl);
		
		this.cashlbl = new JLabel();
		this.cashlbl.setFont(new Font("Georgia", 1, 16));
		this.cashlbl.setForeground(textColor);
		bio.add(this.cashlbl);
		this.invbtn = new JToggleButton("Inventory");
		invbtn.setBackground(frameColor);
		invbtn.setForeground(textColor);
		this.invbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (GUI.this.invbtn.isSelected()) {
					GUI.this.centerPanel.add(GUI.this.inventoryPanel, "East");
				} else {
					GUI.this.centerPanel.remove(GUI.this.inventoryPanel);
				}
				GUI.this.refresh();
				//centerPanel.validate();
			}
		});
		bio.add(this.invbtn);
		closet = new ClothesChangeGUI(player);
		midPanel.add(closet,USE_CLOSET_UI);
		showNone();
		enableOptions();
		this.commandPanel.setBackground(frameColor);
        this.groupPanel.setBackground(frameColor);
		portraitPanel.setBackground(backgroundColor);
		imgPanel.setBackground(backgroundColor);
		this.mainpanel.setBackground(backgroundColor);
		this.topPanel.validate();
	}

	public void createCharacter() {
		getContentPane().remove(this.mainpanel);
		this.creation = new CreationGUI(this);
		getContentPane().add(this.creation);
		//getContentPane().validate();
	}

	public void purgePlayer() {
		getContentPane().remove(this.mainpanel);
		clearText();
		clearCommand();
		showNone();
		if(closet!=null){
			midPanel.remove(closet);
		}
		this.mntmQuitMatch.setEnabled(false);
		this.combat = null;
		this.topPanel.removeAll();
	}

	public void clearText() {
		bodytext = "";
		this.textPane.setText("");
	}

	public void message(String text) {
		if(text==null||text==""){
			return;
		}
		if (text.trim().length() == 0) {
			return;
		}
		HTMLDocument doc = (HTMLDocument)textPane.getDocument();
		HTMLEditorKit editorKit = (HTMLEditorKit)textPane.getEditorKit();
		try {
			bodytext = bodytext+text+"<br>";
			if(Global.checkFlag(Flag.altcolors)){
				editorKit.insertHTML(doc, doc.getLength(), "<font face='Georgia'><font color='black'><font size='"+fontsize+"'>"+text+"<br>", 0, 0, null);
			}
			else{
				editorKit.insertHTML(doc, doc.getLength(), "<font face='Georgia'><font color='black'><font size='"+fontsize+"'>"+text+"<br>", 0, 0, null);
			}
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			   public void run() { 
			       textScroll.getVerticalScrollBar().setValue(0);
			   }
			});
	}

	public void messageHead(String text) {
		if(text==null||text==""){
			return;
		}
		if (text.trim().length() == 0) {
			return;
		}
		String newtext = text+bodytext;
		clearText();
		message(newtext);
	}

	public void combatMessage(String text) {
		HTMLDocument doc = (HTMLDocument)textPane.getDocument();
		HTMLEditorKit editorKit = (HTMLEditorKit)textPane.getEditorKit();
		try {
			bodytext = bodytext+text+"<br>";
			if(Global.checkFlag(Flag.altcolors)){
				editorKit.insertHTML(doc, doc.getLength(), "<font face='Georgia'><font color='black'><font size='"+fontsize+"'>"+text+"<br>", 0, 0, null);
			}
			else{
				editorKit.insertHTML(doc, doc.getLength(), "<font face='Georgia'><font color='black'><font size='"+fontsize+"'>"+text+"<br>", 0, 0, null);
			}
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clearCommand() {
		commandPanel.reset();
		skills.clear();
		flasks.clear();
		potions.clear();
		demands.clear();
		for(TacticGroup tactic: TacticGroup.values()){
			skills.put(tactic, new ArrayList<SkillButton>());
		}
		groupBox.removeAll();
		this.commandPanel.refresh();
	}

	public void addFlasks(Skill action, Combat com) {
		skills.get(TacticGroup.Flask).add(new SkillButton(action,com));
	}
	public void addPotions(Skill action, Combat com) {
		skills.get(TacticGroup.Potion).add(new SkillButton(action,com));
	}
	public void addDemands(Skill action, Combat com){
		skills.get(TacticGroup.Demand).add(new SkillButton(action,com));
	}

	public void addSkill(Combat com, Skill action) {
        SkillButton btn = new SkillButton(action, com);
        skills.get(action.type().getGroup()).add(btn);
    }
    public void showSkills() {
        commandPanel.reset();
        int i = 1;
        for (TacticGroup group : TacticGroup.values()) {
            SwitchTacticsButton tacticsButton = new SwitchTacticsButton(group);
            commandPanel.register(java.lang.Character.forDigit(i % 10, 10), tacticsButton);
            groupBox.add(tacticsButton);
            groupBox.add(Box.createHorizontalStrut(4));
            i += 1;
        }
        ArrayList<SkillButton> flatList = new ArrayList<SkillButton>();
        for (TacticGroup group : TacticGroup.values()) {
        	Collections.sort(skills.get(group), new Comparator<SkillButton>() {
                @Override
                public int compare(SkillButton button1, SkillButton button2)
                {

                    return  button1.getText().compareTo(button2.getText());
                }
            });
            for(SkillButton b: skills.get(group)){
            	flatList.add(b);
            }
        }
        if (currentTactics == TacticGroup.All || flatList.size() <= 6 ) {
            for(SkillButton b : flatList){
            	addToCommandPanel(b);
            }
        } else {
            for (SkillButton button : skills.get(currentTactics)) {
                addToCommandPanel(button);
            }
        }
        Global.getMatch().pause();
        commandPanel.refresh();
    }

    private void addToCommandPanel(KeyableButton button) {
        commandPanel.add(button);
    }

	public void addAction(Action action, Character user) {
		this.commandPanel.add(new ActionButton(action, user));
		Global.getMatch().pause();
		this.commandPanel.refresh();
	}

	public void addActivity(Activity act) {
		if(act.tooltip().isEmpty()){
			this.commandPanel.add(new ActivityButton(act));
		}else{
			this.commandPanel.add(new ActivityButton(act,act.tooltip()));
		}
		this.commandPanel.refresh();
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void next(Combat combat) {
		refresh();
		clearCommand();
		this.commandPanel.add(new NextButton(combat));
		Global.getMatch().pause();
		this.commandPanel.refresh();
	}

	public void next(Activity event) {
		event.next();
		clearCommand();
		this.commandPanel.add(new EventButton(event, "Next"));
		this.commandPanel.refresh();
	}
	public void choose(String choice) {
		this.commandPanel.add(new SceneButton(choice));
		this.commandPanel.refresh();
	}
	public void choose(String choice, String description) {
		this.commandPanel.add(new SceneButton(choice,description));
		this.commandPanel.refresh();
	}
	public void choose(Activity event, String choice) {
		this.commandPanel.add(new EventButton(event, choice));
		this.commandPanel.refresh();
	}
	public void choose(Activity event, String choice, String description) {
		this.commandPanel.add(new EventButton(event, choice, description));
		this.commandPanel.refresh();
	}

	public void sale(Store shop, Item i) {
		this.commandPanel.add(new ItemButton(shop, i));
		this.commandPanel.refresh();
	}
	
	public void sale(Store shop, Clothing i) {
		this.commandPanel.add(new ItemButton(shop, i));
		this.commandPanel.refresh();
	}

	public void promptFF(Encounter enc, Character target) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Fight",enc, target, Encs.fight));
		this.commandPanel.add(new EncounterButton("Flee",enc, target, Encs.flee));
		if(player.has(Consumable.smoke)){
			this.commandPanel.add(new EncounterButton("Smoke Bomb",enc, target, Encs.smokebomb));
		}
		Global.getMatch().pause();
		this.commandPanel.refresh();
	}

	public void promptAmbush(Encounter enc, Character target) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Attack "+target.name(),enc, target, Encs.ambush));
		this.commandPanel.add(new EncounterButton("Wait",enc, target, Encs.wait));
		Global.getMatch().pause();
		this.commandPanel.refresh();
	}

	public void promptOpportunity(Encounter enc, Character target, Trap trap) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Attack "+target.name(),enc, target, Encs.capitalize, trap));
		this.commandPanel.add(new EncounterButton("Wait",enc, target, Encs.wait));
		Global.getMatch().pause();
		this.commandPanel.refresh();
	}

	public void promptShower(Encounter encounter, Character target) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Suprise Her",encounter, target, Encs.showerattack));
		if (target.hasTrophy(null)) {
			this.commandPanel.add(new EncounterButton("Steal Clothes",encounter, target, Encs.stealclothes));
		}
		if (this.player.has(Flask.Aphrodisiac)) {
			this.commandPanel.add(new EncounterButton("Use Aphrodisiac",encounter, target, Encs.aphrodisiactrick));
		}
		this.commandPanel.add(new EncounterButton("Do Nothing",encounter,target,Encs.wait));
		Global.getMatch().pause();
		this.commandPanel.refresh();
	}

	public void promptIntervene(Encounter enc, Character p1, Character p2) {
		clearCommand();
		this.commandPanel.add(new InterveneButton(enc, p1));
		this.commandPanel.add(new InterveneButton(enc, p2));
		Global.getMatch().pause();
		this.commandPanel.refresh();
	}

	public void prompt(String message, ArrayList<KeyableButton> choices){
		clearCommand();
		if(message!=""){
			clearText();
			message(message);
		}
		for(KeyableButton button:choices){
			this.commandPanel.add(button);
		}
		this.commandPanel.refresh();
	}
	
	public void ding() {
		if (this.player.attpoints > 0) {
			message(this.player.attpoints + " Attribute Points remain.\n");
			clearCommand();
			for (Attribute att : this.player.att.keySet()) {
				if (att != Attribute.Perception && att !=Attribute.Speed) {
					this.commandPanel.add(new AttributeButton(att));
				}
			}
			Global.getMatch().pause();
			this.commandPanel.refresh();
		}else if(player.countFeats()<player.getLevel()/4){ 
			message("You've earned a new perk. Select one below.");
			clearCommand();
			boolean available = false;
			for(Trait feat:Global.getFeats()){
				if(!player.has(feat)&&feat.req(player)){
					this.commandPanel.add(new FeatButton(feat));
					available = true;
				}
				this.commandPanel.refresh();
			}
			if(!available){
				clearCommand();
				Global.gainSkills(this.player);
				endCombat();
			}
		}else {
			clearCommand();
			Global.gainSkills(this.player);
			endCombat();
		}
	}

	public void endCombat() {
		this.combat = null;
		clearText();
		resetPortrait();
		showMap();
		this.centerPanel.revalidate();
		this.portraitPanel.revalidate();
		this.portraitPanel.repaint();	
		Global.getMatch().resume();
		
	}
	public void startMatch(){
		this.mntmQuitMatch.setEnabled(true);
		showMap();
	}
	public void endMatch() {
		clearCommand();
		Global.flag(Flag.night);
		this.mntmQuitMatch.setEnabled(false);	
		this.commandPanel.add(new SleepButton());
		this.commandPanel.add(new SaveButton());
		this.commandPanel.refresh();
	}

	public void refresh() {
		this.stamina.setText("Stamina: " + getLabelString(player.getStamina()));
		this.arousal.setText("Arousal: " + getLabelString(player.getArousal()));
		this.mojo.setText("Mojo: " + getLabelString(player.getMojo()));
		this.lvl.setText("Lvl: " + this.player.getLevel());
		this.xp.setText("XP: " + this.player.getXP());
		this.staminaBar.setMaximum(this.player.getStamina().max());
		this.staminaBar.setValue(this.player.getStamina().get());
		this.arousalBar.setMaximum(this.player.getArousal().max());
		this.arousalBar.setValue(this.player.getArousal().get());
		this.mojoBar.setMaximum(this.player.getMojo().max());
		this.mojoBar.setValue(this.player.getMojo().get());
		this.loclbl.setText(this.player.location().name);
		this.cashlbl.setText("$" + this.player.money);
		displayInventory();
		displayStatus();
		if (map != null) {
            map.repaint();
        }
		if (Global.getMatch() != null) {
			String day = "";
			if(Global.getMatch().afterMidnight()){
				day = Global.getDayString(Global.getDate()+1);
			}else{
				day = Global.getDayString(Global.getDate());
			}
			this.timelbl.setText(day + " " + Global.getMatch().getTime());
		}
		if (Global.getDay() != null) {
			this.timelbl.setText(Global.getDayString(Global.getDate())+" "+Global.getDay().displayTime());
		}	
	}
	public void refreshLite(){
		if (Global.getMatch() != null) {
			String day = "";
			if(Global.getMatch().afterMidnight()){
				day = Global.getDayString(Global.getDate()+1);
			}else{
				day = Global.getDayString(Global.getDate());
			}
			this.timelbl.setText(day + " " + Global.getMatch().getTime());
		}
		this.cashlbl.setText("$" + this.player.money);
	}
	
	public void refreshColors(){
		getContentPane().remove(this.mainpanel);
		this.topPanel.removeAll();
		populatePlayer(Global.getPlayer());
		displayInventory();
		displayStatus();
		this.inventoryPanel.setBackground(frameColor);
		this.statusPanel.setBackground(frameColor);
		this.commandPanel.setBackground(frameColor);
		this.textPane.setForeground(textColor);
		this.textPane.setBackground(backgroundColor);
	}

	public void displayInventory() {
		this.inventoryPanel.removeAll();
		this.inventoryPanel.repaint();
		JPanel itemPane = new JPanel();
		JScrollPane itemScroll = new JScrollPane();
		itemPane.setBackground(backgroundColor);
		itemPane.setLayout(new BoxLayout(itemPane,1));
		itemScroll.setViewportView(itemPane);

		JPanel clothingPane = new JPanel();
		clothingPane.setBackground(frameColor);
		inventoryPanel.add(itemScroll);
		inventoryPanel.add(clothingPane,BorderLayout.SOUTH);
		
		HashMap<Item, Integer> items = this.player.listinventory();
		int count = 0;
		ArrayList<JLabel> itmlbls = new ArrayList<JLabel>();
		for (Item i : items.keySet()) {
			itmlbls.add(count, new JLabel(i.getName() + ": " + items.get(i)));
			itmlbls.get(count).setForeground(textColor);
			itmlbls.get(count).setFont(new Font("Georgia", 1, 16));
			itmlbls.get(count).setToolTipText(i.getDesc());
			itemPane.add(itmlbls.get(count));
			count++;
		}
		BufferedImage clothesicon = null;
		if (this.player.top.isEmpty()) {
			if (this.player.bottom.isEmpty()) {
				try {
					URL clothesurl = getClass().getResource(
							"clothes0.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException6) {
				}
			} else if (this.player.bottom.peek().getType() ==ClothingType.BOTOUTER) {
				try {
					URL clothesurl = getClass().getResource(
							"clothes4.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException7) {
				}
			} else {
				try {
					URL clothesurl = getClass().getResource(
							"clothes2.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException8) {
				}
			}
		}else if(this.player.top.peek().getType()==ClothingType.TOPOUTER){
			if (this.player.bottom.isEmpty()) {
				try {
					URL clothesurl = getClass().getResource(
							"clothes6.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException9) {
				}
			} else if (this.player.bottom.peek().getType() ==ClothingType.BOTOUTER) {
				try {
					URL clothesurl = getClass().getResource(
							"clothes8.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException10) {
				}
			} else {
				try {
					URL clothesurl = getClass().getResource(
							"clothes7.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException11) {
				}
			}
		}
		else{
			if (this.player.bottom.isEmpty()) {
				try {
					clothesicon = ImageIO.read(getClass().getResource(
							"clothes1.png"));
				} catch (IOException localIOException9) {
				}
			} else if (this.player.bottom.peek().getType() ==ClothingType.BOTOUTER) {
				try {
					URL clothesurl = getClass().getResource(
							"clothes5.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException10) {
				}
			} else {
				try {
					URL clothesurl = getClass().getResource(
							"clothes3.png");
					if(clothesurl!=null){
						clothesicon = ImageIO.read(clothesurl);
					}
				} catch (IOException localIOException11) {
				}
			}
		}
		if (clothesicon != null) {
			if(Global.checkFlag(Flag.statussprites)){
				BufferedImage sts = loadStatusFilters(this.player);
				if(sts!=null){
					Image scaled = sts.getScaledInstance(clothesicon.getWidth(), clothesicon.getHeight(), BufferedImage.SCALE_FAST);
					clothesicon.createGraphics().drawImage(scaled,0,0,null);
				}
			}
			this.clothesdisplay = new JLabel(new ImageIcon(clothesicon));
			clothingPane.add(this.clothesdisplay);
		}
		this.centerPanel.revalidate();
		this.inventoryPanel.revalidate();
		this.inventoryPanel.repaint();
	}
	
	public void displayStatus(){
		this.statusPanel.removeAll();
		statusPanel.setMinimumSize(new Dimension(500, centerPanel.getHeight()));
		statusPanel.setPreferredSize(new Dimension(500, centerPanel.getHeight()));

		if (width < 720) {
			statusPanel.setMaximumSize(new Dimension(width / 3,height));
			System.out.println("STATUS PANEL");
		}
		
		JPanel statsLeft = new JPanel();
		JPanel statsRight = new JPanel();
		statusPanel.add(statsLeft);
		statusPanel.add(statsRight);
		statsLeft.setMinimumSize(new Dimension(250, centerPanel.getHeight()));
		statsRight.setMinimumSize(new Dimension(250, centerPanel.getHeight()));
		
		statsLeft.setLayout(new BoxLayout(statsLeft,BoxLayout.Y_AXIS));
		statsLeft.setAlignmentY(TOP_ALIGNMENT);
		statsRight.setLayout(new BoxLayout(statsRight,BoxLayout.Y_AXIS));
		statsRight.setAlignmentY(TOP_ALIGNMENT);
		
		JPanel statsPanel = new JPanel();
		statsPanel.setLayout(new BoxLayout(statsPanel,BoxLayout.Y_AXIS));
		statsPanel.setMinimumSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/3));
//		statsPanel.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/2));
		
		JPanel currentStatusPanel = new JPanel();
		currentStatusPanel.setLayout(new BoxLayout(currentStatusPanel,BoxLayout.Y_AXIS));
		currentStatusPanel.setMinimumSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/3));
//		currentStatusPanel.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/2));
		JPanel traitPanel = new JPanel();
		traitPanel.setLayout(new BoxLayout(traitPanel,BoxLayout.Y_AXIS));
		traitPanel.setMinimumSize(new Dimension(statsLeft.getWidth(), centerPanel.getHeight()/2));
//		traitPanel.setPreferredSize(new Dimension(statsLeft.getWidth(), centerPanel.getHeight()));
		JPanel bioPanel = new JPanel();
		bioPanel.setLayout(new BoxLayout(bioPanel,BoxLayout.Y_AXIS));
		bioPanel.setMinimumSize(new Dimension(statsLeft.getWidth(), centerPanel.getHeight()/3));
		JScrollPane statsScroll = new JScrollPane();
		statsScroll.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/2));
		JScrollPane traitScroll = new JScrollPane();
		traitScroll.setPreferredSize(new Dimension(statsRight.getWidth(), 3*centerPanel.getHeight()/2));
		JScrollPane statusScroll = new JScrollPane();
		statusScroll.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight()/2));
		
		JSeparator sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsLeft.getWidth(), 2));
		statsLeft.add(sep);
		JLabel biolbl = new JLabel("Player Info");
		biolbl.setForeground(textColor);
		biolbl.setFont(new Font("Georgia", 1, 24));
		statsLeft.add(biolbl);
		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsLeft.getWidth(), 2));
		statsLeft.add(sep);
		
		JLabel name = new JLabel(player.name());
		name.setForeground(textColor);
		name.setFont(new Font("Georgia", 1, 20));
		JLabel level = new JLabel("Level: "+player.getLevel());
		level.setForeground(textColor);
		level.setFont(new Font("Georgia", 1, 20));
		JLabel exp = new JLabel("XP: "+player.getXP());
		exp.setForeground(textColor);
		exp.setFont(new Font("Georgia", 1, 20));
		JLabel rank = new JLabel("Rank: "+player.getRank());
		rank.setForeground(textColor);
		rank.setFont(new Font("Georgia", 1, 20));		
		bioPanel.add(name);
		bioPanel.add(level);
		bioPanel.add(exp);
		bioPanel.add(rank);
		statsLeft.add(bioPanel);
		
		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsRight.getWidth(), 2));
		statsRight.add(sep);
		JLabel attibuteslbl = new JLabel("Attributes");
		attibuteslbl.setForeground(textColor);
		attibuteslbl.setFont(new Font("Georgia", 1, 24));
		statsRight.add(attibuteslbl);
		statsRight.add(statsScroll);
		statsScroll.setViewportView(statsPanel);

		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsLeft.getWidth(), 2));
		statsLeft.add(sep);
		JLabel traitslbl = new JLabel("Traits");
		traitslbl.setForeground(textColor);
		traitslbl.setFont(new Font("Georgia", 1, 24));
		statsLeft.add(traitslbl);
		statsLeft.add(traitScroll);
		traitScroll.setViewportView(traitPanel);
		
		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsRight.getWidth(), 2));
		statsRight.add(sep);
		JLabel statuslbl = new JLabel("Current Status");
		statuslbl.setForeground(textColor);
		statuslbl.setFont(new Font("Georgia", 1, 24));
		statsRight.add(statuslbl);
		statsRight.add(statusScroll);
		statusScroll.setViewportView(currentStatusPanel);
		
		statsLeft.setBackground(backgroundColor);
		statsRight.setBackground(backgroundColor);
		bioPanel.setBackground(backgroundColor);
		traitPanel.setBackground(backgroundColor);
		currentStatusPanel.setBackground(backgroundColor);
		statsPanel.setBackground(backgroundColor);

		int count = 0;
		ArrayList<JLabel> attlbls = new ArrayList<JLabel>();
		for (Attribute a : player.att.keySet()){
			int amt = player.get(a);
			if (amt > 0) {

				JLabel dirtyTrick = new JLabel(a.toString() + ": " + amt);

				dirtyTrick.setForeground(textColor);
				dirtyTrick.setFont(new Font("Georgia", 1, 16));
				dirtyTrick.setHorizontalAlignment(SwingConstants.CENTER);
				dirtyTrick.setToolTipText(a.getDescription());
				attlbls.add(count, dirtyTrick);

				statsPanel.add(attlbls.get(count));
				count++;
			}
		}
		count = 0;
		ArrayList<JLabel> traitslbls = new ArrayList<JLabel>();
		for (Trait t: player.traits){
			JLabel dirtyTrick = new JLabel(t.toString());
			dirtyTrick.setForeground(textColor);
			dirtyTrick.setFont(new Font("Georgia", 1, 16));
			dirtyTrick.setHorizontalAlignment(SwingConstants.CENTER);
			dirtyTrick.setToolTipText(t.getDesc());
			traitslbls.add(count, dirtyTrick);

			traitPanel.add(traitslbls.get(count));
			count++;
		}
		count = 0;
		ArrayList<JLabel> stslbls = new ArrayList<JLabel>();
		for (String status: player.listStatus()){
			JLabel dirtyTrick = new JLabel(status.toString());
			dirtyTrick.setForeground(textColor);
			dirtyTrick.setFont(new Font("Georgia", 1, 16));
			dirtyTrick.setHorizontalAlignment(SwingConstants.CENTER);
			stslbls.add(count, dirtyTrick);

			currentStatusPanel.add(stslbls.get(count));
			count++;
		}if(player.listStatus().isEmpty()){
			JLabel noStatus = new JLabel("No status effects");
			noStatus.setForeground(textColor);
			noStatus.setFont(new Font("Georgia", 1, 18));
			currentStatusPanel.add(noStatus);
		}
		this.statusPanel.repaint();
		this.statusPanel.revalidate();
		this.centerPanel.revalidate();
		
		
	}

	public void update(Observable arg0, Object arg1) {
		refresh();
		if (this.combat != null) {
			if(this.combat.phase!=2){
				resetPortrait();
				loadPortrait(this.combat.p1,this.combat.p2);
			}
			combatMessage(this.combat.getMessage());
			this.combat.showImage();
			if ((this.combat.phase == 0) || (this.combat.phase == 2)) {
				next(this.combat);
			}
		}
	}
	
	public int nSkillsForGroup(TacticGroup group) {
        return skills.get(group).size();
    }

    public void switchTactics(TacticGroup group) {
        groupBox.removeAll();
        currentTactics = group;
        Global.gui().showSkills();
    }
    
    public void disableOptions(){
    	this.mntmOptions.setEnabled(false);
    }
    public void enableOptions(){
    	this.mntmOptions.setEnabled(true);
    }
    public boolean lowRes(){
    	return lowres;
    }

	private class NextButton extends KeyableButton {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6773730244369679822L;
		private Combat combat;

		public NextButton(Combat combat) {
			super("Next");
			setFont(new Font("Georgia", 0, 18));
			this.combat = combat;
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUI.this.clearCommand();
					if (GUI.NextButton.this.combat.phase == 0) {
						GUI.NextButton.this.combat.clear();
						GUI.this.clearText();
						GUI.NextButton.this.combat.turn();
					} else if (GUI.NextButton.this.combat.phase == 2) {
						GUI.this.clearCommand();
						if (!GUI.NextButton.this.combat.end()) {
							GUI.this.endCombat();
						}
					}
				}
			});
		}
	}

	private class EventButton extends KeyableButton {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7130158464211753531L;
		protected Activity event;
		protected String choice;

		public EventButton(Activity event, String choice) {
			super(choice);
			setFont(new Font("Georgia", 0, 18));
			this.event = event;
			this.choice = choice;
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUI.EventButton.this.event
							.visit(GUI.EventButton.this.choice);
					Global.gui().refreshLite();
				}
			});
		}
		
		public EventButton(Activity event, String choice, String description) {
			this(event, choice);
			this.setToolTipText(description);
		}
	}

	private class ItemButton extends GUI.EventButton {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3200753975433797292L;

		public ItemButton(Activity event, Item i) {
			super(event, i.getName());
			setFont(new Font("Georgia", 0, 18));
			setToolTipText(i.getDesc());
		}
		public ItemButton(Activity event, Clothing i) {
			super(event, i.getProperName());
			setFont(new Font("Georgia", 0, 18));
			setToolTipText(i.getSpecialDescription());
		}
	}

	private class AttributeButton extends KeyableButton {
		private Attribute att;

		public AttributeButton(Attribute att) {
			super(att.toString());
			setFont(new Font("Georgia", 0, 18));
			this.att = att;
			setToolTipText(att.getDescription());
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUI.this.player.mod(GUI.AttributeButton.this.att, 1);
					GUI.this.player.attpoints -= 1;
					GUI.this.ding();
				}
			});
		}
	}

	private class FeatButton extends KeyableButton {
		private Trait feat;

		public FeatButton(Trait feat) {
			super(feat.toString());
			setFont(new Font("Georgia", 0, 18));
			this.feat = feat;
			setToolTipText(feat.getDesc());
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUI.this.player.add(FeatButton.this.feat);
					Global.gainSkills(GUI.this.player);
					GUI.this.endCombat();
				}
			});
		}
	}

	
	private class InterveneButton extends KeyableButton {
		private Encounter enc;
		private Character assist;

		public InterveneButton(Encounter enc, Character assist) {
			super("Help " + assist.name());
			setFont(new Font("Georgia", 0, 18));
			this.enc = enc;
			this.assist = assist;
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUI.InterveneButton.this.enc.intrude(GUI.this.player,
							GUI.InterveneButton.this.assist);
				}
			});
		}
	}

	private class ActivityButton extends KeyableButton {
		private Activity act;

		public ActivityButton(Activity act) {
			super(act.toString());
			setFont(new Font("Georgia", 0, 18));
			this.act = act;
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUI.ActivityButton.this.act.visit("Start");
				}
			});
		}
		
		public ActivityButton(Activity act, String description) {
			this(act);
			this.setToolTipText(description);
		}
	}

	private class SleepButton extends KeyableButton {

		public SleepButton() {
			super("Go to sleep");
			setFont(new Font("Georgia", 0, 18));
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Global.dawn();
				}
			});
		}
	}

	private class PageButton extends KeyableButton {
		private int page;

		public PageButton(String label,int page) {
			super(label);
			setFont(new Font("Georgia", 0, 18));
			this.page = page;
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GUI.this.commandPanel.reset();
					GUI.this.showSkills();
				}
			});
		}
	}
	
	private class MyPainter implements Painter<JProgressBar> {

	    private final Color color;

	    public MyPainter(Color c1) {
	        this.color = c1;
	    }
	    @Override
	    public void paint(Graphics2D gd, JProgressBar t, int width, int height) {
	        gd.setColor(color);
	        gd.fillRect(0, 0, width, height);
	    }
	}
	public void changeClothes(Character player, Activity event) {
		closet.update(event);
		CardLayout midLayout = (CardLayout) (midPanel.getLayout());
        midLayout.show(midPanel, USE_CLOSET_UI);
        midPanel.repaint();
	}
	public void removeClosetGUI(){
		CardLayout midLayout = (CardLayout) (midPanel.getLayout());
        midLayout.show(midPanel, USE_MAIN_TEXT_UI);
        midPanel.repaint();
		displayStatus();
	}
}