package gui;
import global.Global;
import global.Modifier;
import items.Clothing;

import java.util.ArrayList;
import java.util.HashMap;

import characters.Character;

import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.Box;

import daytime.Activity;

import java.awt.Font;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClothesChangeGUI extends JPanel {
	private Character player;
	private Activity resume;
	private ArrayList<Clothing> TopOut;
	private ArrayList<Clothing> TopMid;
	private ArrayList<Clothing> TopIn;
	private ArrayList<Clothing> BotOut;
	private ArrayList<Clothing> BotIn;
	private String noneString = "";
	private JComboBox<String> TOBox;
	private JComboBox<String> TMBox;
	private JComboBox<String> TIBox;
	private JComboBox<String> BOBox;
	private JComboBox<String> BIBox;
	private HashMap<String, Clothing> translator;
	
	public ClothesChangeGUI(Character player){
		this.player = player;
		translator = new HashMap<String,Clothing>();
		translator.put(Clothing.none.getFullDesc(), Clothing.none);
		setLayout(new GridLayout(0, 1, 0, 0));
		
		TopOut = new ArrayList<Clothing>();
		TopMid = new ArrayList<Clothing>();
		TopIn = new ArrayList<Clothing>();
		BotOut = new ArrayList<Clothing>();
		BotIn = new ArrayList<Clothing>();
		
		JSeparator separator_1 = new JSeparator();
		add(separator_1);
		
		JLabel lblTop = new JLabel("Top");
		lblTop.setHorizontalAlignment(SwingConstants.CENTER);
		lblTop.setFont(new Font("Georgia", Font.PLAIN, 20));
		add(lblTop);
		
		TOBox = new JComboBox<String>();
		TOBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		add(TOBox);
		TOBox.addItem(Clothing.none.getFullDesc());
		TOBox.setSelectedItem(Clothing.none.getFullDesc());
		
		TMBox = new JComboBox<String>();
		TMBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		add(TMBox);
		TMBox.addItem(Clothing.none.getFullDesc());
		TMBox.setSelectedItem(Clothing.none.getFullDesc());
		
		TIBox = new JComboBox<String>();
		TIBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		add(TIBox);
		TIBox.addItem(Clothing.none.getFullDesc());
		TIBox.setSelectedItem(Clothing.none.getFullDesc());
		
		JSeparator separator = new JSeparator();
		add(separator);
		
		JLabel lblBottom = new JLabel("Bottom");
		lblBottom.setHorizontalAlignment(SwingConstants.CENTER);
		lblBottom.setFont(new Font("Georgia", Font.PLAIN, 20));
		add(lblBottom);
		
		BOBox = new JComboBox<String>();
		BOBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		add(BOBox);
		BOBox.addItem(Clothing.none.getFullDesc());
		BOBox.setSelectedItem(Clothing.none.getFullDesc());
		
		BIBox = new JComboBox<String>();
		BIBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		add(BIBox);
		
		JSeparator separator_2 = new JSeparator();
		add(separator_2);
		
		Box horizontalBox_2 = Box.createHorizontalBox();
		add(horizontalBox_2);
		
		Component horizontalStrut = Box.createHorizontalStrut(200);
		horizontalBox_2.add(horizontalStrut);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClothesChangeGUI.this.player.outfit[0].clear();
				ClothesChangeGUI.this.player.outfit[1].clear();
				if(ClothesChangeGUI.this.TIBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[0].push(translator.get((String) ClothesChangeGUI.this.TIBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.TMBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[0].push(translator.get((String) ClothesChangeGUI.this.TMBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.TOBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[0].push(translator.get((String) ClothesChangeGUI.this.TOBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.BIBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[1].push(translator.get((String) ClothesChangeGUI.this.BIBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.BOBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[1].push(translator.get((String) ClothesChangeGUI.this.BOBox.getSelectedItem()));
				}
				ClothesChangeGUI.this.player.change(Modifier.normal);
				Global.gui().removeClosetGUI();
				ClothesChangeGUI.this.resume.visit("Leave");
			}
		});
		btnOk.setFont(new Font("Georgia", Font.PLAIN, 24));
		horizontalBox_2.add(btnOk);

	}
	public void update(Activity event){
		this.resume = event;
		TopOut.clear();
		TopMid.clear();
		TopIn.clear();
		BotOut.clear();
		BotIn.clear();
		TOBox.removeAllItems();
		TOBox.addItem(Clothing.none.getFullDesc());
		TOBox.setSelectedItem(Clothing.none.getFullDesc());
		TMBox.removeAllItems();
		TMBox.addItem(Clothing.none.getFullDesc());
		TMBox.setSelectedItem(Clothing.none.getFullDesc());
		TIBox.removeAllItems();
		TIBox.addItem(Clothing.none.getFullDesc());
		TIBox.setSelectedItem(Clothing.none.getFullDesc());
		BOBox.removeAllItems();
		BOBox.addItem(Clothing.none.getFullDesc());
		BOBox.setSelectedItem(Clothing.none.getFullDesc());
		BIBox.removeAllItems();

		for(Clothing article: player.closet){
			translator.put(article.getFullDesc(), article);
			switch(article.getType()){
			case TOPOUTER:
				TopOut.add(article);
				TOBox.addItem(article.getFullDesc());
				break;
			case TOP:
				TopMid.add(article);
				TMBox.addItem(article.getFullDesc());
				break;
			case TOPUNDER:
				TopIn.add(article);
				TIBox.addItem(article.getFullDesc());
				break;
			case BOTOUTER:
				BotOut.add(article);
				BOBox.addItem(article.getFullDesc());
				break;
			case UNDERWEAR:
				BotIn.add(article);
				BIBox.addItem(article.getFullDesc());
				break;
			}
		}
		
		for(Clothing article: player.outfit[0]){
			if(TopOut.contains(article)){
				TOBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[0]){
			if(TopMid.contains(article)){
				TMBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[0]){
			if(TopIn.contains(article)){
				TIBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[1]){
			if(BotOut.contains(article)){
				BOBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[1]){
			if(BotIn.contains(article)){
				BIBox.setSelectedItem(article.getFullDesc());
			}
		}
	}
}
