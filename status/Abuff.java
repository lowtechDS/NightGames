package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Abuff extends Status {
	private Attribute modded;
	private int value;
	public Abuff(Attribute att, int value, int duration) {
		super(att+" buff");
		this.modded=att;
		this.duration=duration;
		this.value=value;
		if(value<0){
			this.name = att+" debuff";
		}
	}
	public Abuff(Character affected, Attribute att, int value, int duration) {
		super(att+" buff", affected);
		this.modded=att;
		if(affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		else{
			this.duration=duration;
		}
		this.value=value;
		if(value<0){
			this.name = att+" debuff";
		}
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int mod(Attribute a) {
		if(a==modded){
			return value;
		}
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}
	public boolean lingering(){
		return true;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Abuff(target, modded, value, duration);
	}
	@Override
	public void turn(Combat c) {
		decay();
	}
}
