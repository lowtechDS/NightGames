package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Caffeinated extends Status {
	public Caffeinated(int duration){
		super("Caffeinated");
		this.duration=duration;
		magnitude = 7;
		stacking = true;
		lingering = true;
	}
	public Caffeinated(int duration,int magnitude){
		this(duration);
		this.magnitude = magnitude;
	}
	public Caffeinated(Character affected,int duration,int magnitude) {
		super("Caffeinated",affected);
		if(affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}else{
			this.duration = duration;
		}
		this.magnitude = magnitude;
		stacking = true;
		lingering = true;
	}


	@Override
	public String describe() {
		if(affected.human()){
			return "You feel the effects of the energy drink filling you with energy.";
		}
		else{
			return affected.name()+" looks a little hyper.";
		}
	}

	@Override
	public int mod(Attribute a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int regen() {
		return 7*magnitude;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Caffeinated(target,duration,magnitude);
	}
	@Override
	public void turn(Combat c) {
		affected.buildMojo(magnitude);
		decay();
	}
	
}
