package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Distorted extends Status {

	public Distorted(){
		super("Distorted");
		magnitude = 5;
	}
	public Distorted(Character affected) {
		super("Distorted", affected);
		if(affected.has(Trait.PersonalInertia)){
			duration = 9;
		}else{
			duration = 6;
		}
		flag(Stsflag.distorted);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your image is distorted, making you hard to hit.";
		}
		else{
			return "Multiple "+affected.name()+"s appear in front of you. When you focus, you can tell which one is real, but it's still screwing up your accuracy.";
		}
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		affected.emote(Emotion.confident,5);
		return 0;
	}

	@Override
	public int damage(int x) {
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return 0;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return 0;
	}

	@Override
	public int evade() {
		return magnitude;
	}

	@Override
	public int escape() {
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}

	@Override
	public int counter() {
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Distorted(target);
	}
	@Override
	public void turn(Combat c) {
		decay();
	}

}
