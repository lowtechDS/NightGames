package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Energized extends Status {
	
	public Energized(int duration){
		super("Energized");
		this.duration=duration;
		lingering = true;
	}
	public Energized(Character affected,int duration) {
		super("Energized", affected);
		if(affected.has(Trait.PersonalInertia)){
			this.duration=duration;
		}else{
			this.duration=3*duration/2;
		}
		flag(Stsflag.energized);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're brimming with energy.";
		}else{
			return "";
		}
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		duration--;
		if(duration<0){
			affected.removelist.add(this);
		}
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Energized(target,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.buildMojo(10);
		affected.emote(Emotion.confident,5);
		affected.emote(Emotion.dominant,10);
		decay();
	}
}
