package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;

public class Feral extends Status {

	public Feral(){
		super("Feral");
	}
	public Feral(Character affected) {
		super("Feral", affected);
		flag(Stsflag.feral);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You feel your animal instincts start to take over, increasing your strength and speed.";
		}
		else{
			return "";
		}
	}

	@Override
	public int mod(Attribute a) {
		switch(a){
		case Power:
			return affected.get(Attribute.Animism)/5;
		case Cunning:
			return affected.get(Attribute.Animism)/4;
		case Seduction:
			return affected.get(Attribute.Animism)/4;
		case Animism:
			return 2;
		case Speed:
			return affected.get(Attribute.Animism)/6;
		}
		return 0;
	}

	@Override
	public int regen() {
		if(affected.getArousal().percent()<50){
			affected.removelist.add(this);
		}
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Feral(target);
	}
	@Override
	public void turn(Combat c) {
		// TODO Auto-generated method stub
		
	}

}
