package status;

import combat.Combat;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Sensitive extends Status {
	private Anatomy part;
	private float value;
	public Sensitive(int duration, Anatomy part, float value){
		super("Sensitive: "+part);
		this.duration=duration;
		this.part = part;
		this.value = value;
		lingering = true;
		if(value<1.0f){
			this.name = "Numb: "+part;
		}
	}
	public Sensitive(Character affected, int duration, Anatomy part, float value) {
		super("Sensitive: "+part, affected);
		if(affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		else{
			this.duration=duration;
		}
		this.flag(Stsflag.sensitive);
		this.part = part;
		this.value = value;
		if(value<1.0f){
			this.name = "Numb: "+part;
		}
	}

	@Override
	public String describe() {
		return "";
	}
	@Override
	public int regen() {
		return 0;		
	}

	@Override
	public int value() {
		return (int) value;
	}
	public float sensitive(Anatomy targeted){
		if(part==targeted){
			return value;
		}
		return 1.0f;
	}
	@Override
	public Status copy(Character target) {
		return new Sensitive(target,duration,part,value);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.horny,10);
		decay();
	}
}
