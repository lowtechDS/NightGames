package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Buzzed extends Status {
	
	public Buzzed(){
		super("Buzzed");
		magnitude = 1;
		stacking = true;
		lingering = true;
	}
	public Buzzed(Character affected) {
		super("Buzzed", affected);
		if(affected.has(Trait.PersonalInertia)){
			duration = 30;
		}else{
			duration = 20;
		};
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You feel a pleasant buzz, which makes you a bit sluggish, but also takes the edge off your sense of touch.";
		}
		else{
			return affected.name()+" looks mildly buzzed, probably trying to dull her senses.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a == Attribute.Perception){
			return -3*magnitude;
		}
		else if(a == Attribute.Power){
			return -1*magnitude;
		}
		else if(a == Attribute.Cunning){
			return -2*magnitude;
		}
		return 0;
	}

	@Override
	public int regen() {
		duration--;
		if(duration<=0){
			affected.removelist.add(this);
		}
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return -(x*magnitude)/10;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		return -5;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Buzzed(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,15);
		decay();
	}
	
}
