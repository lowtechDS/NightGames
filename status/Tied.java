package status;

import characters.Attribute;
import characters.Character;

import combat.Combat;

public class Tied extends Status {

	public Tied() {
		super("Tied Up");
		flag(Stsflag.tied);
	}
	public Tied(Character affected){
		super("Tied Up",affected);
		flag(Stsflag.tied);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "The rope wrapped around you digs into your body, but only slows you down a bit.";
		}
		else{
			return affected.name()+" squirms against the rope, but you know you tied it well.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a==Attribute.Speed){
			return -1;
		}
		return 0;
	}

	@Override
	public int regen() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		return -10;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void turn(Combat c) {
		
	}

	@Override
	public Status copy(Character target) {
		return new Tied(target);
	}

}
