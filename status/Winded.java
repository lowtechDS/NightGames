package status;

import java.util.HashSet;

import combat.Combat;

import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;

public class Winded extends Status {
	
	public Winded(){
		super("Winded");
	}
	public Winded(Character affected) {
		super("Winded", affected);
		duration=3;
		flag(Stsflag.stunned);
	}
	public Winded(Character affected, int duration) {
		super("Winded", affected);
		this.duration=duration;
		flag(Stsflag.stunned);
	}
	@Override
	public String describe() {
		if(affected.human()){
			return "You need a moment to catch your breath";
		}
		else{
			return affected.name()+" is panting and trying to recover";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a==Attribute.Power||a==Attribute.Speed){
			return -2;
		}
		else{
			return 0;
		}
	}

	@Override
	public int regen() {
		return affected.getStamina().max()/4;
	}

	@Override
	public int damage(int x) {
		return -x;
	}

	@Override
	public int pleasure(int x) {
		return 0;
	}

	@Override
	public int weakened(int x) {
		return -x;
	}

	@Override
	public int tempted(int x) {
		return -x/2;
	}

	@Override
	public int evade() {
		return -99;
	}

	@Override
	public int escape() {
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}
	@Override
	public int counter() {
		return -99;
	}
	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Winded();
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous,15);
		affected.emote(Emotion.angry,10);
		decay();
	}
}
