package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;

public class Oiled extends Status {

	public Oiled(){
		super("Oiled");
		lingering = true;
	}
	public Oiled(Character affected) {
		super("Oiled", affected);
		flag(Stsflag.oiled);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your skin is slick with oil and kinda feels weird.";
		}
		else{
			return affected.name()+" is shiny with lubricant, making you more tempted to touch and rub her skin.";
		}
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return x/4;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return 0;
	}

	@Override
	public int evade() {
		return 0;
	}

	@Override
	public int escape() {
		return 8;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}
	@Override
	public int counter() {
		return 0;
	}
	
	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Oiled(target);
	}
	@Override
	public void turn(Combat c) {
		// TODO Auto-generated method stub
		
	}
}
