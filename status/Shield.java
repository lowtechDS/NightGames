package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Shield extends Status {
	
	public Shield(){
		super("Shield");
	}
	public Shield(Character affected) {
		super("Shield", affected);
		if(affected.has(Trait.PersonalInertia)){
			this.duration=6;
		}
		else{
			this.duration=4;
		}
		flag(Stsflag.shielded);
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		return 0;		
	}

	@Override
	public int damage(int x) {
		return -4;
	}

	@Override
	public int pleasure(int x) {
		return 0;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return 0;
	}

	@Override
	public int evade() {
		return 0;
	}

	@Override
	public int escape() {
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}

	@Override
	public int counter() {
		return 2;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Shield(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		decay();
	}

}
