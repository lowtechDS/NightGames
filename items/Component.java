package items;

import characters.Character;

public enum Component implements Item {
	Tripwire	( "Trip Wire",10, "A strong wire used to trigger traps","a "	),
	Spring		( "Spring",20,	"A component for traps","a "	),
	Rope		( "Rope",15,"A component for traps","a "	),
	Phone		( "Phone",30,"A cheap disposable phone with a programable alarm","a "		),
	Sprayer		( "Sprayer",30,	"Necessary for making traps that use liquids","a "	),
	Totem		( "Fetish Totem",150,"A small penis shaped totem that can summon tentacles","a "),
	Capacitor	( "Capacitor",30,"","a "),
	Semen		( "Semen",0 ,"A small bottle filled with cum. Kinda gross", "a bottle of "),
	Battery		( "Battery",0,"Available energy to power electronic equipment","a "),
	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	private Component( String name, int price, String desc,String prefix )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
}
