package items;

import characters.Character;

public enum Consumable implements Item {
	ZipTie		( "Heavy Zip Tie",5,"A thick heavy tie suitable for binding someone's hands","a "),
	Handcuffs	( "Handcuffs",200,"Strong steel restraints, hard to escape from",""),
	Talisman	( "Dark Talisman",100,"","a "),
	FaeScroll	( "Summoning Scroll",150,"","a "),
	needle		( "Drugged Needle",10,"","a "),
	smoke		( "Smoke Bomb",20,"","a "),
	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	private Consumable( String name, int price, String desc,String prefix )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
}
