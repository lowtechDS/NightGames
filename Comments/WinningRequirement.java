package Comments;
import characters.Character;
import combat.Combat;

public class WinningRequirement implements CustomRequirement {

	private static final int INITIAL_WINNING_SCORE = 10;
	
	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null)
			return false;
		int score = INITIAL_WINNING_SCORE;
		score += (other.getArousal().percent() - 50) / 2;
		score += self.getFitness(self.getUrgency(),c.stance) - other.getFitness(other.getUrgency(),c.stance);
		if (c.stance.dom(self))
			score += 10;
		if (c.stance.mobile(self))
			score += 5;
		if (c.stance.mobile(other))
			score -= 5;
		return score >= 0;
	}

}