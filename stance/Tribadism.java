package stance;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;

public class Tribadism extends Position {

	public Tribadism(Character top, Character bottom) {
		super(top, bottom, Stance.trib);
	}

	@Override
	public String describe() {
		return "Trib position: this should never display";
	}

	@Override
	public boolean mobile(Character c) {
		return c==top;
	}

	@Override
	public boolean kiss(Character c) {
		return true;
	}

	@Override
	public boolean dom(Character c) {
		return c==top;
	}

	@Override
	public boolean sub(Character c) {
		return c==bottom;
	}

	@Override
	public boolean reachTop(Character c) {
		return true;
	}

	@Override
	public boolean reachBottom(Character c) {
		return false;
	}

	@Override
	public boolean prone(Character c) {
		return true;
	}

	@Override
	public boolean feet(Character c) {
		return false;
	}

	@Override
	public boolean oral(Character c) {
		return false;
	}

	@Override
	public boolean behind(Character c) {
		return false;
	}

	@Override
	public boolean penetration(Character c) {
		return false;
	}

	@Override
	public Position insert(Character c) {
		return new Tribadism(bottom,top);
	}
	
	public void checkOngoing(Combat c){
		int m = 6 + (2 * pace);
		int r = Math.max(1, 3-pace);
		if(top.has(Trait.experienced)){
			r = r*2;
		}
		if(pace>1){
			if(top.human()){
				c.write(top,"Your intense fucking continues to drive you both closer to ecstasy.");
			}else{
				c.write(top,"Her rapid scissoring gives you intense pleasure.");
			}				
		}else if(pace == 1){
			if(top.human()){
				c.write(top,"Your steady thrusting pleasures you both.");
			}else{
				c.write(top,"Her steady lovemaking continues to erode your resistence.");
			}
		}else{
			if(top.human()){
				c.write(top,"You slowly, but steadily grind against her.");
			}else{
				c.write(top,"She continues to stimulate your clit with slow, deliberate movements.");
			}
		}
		bottom.pleasure(top.bonusProficiency(Anatomy.genitals, m), Anatomy.genitals, c);
		top.pleasure(bottom.bonusProficiency(Anatomy.genitals, m/r+bottom.bonusRecoilPleasure()),Anatomy.genitals,c);
	}


}
