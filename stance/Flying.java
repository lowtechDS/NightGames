package stance;

import combat.Combat;

import characters.Character;
import characters.Anatomy;
import characters.Trait;

public class Flying extends Position {
	private static final long serialVersionUID = 1953597655795344915L;

	public Flying (Character succ, Character target) {
		super(succ, target, Stance.flying);
	}
	
	@Override
	public String describe() {
		return "You are flying some twenty feet up in the air,"
				+ " joinned to your partner by your hips.";
	}

	@Override
	public boolean mobile(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean kiss(Character c) {
		return true;
	}

	@Override
	public boolean dom(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean sub(Character c) {
		return !top.equals(c);
	}

	@Override
	public boolean reachTop(Character c) {
		return true;
	}

	@Override
	public boolean reachBottom(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean prone(Character c) {
		return !top.equals(c);
	}

	@Override
	public boolean feet(Character c) {
		return false;
	}

	@Override
	public boolean oral(Character c) {
		return false;
	}

	@Override
	public boolean behind(Character c) {
		return false;
	}

	@Override
	public boolean penetration(Character c) {
		return true;
	}


	public boolean flying(Character c) {
		return true;
	}
	public void decay(){
		time++;
	}
	public void checkOngoing(Combat c){
		if(top.human()){
			c.write("The strain of flying while carrying another person saps a bit of your stamina.");
		}
		top.weaken(3,c);
		if(top.getStamina().get()<5){
			if(top.human()){
				c.write("You're too tired to stay in the air. You plummet to the ground and "+bottom.name()+" drops on you heavily, knocking the wind out of you.");
			}
			else{
				c.write(top.name()+" falls to the ground and so do you. Fortunately, her body cushions your fall, but you're not sure she appreciates that as much as you do.");
			}
			top.pain(5,Anatomy.chest,c);
			c.stance = new Mount(bottom,top);
		}else{
			int m = 6 + (2 * pace);
			int r = Math.max(1, 3-pace);
			if(top.has(Trait.experienced)){
				r = r*2;
			}
			if(pace>1){
				if(top.human()){
					c.write(top,"Your intense fucking continues to drive you both closer to ecstasy.");
				}else{
					c.write(top,"Her rapid bouncing on your cock gives you intense pleasure.");
				}				
			}else if(pace == 1){
				if(top.human()){
					c.write(top,"Your steady thrusting pleasures you both.");
				}else{
					c.write(top,"Her steady lovemaking continues to erode your resistence.");
				}
			}else{
				if(top.human()){
					c.write(top,"You slowly, but steadily grind against her.");
				}else{
					c.write(top,"She continues to stimulate your penis with slow, deliberate movements.");
				}
			}
			bottom.pleasure(top.bonusProficiency(Anatomy.genitals, m), Anatomy.genitals, c);
			top.pleasure(bottom.bonusProficiency(Anatomy.genitals, m/r+bottom.bonusRecoilPleasure()),Anatomy.genitals,c);
		}
	}
	
	@Override
	public Position insert(Character c) {
		c.weaken(10);
		return new StandingOver(top, bottom);
	}

}
