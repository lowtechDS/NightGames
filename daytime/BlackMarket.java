package daytime;

import items.Consumable;
import items.Flask;
import items.Item;
import items.Potion;
import global.Flag;
import global.Global;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class BlackMarket extends Store {
	private boolean trained;
	public BlackMarket(Character player) {
		super("Black Market", player);
		add(Flask.Aphrodisiac);
		add(Flask.SPotion);
		add(Flask.DisSol);
		add(Potion.Beer);
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.blackMarket);
	}

	@Override
	public void visit(String choice) {
		if(choice=="Start"){
			acted=false;
			trained=false;
		}
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(choice=="Leave"){
			done(acted);
			return;
		}
		checkSale(choice);
		if(player.human()){
			if(Global.checkFlag(Flag.blackMarketPlus)){
				if(!Global.checkFlag(Flag.metRin)){
					Global.gui().message("You knock on the door to the black market. When Ridley answers, you tell him that you're here to see his premium goods on behalf of " +
							"Callisto. Ridley glances back into the room for a moment and then walks past you without saying anything. You stand there confused, until you see the " +
							"girl on the couch stand up and approach you with a smile.<p>"
							+ "<i>\"Hello "+player.name()+",\"</i> she says while extending her hand. <i>\"I'm Rin Callisto. You shouldn't " +
							"be surprised I know who you are, you've been putting on a good show lately.\"</i> You've seen her here before, but you've never taken a good look at her. She " +
							"has elegant features, shoulder length black hair, and looks a couple years older than you. She's very pretty, but you overlooked her because you assumed she " +
							"was Ridley's girlfriend.<p>"
							+ "<i>\"Mike isn't the most pleasant company, but he's a good middleman. He keeps his mouth shut and he doesn't ask questions.\"</i> He must " +
							"not be reliable enough to earn her trust, otherwise she wouldn't feel the need to keep an eye on him all the time.<p>"
							+ "<i>\"Aesop sold you my name, right? I'll have " +
							"to collect my share from him later. I have many items you won't find anywhere else; items that will give you an edge in the Games. Not all of them are completely " +
							"safe, but I think you knew that when you came looking for the black market.\"</i>");
					Global.flag(Flag.metRin);
				}
				else if(choice.startsWith("Cursed Artifacts")){
					Global.gui().message("You ask Rin about the assorted tomes and unpleasant looking idols she's laid out.<p>"
							+ "<i>\"You should be careful with those, they're all " +
							"cursed. None of them will kill you unless you're unusually susceptible to such things, but the effects would not be pleasant.\"</i> Ok, it sounds " +
							"like these aren't what you're looking for. You aren't looking to get cursed, no matter how useful the artifacts are.<p>"
							+ "<i>\"The items aren't very " +
							"valuable themselves, otherwise I'd have sold them to collectors. A skilled spiritualist could refine the curse to bestow an unholy boon. Fortunately, " +
							"I have the training to do so.\"</i> An unholy boon? That sounds more than a bit worrying.<p>"
							+ "<i>\"Are you religious? In my experience, demons are just another " +
							"flavor of spirit. It's still your choice, do what you like.\"</i>");
					Global.flag(Flag.darkness);
					acted=true;
				}
				else if(choice.startsWith("Dark Power")){
					if(player.money>=player.getAdvancedTrainingCost()){
						player.money-=player.getAdvancedTrainingCost();
						/*if(player.getPure(Attribute.Dark)>=5){
						 * Dominating the imps
						 * }
						 */

						/*if(player.getPure(Attribute.Dark)>=8){
						 * Learning Domination
						 * }
						 */
						Global.gui().message("Rin lights some incense and has you lie down on the couch with one of the cursed artifacts on your chest. As she performs a lengthy " +
								"ritual, you feel your body heat up and an overwhelming sense of danger flood through you. You're certain something powerful is trying to take control " +
								"of your soul.<p>"
								+ "Rin chants softly and wraps a talisman around a wooden rod. The presence inside you looms and seems ready to devour you, when suddenly she " +
								"strikes the artifact with the rod. A shock runs through your consciousness, and the sense of danger disappears. The dark power is still present, but it " +
								"seems tame now, willing to obey your command should you call for it.<p>"
								+ "<i>\"The ritual is complete. You can keep the artifact as a souvenir, but all its " +
								"power is in you now.\"</i><p>");
						player.mod(Attribute.Dark, 1);
						acted=true;
						trained=true;
					}
					else{
						Global.gui().message("You can't afford the artifacts.");
					}
				}
				else if(choice=="S&M Gear"){
					Global.gui().message("Rin opens a case containing a variety of S&M toys and bondage gear.<p>"
							+ "<i>\"You probably shouldn't touch those,\"</i> she says as you reach for them. " +
							"<i>\"I didn't pick these up at a sex shop, they have potent enchantments on them.\"</i><p>"
							+ "Enchanted sex toys? At this point, you'll believe just about anything. How " +
							"useful are they? <i>\"They won't do you much good to use them during the match, but if you use them now, their power will transfer to you. Mostly they'll give " +
							"you fetishes that you'll be able to share with your opponents. It's a high risk, high reward style of sex-fighting.\"</i>"
							+ "<p>Rin smiles and reclines on the couch. " +
							"<i>\"If you buy something, I don't mind helping you try it out. No extra charge of course, I'm not a prostitute.\"</i>");
					Global.flag(Flag.fetishism);
					acted=true;
				}
				else if(choice.startsWith("Fetishism")){
					if(player.money>=player.getAdvancedTrainingCost()){
						player.money-=player.getAdvancedTrainingCost();
						player.mod(Attribute.Fetish, 1);
						acted=true;
						trained=true;
						acted=true;
						clearRandomScenes();
						addRandomScene("Masochism",1);
						String scene = "Masochism";

						if(player.getPure(Attribute.Fetish)>6){
							addRandomScene("Bondage",2);
						}

						if(player.getPure(Attribute.Fetish)==6){
							scene = "Bondage";
						}else{
							scene = getRandomScene();
						}
						if(scene.startsWith("Masochism")){
							Global.gui().message("You select one of the S&M toys and pay Rin for it. She picks up the toy - a leather riding crop - and "
									+ "gives you a small wicked smile.<p>"
									+ "<i>\"Good choice. This one will grant you the masochism fetish if used properly. I'm quite good with these. "
									+ "Get undressed and we'll get started.\"</i><p>"
									+ "Your trepidation must show on your face, because her smiles becomes "
									+ "slightly more reassuring. <i>\"Sometimes the path to power can be painful. The gifts in these items may bring you "
									+ "victory, but there's no easy way to unlock them. Besides, you'll enjoy it before we're through. That's the whole "
									+ "point after all.\"</i><p>"
									+ "You strip and stand awkwardly in front of Rin who grins unabashedly as she stares at your flaccid member, twirling "
									+ "the leather riding crop between her fingers.<p>"
									+ "<i>\"I must say, "+player.name()+", you�fre quickly becoming one of my favorite customers.\"</i><p>"
									+ "Rin expertly twirls the riding crop one last time before sending a hard swat to your nipple!<p>"
									+ "You gasp at the stinging sensation and are surprised by the animalistic urge that flares in the back of your mind.<p>"
									+ "She reels back and smacks your other nipple even harder than before! Another primal, beastly feeling flared in the "
									+ "back of your mind! Your chest is heaving with breaths that are coming faster and faster. Your mind rushes with images "
									+ "of slapping the riding crop from Rin�fs hands and pushing her up against the wall!<p>"
									+ "Rin traces the tip of the riding crop down your abs and teases your stiffening cock for a moment before sliding it "
									+ "toward your butt. She reels back and delivers full strength blow with a loud SMACK!<p>"
									+ "Much to your own surprise, you moan in pleasure!<p>"
									+ "As the pain from the welting blow on your ass grows your mind races with images of ripping Rin�fs clothes off and "
									+ "making her yours. You�fre starting to understand that the pain gets you in touch with your inner beast; the part of "
									+ "you that was designed to hunt sabretooth tigers and to mount girls like Rin.<p>"
									+ "And Rin? The fact that such a petite, slender girl was causing you this pain was incredibly arousing! You, a strong "
									+ "man who could stop this at any moment, wait in excitement for where she�fll hit you next. Wherever it is, you know you "
									+ "can take it!<p>"
									+ "<i>\"Wow,\"</i> Rin is staring in wide-eyed disbelief at your throbbing erection. <i>\"I�fve never seen the enchanted "
									+ "riding crop work so quickly! You must�fve been a fairly kinky man before this!\"</i><p>"
									+ "She delivers another intoxicating smack to your butt!<p>"
									+ "<i>\"Let�fs make a fun little wager! If you stay standing for three more hits than I�fll let you fuck me however "
									+ "you please.\"</i> Rin doesn�ft even wait for you to acknowledge her bet before she flicks her wrist between your legs!<p>"
									+ "The tip of the riding crop hits your right ball, causing an intense flash of pain that almost drops you to your knees.<p>"
									+ "<i>\"Hmm, not bad, but I�fve had men stay standing after the first blow before.\"</i> Rin gently rubs your balls with the "
									+ "riding crop as she mocks you. She slowly reels the riding crop back as she prepares her next strike.<p>"
									+ "You fight the instinctive urge to protect yourself and grit your teeth.<p>"
									+ "The riding crop smacks hard against your left testicle! Your knees wobble as the intense pain takes over all your senses. "
									+ "You almost to drop to the floor but stay standing so your struggle thus far wasn�ft in vain.<p>"
									+ "<i>\"Wow! I am impressed!\"</i> Rin reaches over and cups your burning testicles. <i>\"Almost everyone is done by the second swat! I wonder if you�fll be the first to stay standing after all three! I doubt it though, seeing as you were just as dumb as the others.\"</i><p>Before you can fully comprehend the implication of that statement, Rin grabs your shoulder and launches a devastating knee to your testicles!<p>You drop to the floor and moan, clutching at your bruised balls.<p><i>\"I said three hits. Not three hits with the riding crop. Anyhow, your erection is proof that the fetish has been transferred. You may take as long as you need to recover before you show yourself out.\"</i>");
						}else if(scene.startsWith("Bondage")){
							Global.gui().message("You pull a long, golden rope out of the box and pay Rin for it. She gently picks up the rope and smiles seductively.<p>"
									+ "<i>\"A man after my own tastes. This rope will bestow unto you a bondage fetish. Please hold out your wrists and we will "
									+ "begin the process.\"</i><p>"
									+ "You anxiously hold out your wrists and present them to Rin.<p> "
									+ "Your self-doubt at the choice you made grows as you watch her expertly tie a comfortable, yet inescapable, knot around "
									+ "your wrists. Just as you�fre about to voice your concerns she gives the rope one final tug before taking a step back "
									+ "and admiring her work.<p>"
									+ "For a moment it seems as if nothing happened. You�fre about to call the entire thing off before you feel a familiar "
									+ "twitch of arousal in your cock. Your cock continues to grow as your heart and breath begin to race. You look up at "
									+ "Rin in bewilderment who, you just now notice, has been hungrily staring at you this entire time."
									+ "<i>\"You know,\"</i> she gives your bulge a soft squeeze before she begins unbuttoning your pants, <i>\"this fetish "
									+ "isn�ft about immobility. Rather, it�fs a fetish for being completely vulnerable to your captor.\"</i>"
									+ "Your pants fall to the floor and, to your bewilderment, your cock is harder than you can ever recall seeing it. It "
									+ "throbs between you two before she places a delicate hand on your cock and begins to slowly stroke your shaft.<p>"
									+ "<i>\"Tell me "+player.name()+",\"</i> her hand slides down your shaft and cups your balls, <i>\"how does it feel to be "
									+ "at my complete mercy?\"</i> Her hand gently, yet firmly, squeezes your delicate testicles.<p>"
									+ "It dawns on you that you hardly know this woman. You have no clue whether she�fll continue to treat you gently or if "
									+ "she has a sadistic streak. In a quick moment of panic you try to escape, but find the ropes just as unforgiving as when "
									+ "she tied them.<p>"
									+ "Realizing you�fre unable to escape sends an unexpected feeling of arousal through your body. Your mind races with lust, "
									+ "fear, arousal, and just as she predicted, a vulnerability that feels more delicious the longer it lasts.<p>"
									+ "Rin goes back to stroking your cock and smiles as she watches the wide range of emotions playing out on your face.<p>"
									+ "<i>\"Enjoying it more, yes? These ropes have had even the strongest of men on their knees. And don�ft worry. You�fre "
									+ "in absolutely no danger here.\"</i><p>"
									+ "You breathe a sigh of relief.<p>"
									+ "<i>\"I wouldn�ft relax too quickly. I�fll remind you that I said I would help you learn the fetish of the item you purchased. "
									+ "Never once did I say I would help you cum.\"</i><p>"
									+ "Rin teases your cock for almost three hours, constantly stroking or sucking you to the edge of release but never letting "
									+ "you reach climax. By the end of your session you�fre on your knees, begging her to let you cum while at the same time "
									+ "secretly hoping she doesn�ft just so this pleasurable torture can continue.<p>"
									+ "Eventually, she rewards you for holding out as long as you did, and she swallows every drop of cum from your screaming "
									+ "erection down her throat.<p>"
									+ "You leave the shop in a haze, unsure of what happened, but with the rope clutched tightly in your hand.");
						}
						
					}
					else{
						Global.gui().message("You can't afford that.");
					}
				}
				else{
					Global.gui().message("Ridley leaves the black market as soon as he sees you. It seems you're a preferred client now, and you can deal with Rin directly. Rin gives " +
							"you a polite smile and stands up. <i>\"Welcome back. What can I do for you?\"</i>");
				}
				if(!trained){
					if(!Global.checkFlag(Flag.darkness)){
						Global.gui().choose(this,"Cursed Artifacts");
					}else{
						Global.gui().choose(this,"Dark Power: $"+(player.getAdvancedTrainingCost()));
					}
					if(!Global.checkFlag(Flag.fetishism)){
						Global.gui().choose(this,"S&M Gear");
					}else{
						Global.gui().choose(this,"Fetishism: $"+(player.getAdvancedTrainingCost()));
					}
				}
			}
			else{
				Global.gui().message("You knock on the door to the room Aesop pointed you to. A somewhat overweight man, a few years older than you, looks you over for a moment " +
					"before letting you in. The dorm room is tidier than its occupant, whose clothes are noticeably stained and smell faintly of old beer and weed. There's a bong on " +
					"the nearby table and an attractive girl on the couch who makes no indication that she noticed you enter. <i>\"Don't mind the bitch,\"</i> says Ridley, noticing your " +
					"attention. <i>\"She doesn't care who you are and neither do I. What are you looking for?\"</i>");
			}
			for(Item i: stock.keySet()){
				Global.gui().message(i.getName()+": $"+i.getPrice());
			}
			Global.gui().message("You have :$"+player.money+" to spend.");
			displayGoods();
			Global.gui().choose(this,"Leave");
		}
	}

	@Override
	public void shop(Character npc, int budget) {
		int remaining = budget;
		if(npc.getPure(Attribute.Dark)>0&&remaining>=player.getAdvancedTrainingCost()){
			if(remaining>=npc.getAdvancedTrainingCost()*2.5){
				npc.money-=npc.getAdvancedTrainingCost();
				remaining-=npc.getAdvancedTrainingCost();
				npc.mod(Attribute.Dark, 1);
			}
			npc.money-=player.getAdvancedTrainingCost();
			remaining-=player.getAdvancedTrainingCost();
			npc.mod(Attribute.Dark, 1);
		}
		if(npc.getPure(Attribute.Fetish)>0&&remaining>=player.getAdvancedTrainingCost()){
			if(remaining>=npc.getAdvancedTrainingCost()*2.5){
				npc.money-=npc.getAdvancedTrainingCost();
				remaining-=npc.getAdvancedTrainingCost();
				npc.mod(Attribute.Fetish, 1);
			}
			npc.money-=npc.getAdvancedTrainingCost();
			remaining-=npc.getAdvancedTrainingCost();
			npc.mod(Attribute.Fetish, 1);
		}
		int bored = 0;
		remaining = Math.min(remaining, 120);
		while(remaining>25&&bored<5){			
			for(Item i:stock.keySet()){
				if(remaining>i.getPrice()&&!npc.has(i,10)){
					npc.gain(i);
					npc.money-=i.getPrice();
					remaining-=i.getPrice();
				}
				else{
					bored++;
				}
			}
		}
	}
}
