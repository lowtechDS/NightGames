package daytime;

import global.Flag;
import global.Global;
import global.NightOff;
import global.Prematch;

import java.util.ArrayList;


import characters.Attribute;
import characters.NPC;
import characters.Player;
import characters.Character;

public class Daytime {
	private ArrayList<Activity> activities;
	private Player player;
	private int time;
	private Events auto;
	private int NPCDAYTIME = 8;
	
	
	
	public Daytime(Player player){
		Global.gui().clearText();
		this.player=player;
		manageFlags();
		buildActivities();
		this.auto = new Events(player, this);
		time = 9;
		if(auto.checkMorning()){
			return;
		}		
		plan();
	}
	public void manageFlags(){
		if(Global.checkFlag(Flag.metAlice)){
			if(Global.checkFlag(Flag.victory)){
				Global.unflag(Flag.AliceAvailable);
			}else{
				Global.flag(Flag.AliceAvailable);
			}
		}
		if(Global.checkFlag(Flag.metYui)){
			Global.flag(Flag.YuiAvailable);
		}
		if(Global.checkFlag(Flag.threesome)){
			Global.unflag(Flag.threesome);
		}
	}
	
	public void plan(){
		String scene;
		if(time<22){
			if(Global.getDayString(Global.getDate()).startsWith("Sunday")){
				Global.gui().message("It is currently "+displayTime()+". There is no match tonight.");

			}else{
				Global.gui().message("It is currently "+displayTime()+". Your next match starts at 10:00pm.");
			}
			Global.gui().refresh();
			Global.gui().clearCommand();
			Global.gui().showNone();
			if(auto.checkScenes()){
				return;
			}
			for(Activity act: activities){
				if(act.known()&&act.time()+time<=22){
					Global.gui().addActivity(act);
				}
			}
		}
		else{
			for(Character npc: Global.everyone()){
				if(!npc.human()){
					if(npc.getLevel()>=10*(npc.getRank()+1)){
						npc.rankup();
					}
					((NPC)npc).daytime(NPCDAYTIME);
				}
			}
			//Global.gui().nextMatch();
			if(Global.checkFlag(Flag.autosave)){
				Global.save(true);
			}
			if(Global.getDayString(Global.getDate()).startsWith("Sunday")){
				new NightOff(player);
			}else{
				new Prematch(player);
			}
		}
	}
	public void buildActivities(){
		activities = new ArrayList<Activity>();
		activities.add(new Exercise(player));
		activities.add(new Porn(player));
		activities.add(new VideoGames(player));
		activities.add(new Informant(player));
		activities.add(new BlackMarket(player));
		activities.add(new XxxStore(player));
		activities.add(new HWStore(player));
		activities.add(new Bookstore(player));
		activities.add(new Dojo(player));
		activities.add(new AngelTime(player));
		activities.add(new CassieTime(player));
		activities.add(new JewelTime(player));
		activities.add(new MaraTime(player));
		if(Global.checkFlag(Flag.Kat)){
			activities.add(new KatTime(player));
		}
		activities.add(new Closet(player));
		activities.add(new ClothingStore(player));
		if(Global.checkFlag(Flag.Reyka)){
			activities.add(new ReykaTime(player));
		}
		activities.add(new MagicTraining(player));
		activities.add(new Workshop(player));
		activities.add(new YuiTime(player));
	}
	public void setTime(int t){
		time = t;
	}
	public int getTime(){
		return time;
	}
	public void advance(int t){
		time+=t;
	}
	public static void train(Character one, Character two, Attribute att){
		int a;
		int b;
		if(one.getPure(att)>two.getPure(att)){
			a=100-(2*one.get(Attribute.Perception));
			b=90-(2*two.get(Attribute.Perception));
		}
		else if(one.getPure(att)<two.getPure(att)){
			a=90-(2*one.get(Attribute.Perception));
			b=100-(2*two.get(Attribute.Perception));
		}
		else{
			a=100-(2*one.get(Attribute.Perception));
			b=100-(2*two.get(Attribute.Perception));
		}
		if(Global.random(100)>=a){
			one.mod(att, 1);
			if(one.human()){
				Global.gui().message("<b>Your "+att+" has improved.</b>");
			}
		}
	}
	public void visit(String name, Character npc, int budget){
		for(Activity a:activities){
			if(a.toString().equalsIgnoreCase(name)){
				a.shop( npc, budget );
				break;
			}
		}
	}
	public String displayTime(){
		if(time<12){
			return time+":00am";
		}else if(time==12){
			return "noon";
		}
		else{
			return (time-12)+":00pm";
		}
	}
}
