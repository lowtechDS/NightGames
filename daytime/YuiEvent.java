package daytime;

import java.util.ArrayList;
import java.util.HashMap;

import characters.Character;
import characters.Dummy;
import characters.Emotion;
import characters.NPC;
import characters.Yui;
import global.Flag;
import global.Global;
import gui.KeyableButton;
import gui.SceneButton;

public class YuiEvent implements Scene {
	private int scene;
	private Character player;
	
	public YuiEvent(Character player){
		this.player = player;
		scene = 1;
	}

	@Override
	public void respond(String response) {
		String message = "";
		ArrayList<KeyableButton> choice = new ArrayList<KeyableButton>();
		if(response.startsWith("Take her virginity")){
			play("VirginitySex");
		}else if(response.startsWith("Let her service you")){
			play("VirginityHandjob");
		}else if(response.startsWith("Focus on her")){
			play("VirginityOral");
		}
	}

	@Override
	public boolean play(String response) {
		Dummy sprite = new Dummy("Yui",1,false);
		sprite.setBlush(2);
		sprite.setMood(Emotion.confident);
		String message = "";
		ArrayList<KeyableButton> choice = new ArrayList<KeyableButton>();
		if(response.startsWith("VirginityUndressing")){			
			Global.gui().loadPortrait(player, sprite);
			message = "You walk back to your room with Yui. You're still holding hands, but the trip has been heavy with awkward silence. "
				+ "She did agree to give you her virginity and seemed quite pleased with the idea, but she would probably agree to anything you asked "
				+ "to preserve her family's honor. You're 99% sure this is what she genuinely wants, but that shadow of a doubt weighs on you.<p>"
				+ "When you reach your room and unlock the door with your free hand, Yui gives your held hand a noticeable squeeze, looking for "
				+ "reassurance. You scold yourself for brooding over your lingering doubts. She has a much more legitimate reason to feel nervous "
				+ "right now. You should be focusing on making sure Yui feels comfortable about losing her virginity.<p>"
				+ "You pull her in for a gentle kiss as you both enter your room, nudging the door closed with your foot. When you break the kiss, "
				+ "you notice her glance at your bed expectantly. You gradually lead her in that direction, though your progress is impeded as she "
				+ "starts to kiss you with increased passion. Her tongue is remarkably talented. Is this also something she trained? You tangle your "
				+ "tongue with hers as your hands make quick work undressing her.<p>"
				+ "You stumble as you back into your bed, breaking the kiss as you lose your balance. You have to compliment her kissing skill. That's "
				+ "at least on a competitive level. She smiles, pleased at the praise, then gradually reddens as realization dawns on her.<p>"
				+ "<i>\"M-Master, when did you undress me? I didn't even notice.\"</i> She covers herself with both hands, blushing deeply. "
				+ "You chuckle at her embarrassed reaction and point out that undressing others is an important skill in the Games. She "
				+ "listens to your explanation and nods earnestly.<p>"
				+ "<i>\"I'm pretty dexterous too. I'll have you undressed in the blink of an eye.\"</i> "
				+ "She starts to reach for your clothing, but hesitates to expose herself. <i>\"Um... Master? C-can you close your eyes for a bit? I'll "
				+ "be OK when you're naked too, but right now this is really embarrassing.\"</i><p>"
				+ "You resist the urge to tease her more and instead close your eyes. Undressing her and challenging her to do the same seems to "
				+ "have helped break some of her tension, but continuing to push her could have the opposite effect.<p>"
				+ "You hear and feel her moving around near you, but if she's actually undressing you, she must have a really delicate touch. As you "
				+ "shift in place, you notice you can't feel your clothing against your skin. It seems like she's doing a good job.<p>"
				+ "<i>\"All done. You can look now.\"</i><p>"
				+ "Sure enough, when you open your eyes, you see that Yui has not just undressed you, but is wearing your clothes. They're too big for "
				+ "her, but it's a pretty cute look. She smiles at you, looking for approval. You compliment her for her impressive clothing removal skills. "
				+ "It was one of the trickier elements of the Games when you started. She accepts your praise happily, though she can't keep her curious "
				+ "gaze from drifting down your body.<p>"
				+ "<i>\"Master, like I told that girl, I've never touched a real penis, but I've been well-trained.\"</i> She stares at your exposed dick "
				+ "with a sort of excited nervousness. <i>\"Shall I show you what I can do?\"</i>";
			choice.add(new SceneButton("Let her service you"));
			choice.add(new SceneButton("Focus on her"));
			Global.gui().prompt(message, choice);
			return true;
		}else if(response.startsWith("VirginityHandjob")){
			sprite.setBlush(3);
			sprite.setMood(Emotion.dominant);
			Global.gui().loadPortrait(player, sprite);
			message = "You sit on the side of your bed, leaving your legs comfortably open. You are certainly enjoying Yui's company, but with just a little "
					+ "kissing and undressing, you're only about half-mast. You suggest she start with a striptease to get you in the mood.<p>"
					+ "Yui looks a bit shy for a moment, but she had to know she wasn't going to stay dressed very long. After some consideration, she "
					+ "begins a sensual dance as she slowly moves closer to you. The ill-fitting clothing should hinder to such an erotic performance, "
					+ "but she manages to make it work in her favor by letting you get glimpses of her nipples and bush through the gaps in the loose "
					+ "garments. As she peels off the shirt, she leans close to you, so her shapely breasts are exposed mere inches from your face.<p>"
					+ "To your surprise, rather than remove each garment individually, Yui slides the loose jeans and boxers off in a single motion. Almost "
					+ "before you can process her sudden nudity, her soft bare butt presses against your erection. Her strip tease turned out to be more of "
					+ "a lap dance, but she got you plenty excited.<p>"
					+ "<i>\"Did you enjoy the show, Master?\"</i> Yui turns on your lap to face you with a flushed, giddy smile. She seems to be getting "
					+ "more comfortable being naked. <i>\"I'm fine with any amount of embarrassment, if it'll please my Master.\"</i> She slides down to the "
					+ "floor until she's eye level with your penis. <i>\"May I please you more, Master?\"</i><p>"
					+ "She doesn't wait for your reply, eagerly grabbing your shaft with both hands and beginning to stroke it. Her skill makes it immediately "
					+ "clear that she wasn't lying about her training. She's at least as good as any of the girls you've faced in the Games. Her hands coil around "
					+ "your dick, hitting all your sensitive areas while maintaining a steady rhythm. She really learned how to do with by practicing on a dildo?<p>"
					+ "Yui's face practically glows at your praise. <i>\"The size and shape are similar to my training models, but even my best toys can't "
					+ "replicate this heat and responsiveness. The real thing is infinitely more erotic to hold!\"</i> Her hands give your cock a tender caress "
					+ "before moving down to grasp your balls.<br>"
					+ "<i>\"And these are unlike anything I've ever felt. One of the training dummies had testicles, but "
					+ "the texture wasn't even close.\"</i><br>"
					+ "You groan softly as she gets a little carried away exploring your genitals. You warn her to be careful with the bits she's playing with. "
					+ "Those are very sensitive.<p>"
					+ "Yui gives you an almost pitying look. It's probably the closest she's ever gotten to showing you disrespect.<br>"
					+ "<i>\"Do you honestly think there's a female martial artist on Earth that doesn't know a man's testicles are his weakness? Even us innocent virgins "
					+ "know that.\"</i> Her fingers delicately probe your sack, isolating each nut before releasing them. <i>\"Sorry Master, but when we're fighting "
					+ "for real, I'll probably end up targeting these from time to time. I'd much rather hold them gently and give you pleasure. In fact, if you're "
					+ "ready to ejaculate, shall I show you the Ishida Kunoichi 2nd hidden art?\"</i><p>"
					+ "Despite her impressive handjob technique, you aren't really that close to cumming. She's spent too much time indulging her curiosity, and "
					+ "your endurance is also pretty good. Still, she's welcome to try.<p>"
					+ "Yui carefully holds your jewels in both hands while making a triangle with her fingers. She mutters a short Japanese incantation under her "
					+ "breath and makes a series of rapid hand gestures that somehow avoids pinching or squeezing your tender bits. It takes about ten seconds total, "
					+ "then she looks up at you nervously.<p>"
					+ "<i>\"Is it working? This is the first time I've tried it on a real person. Do you feel any different?\"</i> Before she's finished asking the "
					+ "question, you start to feel an urgent heat building up in your groin. An intense need to cum is starting to build up, much worse than even "
					+ "a direct hit from an aphrodisiac flask. How did she do that?!<p>"
					+ "<i>\"This is an old technique passed down by the Ishida women. By stimulating a series of pressure points in the correct order, it temporarily "
					+ "increases sperm production and releases testosterone. The man's body responds by trying to ejaculate quickly. Female assassins would use it to "
					+ "'finish off' a target quickly before finishing him off for good.\"</i> She grabs your dick and starts stroking. \"<i>But I'd rather use it "
					+ "to make you feel good, Master.\"</i><p>"
					+ "The pressure building up in your cock is almost enough to drive you mad. Between that and Yui's skillful stroking, you're pushed to climax "
					+ "in a handful of seconds. Your hips buck involuntarily as you erupt like a geyser. Thick ropes of semen hit Yui directly in the face, much "
					+ "to her surprise. She starts to giggle as you struggle to catch your breath.<br>";
			message +="<i>\"My toys don't do that. I knew that would happen but completely forgot to react.\"</i><p>"
					+ "She's so giddy about what she's done that you end up recovering first. You grab a couple tissues and help her clean the jizz off her face and "
					+ "hair. When you're done, you guide her gently onto the bed and lay her down. If she's still nervous, she doesn't let it show. She smiles "
					+ "adoringly as you caress her wet pussy and find it ready for penetration.<p>"
					+ "You're surprisingly ready too. You expected to need some recovery time after her handjob, but whatever Yui did to you has kept you hard even after "
					+ "your recent ejaculation. A seemingly insatiable primal urge wants you to take her immediately, and judging by her expression, she wants the same thing.";
			choice.add(new SceneButton("Take her virginity"));
			Global.gui().prompt(message, choice);
			Global.gui().displayImage("premium/Yui BJ.jpg", "Art by AimlessArt");
			return true;
		}else if(response.startsWith("VirginityOral")){
			sprite.setBlush(3);
			sprite.setMood(Emotion.desperate);
			Global.gui().loadPortrait(player, sprite);
			message = "Yui makes a tempting offer, but tonight should be all about her. She'll have plenty of chances to impress you in the near future.<p>"
					+ "You guide her onto the bed and start to undress her a second time. This is pretty simple to do, because your clothes are quite loose on her. "
					+ "She doesn't complain or try to stop you, but you can tell she's getting nervous again. You decide to distract her with some questions.<p>"
					+ "If she's never seen a penis before, does she have any experience with boys? She blushes at the question, but smiles slightly.<br>"
					+ "<i>\"Well, you know I'm a virgin, but I have kissed boys before.\"</i><br>"
					+ "So you didn't steal her first kiss. There's a wide range between kissing and sex. As you slip off the last of her (really your) clothing, "
					+ "you ask if you're the first guy to see her naked. She's bright red, but doesn't cover herself.<br>"
					+ "<i>\"Technically no. I had a bad swimsuit malfunction years ago. But you are the first one to see me naked romantically.\"</i><br>"
					+ "You trail your fingers through her thin, blonde pubic hair. Clearly she is a natural blonde. You gently caress her mons, avoiding her "
					+ "more sensitive lower lips. Has she ever been touched down here?"
					+ "<i>\"Ah... I touch myself there multiple times a week. It's part of my training.\"</i><p>"
					+ "Yui's breathing starts to grow heavier and more heated as you skillfully tease her vulva. Her legs open involuntarily as her hips rise "
					+ "slightly to seek your hand. Whatever embarrassment she had about being naked has been forgotten in the pleasure you're giving her. "
					+ "Touching herself is very different than being touched by someone else.<br>"
					+ "<i>\"Yeah...AH!\"</i> She seems to momentarily lose the capacity to speak when you insert a finger into her. <i>\"This is-MMM! This is "
					+ "new... AH! God! Already!? Master, I'm already about to cum!\"</i> She desperately pulls you close as her body trembles. You lean down "
					+ "to capture her soft lips and slip your tongue into her mouth. Moments later, you can feel her shudder as her orgasm shoots through her.<p>"
					+ "After Yui goes limp and you break the kiss, she hides her face in shame. <i>\"That was so fast! My instructor would be furious if she knew "
					+ "I forgot all my training as soon as I was touched!\"</i><br>"
					+ "You console her as best you can. Most girls don't have their first time with a professional sex-fighter. Still, she'll need to improve her "
					+ "endurance if she's going to compete tomorrow night. She moves her hands away from her face and you see a hopeful smile form.<p>"
					+ "<i>\"That's not a lot of time, Master. If you're willing to help me study, I don't mind pulling an all-nighter tonight.\"</i><br>"
					+ "You give her another gentle kiss before slowly moving down her naked body. If this is going to be an intense cram session, you should move "
					+ "on to something more advanced. Her eyes go wide as she realizes your intention.<p>"
					+ "<i>\"Master, you aren't thinking of licking me there, are you?! You don't need to go to such lengths for your humble ninja! For Master to "
					+ "put your mouth on such a place-AH!\"</i><br>"
					+ "Her protests devolve into moans at the first lick. You lap at her love bud for a couple seconds to make her shiver, before you start exploring "
					+ "her labia with long slow licks. Her legs spread wider and her hips rise slightly. Even if she's self-conscious, her body sure wants it.<p>"
					+ "You keep licking her slowly until she starts to regain her composure, and suddenly stick your tongue deep inside her. She jumps at the surprising "
					+ "sensation. This new pleasure throws her into complete disarray, and you're able to continue to overwhelm her until she orgasms again.<p>"
					+ "This climax hits her so suddenly that she can't even manage to speak for almost a minutes. Finally she manages to whimper out a complaint.<br>"
					+ "<i>\"That's not fair, Master. How am I supposed to compete when you can do that? Even if we practiced all night, I would never get used to it.\"</i><br>"
					+ "She might have some trouble then, because the other competitor's also have talented tongues. Yui shakes her head dismissively.<p>"
					+ "<i>\"I could handle someone else doing it. Know it's Master's tongue and Master's fingers is what makes the pleasure irresistible. I'll go crazy "
					+ "before I get used to you!\"</i><p>"
					+ "Your pride swells at her adoration. There's only one way to find out how whether she can get used to it. You give her another lick across the "
					+ "vulva. She yelps and pushes your head away.<br>"
					+ "<i>\"Master, you can't keep making me cum with foreplay! If I'm going to go crazy, I want to feel Master's penis first!\"</i><p>"
					+ "That's a valid point. Lilly specifically asked you to make sure Yui isn't a virgin tomorrow night.";
			choice.add(new SceneButton("Take her virginity"));
			Global.gui().prompt(message, choice);
			return true;
		}else if(response.startsWith("VirginitySex")){
			sprite.setBlush(3);
			Global.gui().loadPortrait(player, sprite);
			message = "You position yourself over Yui, while she wraps her arms lovingly around your neck. Part of her must be nervous considering what is about to "
					+ "happen, but the smile she gives you betrays none of it. You lean down and kiss her softly. She jumps slightly in surprise and you realize your "
					+ "movement cause the head of your dick to touch her entrance.<p>"
					+ "You brush her hair away from her face and confirm again whether she's ready for this. She raises her head to kiss you back before answering.<br>"
					+ "<i>\"I've never been more certain of anything, Master. I may have done all this crazy training out of family tradition, but now I'm happy I did. It means "
					+ "I can experience my first time with the boy I like without any fear.\"</i><p>"
					+ "Her sincerity dispels any remaining doubts you may have had about her intentions. Clearly she isn't doing this out of loyalty or obligation. There's "
					+ "just one thing you want to hear before you take her virginity.<p>"
					+ "Yui blushes and looks away coyly. <i>\"Ok, I get it. I want your dick inside me, Master.\"</i><p>"
					+ "Well, that was hot, but you weren't going to ask for pillow talk. She's been calling you 'Master' all night. You want her to call you by your "
					+ "name before you have sex.<p>"
					+ "Her smile widen as she looks at you with wet eyes. <i>\""+player.name()+", please make me a woman.\"</i><p>"
					+ "You thrust your hips and penetrate Yui smoothly. She's extremely wet, but also quite tight. A quiet moan escapes her, and she pulls you close. Her "
					+ "expression doesn't show any traces of pain, only pleasure. Reassured, you slowly start moving your hips in and out of her.<p>"
					+ "As you begin to find your rhythm, you feel Yui's pussy squeeze you at random intervals. You assume it's a reflex until you notice her whispering to "
					+ "herself between moans.<br>"
					+ "<i>\"Second charkAH- Then first -AH, no third while pulling out...? MMM... Can't -AH- remember...\"</i><p>"
					+ "You slow your thrusting to ask Yui what she's doing, but she's barely able to put together a coherent answer.<p>"
					+ "<i>\"Third hidde-AH... Sex technique, but feels-Ah... Too good! Can't think strAH!\"</i><br>"
					+ "You capture her mouth with your to silence her before thrusting deep inside. She moans passionately against your lips before you pull away. You firmly, "
					+ "but not unkindly insist that she just relax and enjoy her first time. She can practice her techniques another time. You wait for her to nod, "
					+ "then give her another deep kiss. That should help keep her occupied.<p>"
					+ "Pretty soon, you can feel Yui's body begin to tense up, so you quicken your pace. You do your best to match the timing of her approaching orgasm with your "
					+ "own. When she starts to shudder in the first spasms of orgasm, her pussy squeezes you tight enough to help you finish. You shoot your load inside her "
					+ "and collapse next to her on the bed, still embracing.<p>"
					+ "You indulge in the afterglow for a while while she leans on your chest. Eventually, you ask her if she feels ready for her first match. She nods, but "
					+ "you start to notice a wetness on your chest.<br>"
					+ "Yui looks up at you with tears leaking from her eyes and an innocent smile on her face.<p>"
					+ "<i>\""+player.name()+", I'm so happy you were my first. I'm so happy I chose you as my Master. I'm looking forward to playing with those cute girls "
					+ "tomorrow, but if one of them was my first, I never would have experienced this happiness.\"</i>";
			Global.gui().clearText();
			Global.gui().message(message);
			if(Global.checkFlag(Flag.autosave)){
				Global.save(true);
			}
			Global.flag(Flag.Yui);
			Global.newChallenger(new Yui());
			Global.getNPC("Yui").gainAffection(Global.getPlayer(), Math.round(Global.getValue(Flag.YuiAffection)));
			Global.gui().message("<b>You gained affection with Yui.</b>");
			Global.gui().endMatch();
			Global.modCounter(Flag.YuiAffection, 0);
			return true;
		}
		return false;
	}

	@Override
	public String morning() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String mandatory() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		// TODO Auto-generated method stub
		
	}

}
