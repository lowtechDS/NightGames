package daytime;

import items.Clothing;
import items.Item;
import items.Toy;

import global.Flag;
import global.Global;

import java.util.ArrayList;

import characters.Attribute;
import characters.Character;
import characters.Dummy;
import characters.Emotion;

public class Workshop extends Activity {
	private boolean acted;
	
	public Workshop(Character player) {
		super("Workshop", player);
		acted = false;
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.workshop);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(!Global.checkFlag(Flag.metJett)){
			Global.gui().message("You head to Jett's workshop. Apparently he has an implicit claim on this workshop in the same way that Mara does on her computer room. " +
					"When you enter the room, he's busy machining an unidentifiable metal component. When he finishes the part, he walks over to greet you, but doesn't " +
					"bother removing his safety glasses. He's a bit short and skinny. You can't help thinking of him as a stereotypical science nerd.<p>"
					+ "<i>\"You're "+player.name()+"? " +
					"You don't need to nod, that was a rhetorical question. I've seen your fights.\"</i> His tone is not quite hostile, but he's clearly not interested in making " +
					"friends. <i>\"If you're looking to upgrade your toys, I'm your man. It's not cheap, but my work is well worth the money.\"</i><p>"
					+ "Aesop made it sound like Jett " +
					"could make some more versatile equipment. What all is for sale?<p>"
					+ "Jett gives you a disdainful look. <i>\"Nothing. I'm not selling anything potentially dangerous " +
					"to someone who doesn't know how to maintain and operate it safely. I'm not risking you sodding up one of my inventions in a way that gets a girl hurt. What " +
					"I am willing to do is teach you how I put my equipment together. If you're a quick study and can understand how my toys work, I'll help you build your own.\"<p>"
					+ "\"Oh, " +
					"and I'm not helping you for free. No offence, but I'm rooting for your opponents. Male solidarity is fine and well, but they're a lot more attractive than you.\"</i>");
			Global.flag(Flag.metJett);
			acted=true;
			Global.gui().choose(this,"Lecture: $"+(player.getAdvancedTrainingCost()));			
		}
		else if(choice=="Start"){
			Global.gui().message("You head to Jett's workshop. He sets down the parts he was working on and turns to face you. <i>\"You need something? I hope you brought your " +
					"wallet.\"</i>");
			Global.gui().choose(this,"Lecture: $"+(player.getAdvancedTrainingCost()));
			acted = false;
		}
		else if(choice=="Upgrade Dildo: $2000"){
			if(player.money>=Toy.Dildo2.getPrice()){
				Global.gui().message("You hand over the dildo and ask Jett how he plans to upgrade it. <i>\"If I hollow out a small space inside it, there will be enough " +
						"room for a compact oscillating motor.\"</i> That's it? He's just going to put a motor in it? You could have just bought a vibrating dildo at the sex " +
						"shop.<p>"
						+ "<i>\"No. Nonono... no, you don't want to do that. That's a classic beginner's mistake. Crude store bought vibrators just overstimulate nerve " +
						"endings and cause numbness. This magnetic motor will allow me to tune the vibration to the ideal frequency to maximize pleasure without reducing " +
						"sensation over time. It's not cheap, but you wouldn't be coming to me if you weren't willing to shell out a bit.\"</i><p>"
						+ "After a few minutes, he sets " +
						"the dildo on the table in front of you. <i>\"It's on, try touching it.\"</i> What does he mean it's on? The dildo is completely silent and motionless. You " +
						"pick up the dildo, but immediately drop it in surprise. It's vibrating, but that doesn't feel like vibration you've ever experienced, it's almost " +
						"like a current running through your arm. Jett clearly knows what he's doing. You're not going to question his pricing again.");
				player.money-=Toy.Dildo2.getPrice();
				player.remove(Toy.Dildo, 1);
				player.gain(Toy.Dildo2);
				acted=true;
			}
			else{
				Global.gui().message("You can't afford that upgrade");
			}
		}
		else if(choice=="Upgrade Tickler: $3000"){
			if(player.money>=Toy.Tickler2.getPrice()){
				Global.gui().message("Jett rummages around in the back of his workshop for awhile before returning with a small box of feathers. <i>\"Arm,\"</i> he demands bluntly. " +
						"You extend your arm and he touches it with one of the feathers. It feels like a normal feather, but goosebumps appear where it touched. Your skin feels " +
						"more sensitive where it touched. <i>\"The feathers come from a bird which secretes an oil that can increase sensitivity in primates. There's no evolutionary " +
						"reason for this, it's just a quirk of nature.\"</i> He inserts a few of these into your tickler and returns it. <i>\"That's sufficient. I once made a tickler " +
						"entirely out of these. It wasn't a good idea.\"</i>");
				player.money-=Toy.Tickler2.getPrice();
				player.remove(Toy.Tickler, 1);
				player.gain(Toy.Tickler2);
				acted=true;
			}
			else{
				Global.gui().message("You can't afford that upgrade");
			}
		}
		else if(choice=="Upgrade Riding Crop: $1500"){
			if(player.money>=Toy.Crop2.getPrice()){
				Global.gui().message("Jett works on the riding crop for a few minutes and when he returns it, there's a short length of cord attached to the end. You feel the " +
						"cord. It's flexible and softer than leather, but still has some resilience. <i>\"I call it the 'Treasure Hunter.' It's a painful, yet elegant attachment that's " +
						"the right length and size to hit extremely sensitive areas. It's effective against both male and female anatomy. Simple enough to accomplish, but the real " +
						"trick was finding the ideal material. My solution is quite painful on impact, but has no risk of breaking the skin or leaving unpleasant welts.\"</i> You take " +
						"the crop with great care. It suddenly feels quite dangerous.");
				player.money-=Toy.Crop2.getPrice();
				player.remove(Toy.Crop, 1);
				player.gain(Toy.Crop2);
				acted=true;
			}
			else{
				Global.gui().message("You can't afford that upgrade");
			}
		}
		else if(choice=="Upgrade Onahole: $3000"){
			if(player.money>=Toy.Onahole2.getPrice()){
				Global.gui().message("Jett reluctantly takes the Onahole from you. <i>\"You wash this, right?\"</i> He takes it to the back room and is gone for a while. He returns " +
						"and hands you the cock sleeve which is not visibly different. <i>\"Put your finger inside.\"</i> It's hot and wet, almost indistinguishable from the real thing. " +
						"<i>\"It's self lubricating and maintains a temperature slightly above a normal human body.\"</i> Quite impressive, and there's no obvious heating mechanism or " +
						"liquid supply. How does it work? <i>\"Trade secret. Sorry mate, but until the patent is finalized, I can't reveal the magic trick.\"</i>");
				player.money-=Toy.Onahole2.getPrice();
				player.remove(Toy.Onahole, 1);
				player.gain(Toy.Onahole2);
				acted=true;
			}
			else{
				Global.gui().message("You can't afford that upgrade");
			}
		}
		else if(choice=="Leave"){
			done(acted);
			return;
		}
		else if(choice.startsWith("Lecture")){
			Dummy mara = new Dummy("Mara");
			mara.setBlush(0);
			mara.dress();
			mara.setMood(Emotion.confident);
			if(player.money>=player.getAdvancedTrainingCost()){
				player.money-=player.getAdvancedTrainingCost();
				player.mod(Attribute.Science, 1);
				if(!player.has(Toy.ShockGlove)){
					player.gain(Toy.ShockGlove);
				}
				if(!player.has(Clothing.labcoat)){
					player.gain(Clothing.labcoat);
				}
				if(player.getPure(Attribute.Science)>=6&&!player.has(Toy.Aersolizer)){
					player.gain(Toy.Aersolizer);
				}
				acted=true;
				clearRandomScenes();
				addRandomScene("Basic",1);
				String scene = "Basic";

				if(player.getPure(Attribute.Science)>3){
					addRandomScene("Slime",2);
				}
				if(player.getPure(Attribute.Science)>6){
					addRandomScene("Aerosolizer",2);
				}
				if(player.getPure(Attribute.Science)>12){
					addRandomScene("Shrink Ray",2);
				}

				if(player.getPure(Attribute.Science)==3){
					scene = "Slime";
				}else if(player.getPure(Attribute.Science)==6){
					scene = "Aerosolizer";
				}else if(player.getPure(Attribute.Science)==12){
					scene = "Shrink Ray";
				}else{
					scene = getRandomScene();
				}

				if(scene.startsWith("Aerosolizer")){
					Global.gui().loadPortrait(player, mara);
					Global.gui().message("There's no lecture this time. Jett has decided you're finally ready to build your own multitool. Mara is also hanging out in the "
							+ "workshop today. She claims to be doing maintenance on her own equipment, but she's spent almost the entire time assisting you.<p>"
							+ "The multitool seems needlessly complicated, considering it basically just combines your existing shock glove with an aerosolizer that "
							+ "is little more than an industrial sprayer. Despite the simple functionality, Jett forces you to include a significant amount of unused space "
							+ "and control inputs that don't connect to anything.<p>"
							+ "<i>\"You'll be adding to the multitool over time. There is a lot of functionality you'll want in the future, but you aren't ready for it "
							+ "yet.\"</i> That's all the explanation Jett would give you. That would be fine if you had some proper blueprints to work from, but he only gave "
							+ "you vague requirements and forced you to design the thing yourself. You've already had to throw out a half-dozen designs because they won't fit "
							+ "some component you've never even heard of.<p>"
							+ "You vent some of your frustration to Mara, since Jett has started ignoring you. You point out that it would make much more sense for Jett to "
							+ "come up with a standardized frame and then slot in additional modules over time. Mara immediately takes exception with this idea.<p>"
							+ "<i>\"You can't just standardize it! Each multitool reflects the individual traits of the owner. Designing and building it is like a rite of "
							+ "passage. It's just like how a jedi always constructs her own lightsaber. You can't just pick one off an assembly line.\"</i><p>"
							+ "Mara's romantic notions are cute, but she's confusing science fiction with science. Replaceable parts and modular design are the standard for "
							+ "a reason. No respectable engineer would replace basic manufacturing theory with a design philosophy from a 40 year old sci-fi movie.<p>"
							+ "Mara goes very quiet while looking past you. You notice Jett is trying very hard to look busy and avoid eye contact. It may be your imagination, "
							+ "but his face looks a little red. Mara leans close to you and whispers in your ear.<p>"
							+ "<i>\"I asked Jett the same thing when I was making my multitool. I was just repeating the same explanation he gave me.\"</i><p>"
							+ "Jett finally looks in your direction, more than a little flustered. <i>\"If you two have time to fuck about, you better have that bloody multitool "
							+ "finished soon!\"</i>");
				}else if(scene.startsWith("Slime")){
					Global.gui().message("Several minutes into Jett's lecture on fluid dynamics, you decide to interrupt him. His lessons are sure to be helpful in the long "
							+ "run, but you need to be winning fights now to keep paying his fees. Doesn't he have anything you can apply right away?<p>"
							+ "Jett considers this silently for a long while. You start to worry that he may throw you out for your impatience, but eventually, "
							+ "he gives you a sympathetic nod.<p>"
							+ "<i>\"You need some help in the short term so you can keep up. I remember feeling that way when I started out.\"</i> He unlocks "
							+ "a door in the back of his lab. Presumably a storage closet. <i>\"Nothing wrong with getting a little assistance. Follow me.\"</i><p>"
							+ "The storage closet turns out to be bigger than you expected. You attention is immediately drawn to the large aquarium in the corner. Inside the "
							+ "aquarium is a wriggling blue blob of viscous fluid. When Jett taps on the glass, the slime forms into a crude approximation of a girl. "
							+ "It's actually kind of cute.<p>"
							+ "<i>\"I would introduce you properly, but I don't know her name, if she even has one. Before you ask, no, she isn't trapped in the tank. "
							+ "I just store the fluid medium here so she doesn't make a mess. She is a living waveform, covering the entire campus and completely "
							+ "intangible. I don't know where she comes from, but we wouldn't even know she exists if I hadn't been calibrating my RF receiver when she was feeling playful.\"</i><p>"
							+ "His explanation lost you along the way. If she's some kind of invisible wave-thing, why does she look like a blue slime monster?<p>"
							+ "<i>\"The blue liquid is her medium. I developed a substance that she can freely manipulate. She likes to manifest physically to "
							+ "seek out affection and sexual contact. She even seems to orgasm with enough stimulation, but it makes her lose control of her nearby "
							+ "slime.\"</i><p>"
							+ "The slime girl stares at you intently and blows you a kiss. <i>\"I think she likes you. She may have even been watching your earlier "
							+ "matches. I bet she'll help you out during the Games if you give her a chance. I'll provide you with some of the fluid medium. You "
							+ "just need to charge it with your glove before she'll be able to take control. Her body is highly corrosive to synthetic materials, "
							+ "but harmless on skin. She likes to go after sensitive areas, and she obviously provides her own lube.\"</i><p>"
							+ "Sounds like you have a slimy new ally. Hopefully this will give you the edge you need.");
				}else if(scene.startsWith("Shrink Ray")){
					Global.gui().message("You have returned to Jett's lab and are once again handed the task to build a device.  This time Jett tells you to "
							+ "build a Shrink Ray with some vague specifics and then goes back to his own project.  Mara is there working at something on her "
							+ "own in the corner but seems to be ignoring you, so you assume you are on your own.  You head over to a workbench and get started "
							+ "with what you think is the correct combination of parts.<p>"
							+ "After the fifth time you let out a yelp as the device shocks your fingers, you realize that there is a presence behind you.  "
							+ "You turn around and Mara is there trying to look around your larger frame.<p>"
							+ "<i>\"Your leads are reversed,\"</i> she says without waiting for your question, <i>\"you are sending power the wrong way and "
							+ "overloading it.\"</i><p>"
							+ "You reverse the leads and solder them back down.  You turn it on, resulting in another, bigger shock and smoke.  "
							+ "She lets out an exasperated sigh<p>"
							+ "<i>\"Your soldering is a mess, you bridged the connections over here.  Just gimme it.\"</i><p>"
							+ "She yanks the whole thing out of your hands and tries to push you out of the way so she can get to the bench.  "
							+ "You just step out of the way knowing if you tried to stop her it wouldn't end well.  She makes some quick changes and "
							+ "then turns around, with the device in hand.<p>"
							+ "<i>\"Now it will work, see!\"</i><p>"
							+ "She points the device at you and pulls the trigger.  There is a flash of light and when your eyes clear you can't quite "
							+ "tell what you are looking at.  You feel a pat on your head.<p>"
							+ "<i>\"Aww, you're so cute.  But my eyes are up here, mister.\"</i><p>"
							+ "Your senses clear and you realize that what you are seeing in front of you are the small mounds of Mara's breasts.  You "
							+ "look up and see her smiling face, and it dawns on you what happened, she shrunk you!  You try to grab for the shrink ray "
							+ "in her hand but she anticipates your intention and holds it up above her head.  You jump a few times, but don't manage "
							+ "to get close.  She smiles down at you smugly.<p>"
							+ "<i>\"Come on little boy, you can do better than that.\"</i><p>"
							+ "You decide to change tactics, you reach out and begin to tickle Mara's sides.  She begins to laugh but quickly places "
							+ "her hand on your forehead and pushes you away with almost no effort.  You step back for a minute, and then take a "
							+ "running start at her.  She steps out of the way and slaps your ass as you barrel past her, stopping just short of the "
							+ "work bench.  You turn around to see her holding the shrink ray in the air with one hand.  With the other hand she waves "
							+ "her index finger back and forth, taunting you.<p>"
							+ "You lock eyes with her and wait for a minute before charging again.  However, this time she doesn't move and just pushes "
							+ "you backwards before you even get close.  You flail wildly as you end up completely off balance.  You fall backwards "
							+ "against the workbench, knocking some parts to the floor with a loud clatter.<p>"
							+ "<i>\"HEY!\"</i> Jett yells angrily<p>"
							+ "You ignore him though as you focus on Mara, plotting your next attempt.  You take a couple steps forward and then a few "
							+ "side to side, almost stalking her like a wild animal.  She watches you warily, making no move of her own.   Suddenly "
							+ "you see her eyes dart to something behind you and knowing she is distracted you lunge.  At the last minute she sees you "
							+ "coming but it is too late as your shoulder runs into her hip as you attempt a full on tackle.  But she only steps "
							+ "backwards before catching her balance.  She plants her back foot and suddenly you feel her arm wrap around your back. "
							+ "She pivots and pulls you around, throwing you over her hip and depositing you on the floor on your back.<p>"
							+ "Before you can recover she is on top of you, pinning your arms to the floor over your head with one hand.  Her other "
							+ "hand reaches down and starts to work its way inside your pants.  You feel her hand wrap around your flaccid penis and "
							+ "balls as she lets out a triumphant laugh.<p>"
							+ "<i>\"It feels so tiny, it fits so nicely in my hand.\"</i><p>"
							+ "You feel your face fall as you realize that she has gotten the best of you and there is nothing you can do.<p>"
							+ "<i>\"Cut it out, you two!\"</i> Jett pipes up angrily from his bench. <i>\"If you are going to fuck about, get out of "
							+ "my lab!  Otherwise clean up this mess and get back to work.\"</i><p>"
							+ "Mara pulls her hand out of your pants and gets up.  She gives you a hand and helps you up.  As you stand, she bends "
							+ "over and kisses your forehead.<p>"
							+ "<i>\"Don't worry, the shrinking effect will wear off . . . I think.\"</i> She winks as she says the last part and "
							+ "then walks off.");
				}else{
					Global.gui().message("They say that geniuses make poor teachers, but Jett disproves that theory. He explains the principles behind his work in a way that you " +
							"can easily follow. Despite his unfriendly demeanor, he answers any questions you have without complaint. After about an hour of intense lecture, you " +
							"feel like you've gotten the benefits of a week of classes.<p><i>\"Some of this equipment is likely to consume battery power rapidly. If you need to " +
							"recharge during a match, there are a few compatible charging stations in the Mechanical Engineering workshops.\"</i>");
				}

			}
			else{
				Global.gui().message("You don't have enough money for Jett's lecture.");
			}			
			acted=true;
		}
		if(player.has(Toy.Dildo)){
			Global.gui().choose(this,"Upgrade Dildo: $2000");
		}
		if(player.has(Toy.Tickler)){
			Global.gui().choose(this,"Upgrade Tickler: $3000");
		}
		if(player.has(Toy.Crop)){
			Global.gui().choose(this,"Upgrade Riding Crop: $1500");
		}
		if(player.has(Toy.Onahole)){
			Global.gui().choose(this,"Upgrade Onahole: $3000");
		}
		if(player.has(Toy.Strapon)){
			Global.gui().choose(this,"Upgrade Strapon: $2500");
		}
		Global.gui().choose(this,"Leave");

	}
	
	@Override
	public void shop(Character npc, int budget) {
		int remaining = budget;
		if(npc.getPure(Attribute.Science)>0&&remaining>=npc.getAdvancedTrainingCost()){
			npc.money-=npc.getAdvancedTrainingCost();
			remaining-=npc.getAdvancedTrainingCost();
			npc.mod(Attribute.Science, 1);
			if(!npc.has(Toy.ShockGlove)){
				npc.gain(Toy.ShockGlove);
			}
		}
		if(npc.has(Toy.Onahole)&&remaining>=Toy.Onahole2.getPrice()){
			npc.money-=Toy.Onahole2.getPrice();
			remaining-=Toy.Onahole2.getPrice();
			npc.remove(Toy.Onahole, 1);
			npc.gain(Toy.Onahole2);
		}
		if(npc.has(Toy.Dildo)&&remaining>=Toy.Dildo2.getPrice()){
			npc.money-=Toy.Dildo2.getPrice();
			remaining-=Toy.Dildo2.getPrice();
			npc.remove(Toy.Dildo, 1);
			npc.gain(Toy.Dildo2);
		}
		if(npc.has(Toy.Tickler)&&remaining>=Toy.Tickler2.getPrice()){
			npc.money-=Toy.Tickler2.getPrice();
			remaining-=Toy.Tickler2.getPrice();
			npc.remove(Toy.Tickler, 1);
			npc.gain(Toy.Tickler2);
		}
		if(npc.has(Toy.Crop)&&remaining>=Toy.Crop2.getPrice()){
			npc.money-=Toy.Crop2.getPrice();
			remaining-=Toy.Crop2.getPrice();
			npc.remove(Toy.Crop, 1);
			npc.gain(Toy.Crop2);
		}
		if(npc.has(Toy.Strapon)&&remaining>=Toy.Strapon2.getPrice()){
			npc.money-=Toy.Strapon2.getPrice();
			remaining-=Toy.Strapon2.getPrice();
			npc.remove(Toy.Strapon, 1);
			npc.gain(Toy.Strapon2);
		}
		if(npc.getPure(Attribute.Science)>0&&remaining>=npc.getAdvancedTrainingCost()){
			npc.money-=npc.getAdvancedTrainingCost();
			remaining-=npc.getAdvancedTrainingCost();
			npc.mod(Attribute.Science, 1);
			if(!npc.has(Toy.ShockGlove)){
				npc.gain(Toy.ShockGlove);
			}
		}
	}

}
