package actions;

import items.Item;
import items.Trophy;

import java.io.PrintWriter;
import java.io.StringWriter;

import areas.Area;
import global.Flag;
import global.Global;
import gui.GUI;
import characters.Character;
import characters.NPC;
import characters.Trait;

public class Locate extends Action {
	private static final long serialVersionUID = 1L;

	private Character target;

	public Locate(Character target) {
		super("Locate "+target.name());
		this.target = target;
	}

	@Override
	public boolean usable(Character self) {
		return self.has(Trait.locator)&&self.has(target.getUnderwear())&&target!=self;
	}

	@Override
	public Movement execute(Character self) {
		Area area = target.location();
		Global.gui().clearText();
		Global.gui().message("Focusing on the essence contained in the "
				+ target.getUnderwear().getName()
				+ ". In your mind, an image of the "
				+ area.name
				+ " appears. It falls apart as quickly as it came to be, but you know where "
				+target.name()+" currently is. Your hard-earned trophy is already burning up in those creepy "
				+"purple flames, the smoke flowing from your nose straight to your crotch and setting another fire there.");
		self.tempt(15);
		self.consume(target.getUnderwear(), 1);
		return Movement.locating;
	}

	@Override
	public Movement consider() {
		return Movement.locating;
	}
	public boolean freeAction(){
		return true;
	}
}
