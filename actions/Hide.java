package actions;

import global.Global;
import characters.Character;
import characters.State;

public class Hide extends Action {

	public Hide() {
		super("Hide","Gives an opportunity to ambush opponents who fail a Perception check against your Cunning");
	
	}

	@Override
	public boolean usable(Character user) {
		return !(user.location().open()||user.location().corridor()||user.state==State.hidden);
	}

	@Override
	public Movement execute(Character user) {
		if(user.human()){
			Global.gui().message("You find a decent hiding place and wait for unwary opponents.");
		}
		user.state=State.hidden;
		return Movement.hide;
	}

	@Override
	public Movement consider() {
		return Movement.hide;
	}	
}
