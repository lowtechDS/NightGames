package actions;

import items.Flask;
import items.Item;
import items.Potion;
import status.Buzzed;
import status.Oiled;
import global.Global;
import global.Modifier;

import characters.Character;

public class Use extends Action {
	private Item item;
	
	public Use(Item item) {	
		super("Use "+item.getName(),item.getDesc());
		if(item==Flask.Lubricant){
			name="Oil up";
		}
		this.item=item;
	}

	@Override
	public boolean usable(Character user) {
		return user.has(item)&&(!user.human()||Global.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public Movement execute(Character user) {
		if(item==Flask.Lubricant){
			if(user.human()){
				Global.gui().message("You cover yourself in slick oil. It's a weird feeling, but it should make it easier to escape from a hold.");
			}
			user.add(new Oiled(user));
			user.consume(Flask.Lubricant, 1);
			return Movement.oil;
		}

		return Movement.wait;
	}

	@Override
	public Movement consider() {
		if(item==Flask.Lubricant){
			return Movement.oil;
		}
		return Movement.wait;
	}

}
