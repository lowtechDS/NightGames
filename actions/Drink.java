package actions;

import global.Global;
import global.Modifier;
import items.Potion;
import characters.Character;

public class Drink extends Action {
	private Potion item;
	
	public Drink(Potion item) {
		super(item.getName(),item.getDesc());
		this.item = item;
	}

	@Override
	public boolean usable(Character user) {
		return user.has(item)&&(!user.human()||Global.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public Movement execute(Character user) {
		if(user.human()){
			Global.gui().message(item.getMessage(user));
		}
		user.add(item.getEffect(user));
		user.consume(item, 1);
		return item.getAction();
	}

	@Override
	public Movement consider() {
		return item.getAction();
	}

}
