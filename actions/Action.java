package actions;
import java.io.Serializable;


import characters.Character;

public abstract class Action implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4981682001213276175L;
	protected String name;
	protected String tooltip;
	public Action(String name){
		this.name=name;
		this.tooltip = "";
	}
	public Action(String name, String tooltip){
		this.name=name;
		this.tooltip = tooltip;
	}
	public abstract boolean usable(Character user);
	public abstract Movement execute(Character user);
	public String toString(){
		return name;
	}
	public abstract Movement consider();
	public boolean freeAction(){
		return false;
	}
	public String tooltip(){
		return this.tooltip;
	}
}
